-- iaw14270791
-- Jacint Iglesias Casanova
-- HIAW1
/*		--RESET DATABASE--		*/
/*
\c  clubnautic
drop table if exists sortida ;
drop table if exists vaixell ;
drop table if exists soci ;
drop table if exists patro;
*/
\c template1
drop database if exists clubnautic;
create database clubnautic;
\c clubnautic

/*		--CREATE TALBES/SEQUENCE/INSERTS--		*/

-- patro
create table patro (
	DNI varchar(10),
	nom varchar(150),
	adreca varchar(150),
	telefon varchar(15),
	ciutat varchar(100),
	CONSTRAINT PK_PATRO_DNI PRIMARY KEY(DNI) 
);
/*		--INSERTS PATRO--		*/
insert into patro  
    (DNI,nom,adreca,telefon,ciutat)  
    values ('13567893G', 'Francis', 'C/ Almogabers 3º 4ª', '+34 674992467', 'Barcelona')
;
insert into patro  
    (DNI,nom,adreca,telefon,ciutat)  
    values ('13567895F', 'Paco', 'C/ Diagonal 1º 2ª', '+34 739116495','Barcelona')
;
insert into patro  
    (DNI,nom,adreca,telefon,ciutat)  
    values ('13567896X', 'Enrique', 'C/ Travesera de Gracia 2º 1ª', '+34 739111234','Madrid')
;
--

-- soci
create table soci (
	numSoci int,
	DNI varchar(10),
	nom varchar(150),
	adreca varchar(150),
	telefon varchar(15),
	dataAlta date,
	CONSTRAINT PK_SOCI_NUMSOCI PRIMARY KEY(numSoci) 
);

-- sequence for soci
create sequence seq_numsoci
start with 1 increment by 1;


-- 
-- vaixell
create table vaixell (
	matricula varchar(5),
	soci int,
	nomVaixell varchar(50),
	numAmarre int,
	quota numeric(9,2),
	CONSTRAINT PK_VAIXELL_MATRICULA PRIMARY KEY(matricula),
	CONSTRAINT FK_VAIXELL_SOCI FOREIGN KEY(soci) REFERENCES soci(numSoci)
		on delete cascade
		on update cascade,
	CONSTRAINT UQ_VAIXELL_NOMVAIXELL UNIQUE(nomVaixell) 
);  
/*		--INSERTS SOCI/VAIXELL--		*/
insert into soci  
    (numSoci,DNI,nom,adreca,telefon,dataAlta)  
    values (nextval('seq_numsoci'),'13567893G', 'Francis', 'C/ Almogabers 3º 4ª', '+34 674992467',current_date)
;

-- curr val vaixell
insert into vaixell  
    (matricula,soci,nomVaixell,numAmarre,quota)  
    values ('b123',currval('seq_numsoci'),'El Brivon',1,123.25)
;

insert into soci  
    (numSoci,DNI,nom,adreca,telefon,dataAlta)  
    values (nextval('seq_numsoci'),'13567895F', 'Paco', 'C/ Diagonal 1º 2ª', '+34 739116495',current_date - interval '8 months' )
;
-- curr val vaixell
insert into vaixell  
    (matricula,soci,nomVaixell,numAmarre,quota)  
    values ('B456',currval('seq_numsoci'),'Valeria',2,200.50)
;

--update 

insert into soci  
    (numSoci,DNI,nom,adreca,telefon,dataAlta)  
    values (nextval('seq_numsoci'),'13567896X', 'Enrique', 'C/ Travesera de Gracia 2º 1ª', '+34 739111234',current_date )
;
-- curr val vaixell
insert into vaixell  
    (matricula,soci,nomVaixell,numAmarre,quota)  
    values ('a123',currval('seq_numsoci'),'Pepeluis',3,300.50)
;

insert into soci  
    (numSoci,DNI,nom,adreca,telefon,dataAlta)  
    values (nextval('seq_numsoci'),'13567897X', 'Pepe', 'C/ Cristoval de Moura 3º 4ª', '+34 739111213',current_date - interval '8 months')
;
-- curr val vaixell
insert into vaixell  
    (matricula,soci,nomVaixell,numAmarre,quota)  
    values ('c456',currval('seq_numsoci'),'Oliver',4,50.33)
;
-- curr val vaixell
-- sortida
create table sortida (
	dataSortida date,
	vaixell varchar(5),
	patro varchar(10),
	horaSortida varchar(4),
	dataRetorn date,
	horaRetorn varchar(4),
	destinacio varchar(100),
	CONSTRAINT PK_SORTIDAL_DATASORTIDAVAIXELL PRIMARY KEY(dataSortida,vaixell),
	CONSTRAINT FK_SORTIDAL_VAIXELL FOREIGN KEY(vaixell) REFERENCES vaixell(matricula)
		on delete restrict
		on update cascade,
	CONSTRAINT FK_SORTIDAL_PATRO FOREIGN KEY(patro) REFERENCES patro(DNI)
		on delete set null
		on update cascade,
	CONSTRAINT CH_SORTIDAL_datasortida CHECK ( dataSortida >= current_date )-- esta mal es data sortida tiene que ser solo
																			-- < que data de retorn
);  
/*		--INSERTS SORTIDA--		*/

insert into sortida  
    (dataSortida,vaixell,patro,horaSortida,dataRetorn,horaRetorn,destinacio)  
    values (current_date,'B456','13567893G','1030',current_date + interval '1 day','1030', 'Martorell')
;

/*		-- DML--		*/
\echo 'DML'

-- A)
insert into sortida  
    (dataSortida,vaixell,patro,horaSortida,dataRetorn,horaRetorn,destinacio)  
    values (current_date + interval '1 day','B456','13567893G','1030',current_date + interval '1 day','1030', 'Martorell')
;

-- B)
delete from patro
where lower(ciutat) = 'barcelona'
;

-- C)
update vaixell
set 
	quota = quota + quota * 0.20
where lower(matricula) like 'b%'
;


/*		--EXERCICIS JOINS--		*/

\echo 'Exercicis de Joins'


-- 1)
-- Mostrar les pel·lícules que té prestades actualment cada soci (si no 
-- té prestecs, no cal mostrar-lo). Mostrar codi de dvd, codi de peli, 
-- titol, Cognoms i nom del soci (aquest darrers apareixeran junts en la 
-- mateixa columna separats per com=. Ordeneu per code de pelo.(1pt))
-- no se si funciona o no

select 
	d.coddvd,
	p.codpeli,
	p.titol,
	s.cognoms || ',' || s.nom "Cognoms, Nom"
from dvd d join pelicula p
	on d.codpeli = p.codpeli
join lloguer ll
	on ll.coddvd = d.coddvd
join soci s
	on ll.codsoci = s.codsoci
where datadev is null --corrección
order by 2
;

-- 2)
-- Mostrar el llistat de socis, hagin o no fet algun prèstec. Sleccioneu 
-- els cognoms, nom titol de la peli, data de prèstec i data de 
-- devolució- Ordeneu alfabèticament els socis i després per títol.(1pt)


select 
	initcap(s.cognoms),
	initcap(s.nom),
	coalesce(p.titol,'no te pelicula en prestec') titol,
	coalesce(ll.datapres::text,'no te pelicula en prestec') dataPrestec,
	coalesce(ll.datadev::text,'en prestec/no ha llogat') dataDevolucio
from soci s left join lloguer ll
	on ll.codsoci = s.codsoci
left join dvd d 
	on ll.coddvd = d.coddvd
left join pelicula p 
	on d.codpeli = p.codpeli
order by 1, 3
;

-- apuntate el initcap!

-- 3)
-- Mostra de cada pel·licula el codi, titol, , 
-- iva corresponent (en euros) i preu de lloguer, de les pel·licules que 
-- contenen al títol la paraula "crimen" i que valen entre 1,5 i 3 
-- euros. Teniu en compte que l'IVA és del 21% o qie eñ ca,`eu jate Iva 
-- inclòs i l'heu de descomposar. (1pt).
-- La sorida per pantallla ha de quedar aixi:
-- Codi		Titol		Preu sense iva(€)		IVA		Preu(€)

select	p.codpeli,
		p.titol,
		round(p.preu / 1.21,2) "Preu sense iva(€)",
		round(p.preu - (p.preu / 1.21),2) "IVA",
		p.preu "Preu(€)"
from pelicula p
where lower(titol) like '%a%'
and preu between 1.5 and 3;
		
		


