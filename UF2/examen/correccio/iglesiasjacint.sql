-- Nom: Jacint 
-- Cognoms: Iglesias Casanova
-- Codi: iaw14270791
-- Nom projecte: iglesiasjacint
-- Data: 13-02-2020
\c videoclub

-- 2. De les pelicules, codi, titol, numero de prestecs de la pelicula
-- i guanys generats
\echo 'Ex 2.'

select 	p.codpeli,
		titol,
		count(l.*) "Prestec",
		count(l.*) * preu "Guanys"
from pelicula p left join dvd d
	on d.codpeli = p.codpeli
left join lloguer l
	on l.coddvd = d.coddvd
group by p.codpeli,titol
order by 3 desc,2;

-- ^ correcció:

-- primero hacer como comprovacion hacer un count de pelicula,
-- cuantas salen? (14)

-- hay que agrupar por codplei, si qagrupamos solo por titulo, hay 
-- repetidos

-- si hicieramos sum(preu) saldria el precio aun cuando no ha sido 
-- alquilado

select p.codpeli,titol, sum(preu) 
from pelicula p left join dvd d on d.codpeli = p.codpeli 
left join lloguer l on l.coddvd = d.coddvd 
group by p.codpeli, titol;
-- ^esta mal por el sum, solo es un ejemplo






-- 3.
\echo 'Ex 3.'
select	ge.genere,
		coalesce(pelicula, '---') pelicula,
		coalesce((select max(preu)
		from pelicula
		where codgen = ge.codgen
		group by codgen)::text,'---') "preu"
from genere ge left join
	(select codgen,titol pelicula
	from pelicula pe
	where preu = ( 	select max(preu)
					from pelicula
					where codgen = pe.codgen))
	 peli on peli.codgen = ge.codgen;

-- ^demasiado complicado
-- corrección de profe:

select genere, titol, preu
from genere g left join pelicula p on p.codgen = g.codgen
where (genere, coalesce(preu,0)) in (select genere, coalesce(max(preu), 0)
									 from genere g left join pelicula p on p.codgen = g.codgen
									 group by genere)
order by 1,2;


	 
-- 4.
\echo 'Ex 4.'

select genere
from genere
intersect
select genere
from genere ge join pelicula p
	on p.codgen = ge.codgen
join dvd d
	on p.codpeli = d.codpeli
except
select genere 
from genere ge join pelicula p
	on p.codgen = ge.codgen
join dvd d
	on d.codpeli = p.codpeli
join lloguer l
	on l.coddvd = d.coddvd;
	
-- ^ le falta order by alfabeticamente
-- correcció :

select genere
from genere ge join pelicula p
	on p.codgen = ge.codgen
join dvd d
	on p.codpeli = d.codpeli
except
select genere 
from genere ge join pelicula p
	on p.codgen = ge.codgen
join dvd d
	on d.codpeli = p.codpeli
join lloguer l
	on l.coddvd = d.coddvd;

-- lo que queria el profe : tots els genrees - generes presents a lloguer

select genere
from genere
except
select genere
from genere g join pelicula p
	on p.codgen = g.codgen
join dvd d on d.codpeli = p.codpeli
join dvd on d.codpeli = p.codpeli
join lloguer l on l.coddvd = d.coddvd;


-- 5. Socis que han demant en prestec mes de dos pelis diferents.  

select	nom,
		cognoms
from soci s join lloguer l
	on l.codsoci = s.codsoci
join dvd d
	on l.coddvd = d.coddvd
join pelicula p 
	on d.codpeli = p.codpeli
group by nom,cognoms
having count(distinct p.codpeli) > 2;

-- ^ tiene un join de mas a pelicula
-- corrección

select  nom,
        cognoms
from soci s join lloguer l
        on l.codsoci = s.codsoci
join dvd d
        on l.coddvd = d.coddvd
group by nom,cognoms
having count(distinct d.codpeli) > 2;



\c lloguercotxes

-- 1.
\echo 'Ex 1.'

create or replace view vehicle as
select matricula, marca, model, 'moto' tipus,
	case 
		when lower(tipus) = 'elèctric de bateria' or
			lower(tipus)|| ' '|| lower(subtipus) = 'híbrid endollable'
				then '0 emissions'
		when lower(tipus)|| ' '|| lower(subtipus) = 'propulsió gas  gas natural' or
			lower(tipus)|| ' '|| lower(subtipus) = 'propulsió gas gas liquat' or
			lower(tipus)|| ' '|| lower(subtipus) = 'híbrid no endollable' 
			then 'Eco'
		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2006' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2015' 
		then 'C'
		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2001' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2006' 
		then 'B'
		else
			'sense etiqueta'
		end etiqueta
from moto
union
select matricula, marca, model, 'cotxe' tipus,
	case 
		when lower(tipus) = 'elèctric de bateria' or
			lower(tipus)|| ' '|| lower(subtipus) = 'híbrid endollable'
				then '0 emissions'
		when lower(tipus)|| ' '|| lower(subtipus) = 'propulsió gas  gas natural' or
			lower(tipus)|| ' '|| lower(subtipus) = 'propulsió gas gas liquat' or
			lower(tipus)|| ' '|| lower(subtipus) = 'híbrid no endollable' 
			then 'Eco'
		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2006' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2015' 
		then 'C'
		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2001' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2006' 
		then 'B'
		else
			'sense etiqueta'
		end etiqueta
from cotxe;


-- correcció:

		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2006' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2015' 
		then 'C'
		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2001' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2006' and to_char(datamatriculacio,'yyyy') <'2015'
		then 'B'
		else

















