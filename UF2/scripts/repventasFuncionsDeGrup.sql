-- Script: 			repventasFuncionsDeGrup.sql
-- Descripció: 		exercicis de funcions de grup a training
-- Autor: 			Jacint Iglesias Casanova
-- Codi: 			iaw14270791
-- Darrera revisió:	09/01/2020
-- Versió:			0.1

--1. Mostrar la suma de las cuotas y la suma de las ventas totales de todos los
--representantes.

select 
	sum(cuota) "total cuota",
	sum(ventas) "total ventas"
from repventa
order by 1,2
;

--2. ¿Cuál es el importe total de los pedidos tomados por Bill Adams?

select 	
	--nombre,
	sum(importe) "total pedidos"
from pedido p join repventa r
	on p.repcod = r.repcod
where lower(nombre) = 'bill adams'
--group by 1
;
-- no pide el nombre, por lo tanto no complicar la consulta mas de lo
-- exigido


--3. Calcula el precio medio de los productos del fabricante ACI.

select
	fabcod,
	round(avg(importe / cant),2) "precio medio" -- considerando precio 
									   -- como importe / cant
from pedido
where lower(fabcod) = 'aci'
group by 1
;

--4. ¿Cuál es el importe medio de los pedido solicitados por el cliente 2103

select
	cliecod,
	round(avg(importe),2) "media importe"
from pedido
where cliecod = 2103
group by 1
;

--5. Mostrar la cuota máxima y la cuota mínima de las cuotas de los representantes.

select
	max(cuota) "cuota maxima",
	min(cuota) "cuota minima"
from repventa
;

--6. ¿Cuál es la fecha del pedido más antiguo que se tiene registrado?

select 
	min(fecha) "pedido mas antiguo" 
from pedido 
;

--7. ¿Cuál es el mejor rendimiento de ventas de todos los representantes? (consid-
--erarlo como el porcentaje de ventas sobre la cuota).

select 
	round(max(ventas/cuota*100),2) "maximo porcentaje de rendimiento" 
from repventa
;

--8. ¿Cuántos clientes tiene la empresa?

select count(*) "total numero de clientes"
from cliente;


--9. ¿Cuántos representantes han obtenido un importe de ventas superior a su
--propia cuota?

select
	count(*) "total representantes con ventas superior a cuota"
from repventa
where ventas > cuota
;

--10. ¿Cuántos pedidos se han tomado de más de 150 euros?

select
	count(*) "cantidad de pedidos superiores a 150"
from pedido
where importe > 150
;

--11. Halla el número total de pedidos, el importe medio, el importe total de los
--mismos.

select
	count(*) "numero total de pedidos",
	round(avg(importe),2) "media total de los pedidos",
	sum(importe) "total importe de pedidos"
from pedido
;

--12. ¿Cuántos puestos de trabajo diferentes hay en la empresa?

select
	count(distinct puesto) "total trabajos difernetes"
from repventa
;

--13. ¿Cuántas oficinas de ventas tienen representantes que superan sus propias
--cuotas?

select
	count(distinct ofinum) "total oficinas"
from repventa 
where ventas > cuota
;
--514. ¿Cuál es el importe medio de los pedidos tomados por cada representante?

select
	r.repcod,
	round(avg(importe),2) "media importe pedido"
from pedido p right join repventa r
	on p.repcod = r.repcod
group by r.repcod
order by 1
;

-- preguntar por que es right join?!



--15. ¿Cuál es el rango de las cuotas de los representantes asignados a cada oficina
--(mínimo y máximo)?

select
	o.ofinum,
	max(cuota) "cuota maxima", 
	min(cuota) "cuota minima"
from repventa r join oficina o
	on r.ofinum = o.ofinum
group by o.ofinum
;


--16. ¿Cuántos representantes hay asignados a cada oficina? Mostrar Ciudad y
--número de representantes.

select
	coalesce(ciudad, 'no consta') "ciudad",
	count(*) "numero de representats" -- puedes hacer count(*) ya qye es 
									  -- un join no añade filas, las 
									  -- limita en todo caso
									  -- count(*) en lugar de count(r.*)
from repventa r left join oficina o
	on r.ofinum = o.ofinum
group by ciudad
;
-- hacia falta OUTER LEFT join para especificar el empleado no asignado
-- a ninguna oficina

--17. ¿Cuántos clientes ha contactado por primera vez cada representante? Mostrar
--el código de representante, nombre y número de clientes.
/*
select
	r.repcod,
	r.nombre,
	count(cliecod) "cantidad de clientes"
from pedido p join repventa r
	on p.repcod = r.repcod
group by 1,2
order by 3
;
*/ -- esta mal el campo a contar es repcod en cliente
select 
	r.repcod,
	r.nombre,
	count(cliecod) "total clientes contactados"
from cliente cl join repventa r
	on cl.repcod = r.repcod
group by 1,2
;
--18. Calcula el total del importe de los pedidos solicitados por cada cliente a cada
--representante.

select
	repcod,
	cliecod,
	sum(importe) "total importe"
from pedido
group by 1,2
order by 1,2
;
	
--19. Lista el importe total de los pedidos tomados por cada representante.

select
	repcod,
	sum(importe) "total importe"
from pedido
group by 1
order by 1
;

-- 19.bis Mostrar nomes els representants que tenen al menys tres comandes

select
	repcod,
	sum(importe) "total pedido"
from pedido
group by 1
having count(*) >= 3
;

-- 19.bis.bis Y que la suma superi els 30.000

select
	repcod,
	sum(importe) "total pedido"
from pedido
group by 1
having 
	count(*) >= 3 and 
	sum(importe) > 30000
;



--20. Para cada oficina con dos o más representantes, calcular el total de las cuotas
--y el total de las ventas de todos sus representantes.


-- añadir count(repventa) para validar que la condición se esta
-- cumpliendo, quitarlo cuando sea validado para coincidir con lo pedido
-- en el 

select
	ciudad,
	sum(cuota) "total cuota",
	sum(r.ventas) "total ventas"
from repventa r join oficina o
	on r.ofinum = o.ofinum
group by 1
--having count(repcod) >= 2 -- hay que hacer el count(*)(en este caso no)
-- hay diferencia pero aun y asi
having count(*) >= 2
order by 1
;
-- mana la taula representants per que te la clau aliena, per tant repventa


-- 20.bis igual pero mostrant objetivo y ventaqs de oficina
-- capçalera:
-- ciutat, objectiu, objectiu calculat, vendess, vendes cal


select
	ciudad "oficina",
	o.objetivo,
	sum(cuota) "Objetivo calculado",
	o.ventas,
	o.objetivo - sum(cuota) "diferencia",
	sum(r.ventas) "vendes calculades",
	o.ventas,
	o.ventas - sum(r.ventas) "diferencia"
from repventa r join oficina o
	on r.ofinum = o.ofinum
group by 1,2,4,7
--having count(repcod) >= 2 -- hay que hacer el count(*)(en este caso no)
-- hay diferencia pero aun y asi
having count(*) >= 2
order by 1
;


/*
	orden correcto es:
select
from
group by
having
order by
*/

--21. Muestra el número de pedidos que superan el 75% de las existencias.

select
	count(*) "numero de pedidos 75%+ existencias"
from pedido p join producto pr
	on p.fabcod||p.prodcod = pr.fabcod||pr.prodcod
where cant / exist > 0.75
;
