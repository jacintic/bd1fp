-- Script: 			exerciciMaxMesJoins.sql
-- Descripció: 		exercici de funcions de grup, joins i subconsultes
-- Autor: 			Jacint Iglesias Casanova
-- Codi: 			iaw14270791
-- Darrera revisió:	09/01/2020
-- Versió:			0.1

-- Enunciat:
-- Per cada departament, qui cobra mes. Lloc de proves, nom departament 
-- y nom persona.


select  dname,
        ename
from dept d
        full outer join emp e
                on e.deptno = d.deptno
where sal::text||e.deptno::text in (select max(sal)::text||de.deptno::text
									from emp em
										full outer join dept de
											on em.deptno = de.deptno
									
									group by de.deptno)
or e.deptno is null and sal in (select max(sal)
								from emp
								where deptno is null)
or d.deptno is not null and ename is null
order by 1,2
;

<<<<<<< HEAD
-- joc de proves
select max(sal) 
from emp
group by deptno;
--
select  dname,
        ename,
        sal
from dept d
        full outer join emp e
                on e.deptno = d.deptno
where sal::text||e.deptno::text in (select max(sal)::text||de.deptno::text
									from emp em
										full outer join dept de
											on em.deptno = de.deptno
									
									group by de.deptno)
or e.deptno is null and sal in (select max(sal)
								from emp
								where deptno is null)
or d.deptno is not null and ename is null
order by 1,2
;
--
select dname
from dept;
=======

-- ref

select deptno, max(sal)
from emp
group by deptno;
>>>>>>> 21781a1e2001534e7ffddb607f0a52d20b109fcc
