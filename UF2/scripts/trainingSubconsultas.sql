-- Script: 			repventasFuncionsDeGrup.sql
-- Descripció: 		exercicis de subconsultes a training
-- Autor: 			Jacint Iglesias Casanova
-- Codi: 			iaw14270791
-- Darrera revisió:	09/01/2020
-- Versió:			0.1



-- D. Subconsultas

-- 0. Mostrar el nombre y el puesto de los que son jefe (ya está hecho con self
-- join, ahora con subconsultas)

select 	nombre,
		puesto
from repventa
where repcod in (	select jefe
					from repventa);


-- 1. Obtener una lista de los representantes cuyas cuotas son iguales ó superiores
-- al objetivo de la oficina de Atlanta.

select *
from repventa  
where cuota >= (	select objetivo
					from oficina
					where lower(ciudad) = 'atlanta');


-- 2. Obtener una lista de todos los clientes (nombre) que fueron contactados por
-- primera vez por Bill Adams.

select nombre
from cliente
where repcod = (	select repcod
					from repventa
					where lower(nombre) = 'bill adams');

-- 3. Obtener una lista de todos los productos del fabricante ACI cuyas existencias
-- superan a las existencias del producto 41004 del mismo fabricante.

select * 
from producto
where exist > (	select exist
				from producto
				where lower(prodcod|| fabcod) = '41004aci')
and lower(fabcod) = 'aci';


-- estaria mal si no tuviera fabcod en el where de la subconsutla

-- extra insert para desmontar lo de arriba si no tuviera fabcod en el 
--where de subconsulta

insert into producto
values ('asd','41004','sdaf',1000,10);


-- 4. Obtener una lista de los representantes que trabajan en las 
-- oficinas que han logrado superar su objetivo de ventas.
					
select *
from repventa r
where ventas >= (	select objetivo
					from oficina
					where r.ofinum = ofinum);
-- esta mal ^

select *
from repventa r
where ofinum in (	select ofinum
					from oficina
					where ventas > objetivo);

-- 5. Obtener una lista de los representantes que no trabajan en las oficinas dirigidas
-- por Larry Fitch.

select *
from repventa
where ofinum not in (	select o.ofinum
						from oficina o join repventa r
							on o.director = r.repcod
						where lower(nombre) = 'larry fitch');
					
-- con joins

select r.*
from repventa r join oficina o
	on r.ofinum = o.ofinum
join repventa d
	on director = d.repcod
where lower(d.nombre) != 'larry fitch';


-- 6. Obtener una lista de todos los clientes que han solicitado pedidos del fabricante
-- ACI entre enero y junio de 2003.
update pedido
set fecha = fecha + interval '13 years'
; -- recomendable hacer insert en vez de update, insert de prueva

select *
from cliente
where cliecod in (	select cliecod
					from pedido
					where lower(fabcod) = 'aci'
					and to_char(fecha, 'yyyy-mm') between'2003-01' and '2003-6');
					
					-- sin update
					
					
select *
from cliente 
where cliecod in (	select cliecod
					from pedido
					where lower(fabcod) = 'aci'
					and to_char(fecha, 'yyyy-mm') between'1990-01' and '1990-6');			


-- joins

select cl.*
from cliente cl
	join pedido p
		on p.cliecod = cl.cliecod
where lower(fabcod) = 'aci'
and to_char(fecha, 'yyyy-mm') between'1990-01' and '1990-6';

-- 7. Obtener una lista de los productos de los que se ha tomado un pedido de 150
-- euros ó mas.

select *
from producto
where fabcod || prodcod in (	select fabcod || prodcod
								from pedido
								where importe >= 150);


-- 8. Obtener una lista de los clientes contactados por Sue Smith que no han
-- solicitado pedidos con importes superiores a 18 euros.

select *
from cliente
where cliecod in (      select cl.cliecod
                        from cliente cl join repventa r
                                on cl.repcod = r.repcod
                        join pedido p
                                on p.cliecod = cl.cliecod
                        where cl.cliecod not in (	select cliecod
													from pedido
													where importe > 3000)
                        and lower(r.nombre) = 'sue smith');
-- no contactados por primera vez sino contactados
select * 
from cliente 
where cliecod in (	select p.cliecod
						from pedido p join repventa r
							on p.repcod = r.repcod
						where lower(r.nombre) != 'sue smith'
						and p.cliecod not in (	select cliecod
													from pedido
													where importe > 3000));

-- intento optimizar
select cl.*
from cliente cl join pedido p
	on p.cliecod = cl.cliecod
join repventa r
	on p.repcod = r.repcod
where lower(r.nombre) = 'sue smith'
and p.cliecod not in (	select cliecod
						from pedido
						where importe > 3000);
						
						-- seguramente esta bien, pero hay que preguntar 
						-- por que manda la tabla pedido en este caso
-- corrección profe:

select * 
from cliente
where repcod = (select repcod
				from repventa
				where lower(nombre) = 'sue smith')
and cliecod not in (select cliecod
					from pedido
					where importe > 18);
					
					
					-- proveer con subconsultas los datos que faltan
					
					-- puedes substituir la quivalencia por un join
					-- (repcod r = repcod p) r.name= ""


					-- no se puede substituir un NOT IN por un JOIN
					
					

-- 9. Obtener una lista de las oficinas en donde haya algún representante cuya
-- cuota sea más del 55% del objetivo de la oficina.
                                                                        
select *
from oficina ofi
where ofinum in (       select r.ofinum
                        from oficina o join repventa r
                                on r.ofinum = o.ofinum
                        where r.cuota / o.objetivo > 0.55
                        and ofi.ofinum = r.ofinum);
--NO HACER JOIN SI PIDEN SUBCONSULTA!

select *
from oficina o
where ofinum in (select ofinum
				from repventa r
				where cuota > o.objetivo * 0.55);
				-- and r.ofinum = o.ofinum); -- poner a prueva si es 
										-- necesario este enlace
										-- no es necesario, ya estas
										-- igualando el primer ofinum

-- consulta joc de proves

select r. repcod, 
	r.nombre, 
	coalesce(r.ofinum::text, '-'), 
	coalesce(r.cuota::text, '-'), 
	coalesce((o.objetivo * 0.55)::text,'-') "objetivo 50%", 
	coalesce((r.cuota > o.objetivo * 0.55)::text,'-') "Condition"
from repventa r left join oficina o
	on r.ofinum = o.ofinum;
-- si quieres mirate el case

-- 610. Obtener una lista de los representantes que han tomado algún pedido cuyo
-- importe sea más del 10% de de su cuota.
 
 select *
from repventa
where repcod in (       select r.repcod
                        from pedido p join repventa r
                                on p.repcod = r.repcod
                        where p.importe / cuota > 0.1);


select * 
from repventa r
where repcod in (	select repcod
					from pedido
					where importe > r.cuota * 0.1); -- este si que es 
													-- correcto
-- 11. Obtener una lista de las oficinas en las cuales el total de ventas de sus
-- representantes han alcanzado un importe de ventas que supera el 50% del
-- objetivo de la oficina. Mostrar también el objetivo de cada oficina (suponed que
-- el campo ventas de oficina no existe).

select *
from oficina o
where ofinum in  (      select distinct o.ofinum
                        from oficina o join repventa r
                                on r.ofinum = o.ofinum
                        where o.objetivo * 0.5 < (      select sum(ventas)
                                                        from repventa
                                                        where ofinum = o.ofinum
                                                        group by ofinum));
                                                        
select *
from oficina o
where ofinum in ( 	select ofi.ofinum
					from oficina ofi join repventa r
						on r.ofinum = ofi.ofinum
					group by ofi.ofinum
					having sum(r.ventas) > objetivo * 1.2);
  
  
select * 
from oficina o
where objetivo * 1.2 < (select sum(ventas)
						from repventa 
						where o.ofinum = ofinum
						group by ofinum);
						
						
						
-- con joins						
select o.*
from oficina o join repventa r
			on r.ofinum = o.ofinum
group by o.ofinum
having sum(r.ventas) > objetivo * 1.2;

-- para que ^ sea SQL standard deven ir todos los campos en el group by
-- Oracle te diria que nanai

 
						--
--select objetivo * 0.5, ofinum from oficina order by ofinum;
--select sum(ventas),ofinum from repventa group by ofinum order by ofinum;



-- 12. sum sum
-- Clients que no han fet cap comanda

select *
from cliente
where cliecod not in ( select cliecod
						from pedido);
						
						

