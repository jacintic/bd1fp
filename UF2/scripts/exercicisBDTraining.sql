/*
HIAW1
iaw14270791
Jacint Iglesias
Base de Dades
UF2
exercicisTraining.sql
DB: training
*/
\c training
--CONSULTAS SIMPLES

/*
1. Obtener los datos de los productos cuyas existencias estén entre 25 
y 40 unidades.
*/
select * 
from producto
where exist between 25 and 40
order by 5
;



/*
2. Obtener los códigos de los representantes que han tomado algún pedido
(evitando su repetición).
*/

select distinct repcod 
from pedido 
order by 1
;

/*
3. Obtener los datos de los pedidos realizados por el cliente cuyo código es el
2111.
*/
select *
from pedido
where cliecod = 2111
order by 1
;


/*
4. Obtener los datos de los pedidos realizados por el cliente cuyo código es el
2111 y que han sido tomados por el representante cuyo código es el 103.
*/

select *
from pedido
where cliecod = 2111 and
repcod = 103
order by 1
;


/*
5. Obtener los datos de los pedidos realizados por el cliente cuyo código es el
2111, que han sido tomados por el representante cuyo código es el 103 y que
solicitan artículos del fabricante cuyo código es ACI.
*/
select *
from pedido
where cliecod = 2111 and
repcod = 103 and
lower(fabcod) = 'aci'
order by 1
;


/*
6. Obtener una lista de todos los pedidos ordenados por cliente y, para cada
cliente, ordenados por la fecha del pedido (ascendentemente)
*/
select *
from pedido
order by cliecod, fecha 
;

/*
7. Obtener los datos de los representantes que pertenecen a la oficina de código
12 y 13 (cada representante solo pertenece a una oficina).
*/
select *
from repventa
where ofinum in (12,13)
order by ofinum desc
;

/*
8. Obtener los datos de productos de los que no hay existencias o bien éstas son
desconocidas.
*/

select *
from producto
where exist = 0 or exist is null
;
/*
9. Mostrar los representantes que fueron contratados en el 2003 (sumem 5000 a
la data de contracte)
310. Mostrar el nombre y días que lleva contratados los representants
*/
update repventa
        set fcontrato = fcontrato + 5000
;

select *
from repventa
where to_char(fcontrato,'yyyy') = '2003'
;

--
-- CONSULTAS MULTITABLA
--

/*
1. Lista los nombres de los representantes, mostrando la ciudad de la oficina
donde trabaja y la región y las ciudades de las oficinas en donde trabajan.
*/
select
	repcod,
	nombre,
	ciudad,
	region
from repventa r left join oficina o
	on r.ofinum = o. ofinum
;


/*2. Obtener una lista de todos los pedidos, mostrando el número de pedido,
su importe, el nombre del cliente que lo realizó y el límite de crédito de
dicho cliente.
*/
select
	pednum,
	importe,
	nombre,
	limcred
from pedido p join cliente c
	on p.cliecod = c.cliecod
order by 3
;
-- aun que hay un NULL en limcred, no afecta a la condición de join
-- por lo que no hay que hacer un left/right join


/*
3. Obtener una lista con los nombres de los representantes de la empresa,
mostrando además el nombre de la oficina en la que trabaja cada uno y la
región a la que vende.
*/
select 
	nombre,
	ciudad,
	region
from repventa r left join oficina o
	on r.ofinum = o.ofinum
order by 3 desc
;

/*
4. Obtener una lista de las oficinas (ciudades, no códigos) que tienen un
objetivo superior a 3600 euros. Para cada oficina mostrar la ciudad, su
objetivo, el nombre de su director y puesto del mismo.
*/
select
	ciudad,
	objetivo,
	d.nombre "Director",
	d.puesto
from oficina o join repventa d
	on o.director = d.repcod
where objetivo > 3600
order by 2 desc
;
-- major de 360000
	-- en este caso filtra resultados, el anterior de 3600 no filtraba 
	-- nada ya que todos cumplian la condición
select
	ciudad,
	objetivo,
	d.nombre "Director",
	d.puesto
from oficina o join repventa d
	on o.director = d.repcod
where objetivo > 360000
order by 2 desc
;


/*
5. Obtener una lista de todos los pedidos mostrando su número, el importe y
la descripción de los productos solicitados.
*/
select
	pednum,
	importe,
	descrip
from pedido p join producto pr
	on p.fabcod = pr.fabcod AND p.prodcod = pr.prodcod
order by 1
;

-- manera con FK/PK compuesta con parentesis

select
	pednum,
	importe,
	descrip
from pedido p join producto pr
	on (p.fabcod,p.prodcod) = (pr.fabcod, pr.prodcod)
order by 1
;
-- maenera concatenando
select
	pednum,
	importe,
	descrip
from pedido p join producto pr
	on p.fabcod || p.prodcod = pr.fabcod || pr.prodcod
order by 1
;
-- con order by fabcod
select
	pednum,
	p.fabcod||pr.prodcod
	importe,
	descrip
from pedido p join producto pr
	on p.fabcod || p.prodcod = pr.fabcod || pr.prodcod
order by p.fabcod
;
/*
6. Obtener una lista de los pedidos con importes superiores a 150 euros.
Mostrar el código del pedido, el importe, la descripción del producto
solicitado, el nombre del representante que lo tomó y el nombre del cliente
que lo solicitó.
*/
select 
	pednum, 
	importe,
	descrip, 
	r.nombre "nombre representante",
	cl.nombre "nombre cliente"
from pedido p join producto pr
	on p.fabcod = pr.fabcod 
		and p.prodcod = pr.prodcod
join repventa r
	on p.repcod = r.repcod
join cliente cl
	on p.cliecod = cl.cliecod
where importe > 150
order by 4
;

/*
7. Obtener una lista de los pedidos con importes superiores a 150 euros,
mostrando el nombre del cliente que lo solicitó y el nombre del representante
que contactó con el cliente por primera vez.
*/
select
	c.nombre,
	r.nombre
from pedido p join cliente c
	on p.cliecod = c.cliecod
join repventa r
	on p.repcod = r.repcod
where importe > 150
;

/*
8. Obtener una lista de los pedidos con importes superiores a 150 euros,
mostrando el código del pedido, el importe, el nombre del cliente que lo
solicitó, el nombre del representante que contactó con él por primera vez y
la ciudad de la oficina donde el representante trabaja.
*/
select
	p.pednum,
	p.importe,
	cl.nombre "nombre cliente",
	r.nombre "nombre representante",
	o.ciudad
from pedido p join cliente cl
	on p.cliecod = cl.cliecod
join repventa r
	on p.repcod = r.repcod
left join oficina o
	on r.ofinum = o.ofinum
where importe > 150
;
/*
9. Lista los pedidos tomados durante el mes de octubre del año 2003 ,
mostrando solamente el número del pedido, su importe, el nombre del
cliente que lo realizó, la fecha y la descripción del producto solicitado
*/
update pedido                                                   
        set fecha = fecha + 5000
;

select
	pednum,
	importe,
	nombre,
	fecha,
	descrip
from pedido p join producto pr
	on p.prodcod = pr.prodcod and
		p.fabcod = pr.fabcod
join cliente cl
	on p.cliecod = cl.cliecod
where to_char(fecha,'yyyy-mm') = '2003-10' 
;

/*
10. Obtener una lista de todos los pedidos tomados por representantes de
oficinas de la región Este, mostrando solamente el número del pedido, la
descripción del producto y el nombre del representante que lo tomó
*/
select 
	p.pednum,
	descrip,
	r.nombre 
from pedido p join repventa r
	on p.repcod = r.repcod
left join oficina o
	on r.ofinum = o.ofinum
join producto pr
	on p.prodcod = pr.prodcod and p.fabcod = pr.fabcod
where lower(region) = 'este'
;


/*
11. Obtener los pedidos tomados en los mismos días en que un nuevo rep-
resentante fue contratado. Mostrar número de pedido, importe, fecha
pedido.
*/

select 
	distinct pednum,
	importe,
	fecha
from  pedido p cross join repventa r
where fcontrato = fecha
;

-- NO FUNCIONA, PARECE QUE NO SE CUMPLE LA CONDICION EN NINGUN CASO
-- HACER INSERTS

-- nota cross join es el producto cartesiano, incluir un distinct para 
-- evitar duplicados


-- otra nota, es realmente DISTINCT la unica forma de hacer esto?

-- forma standard:
select 	
	distinct pednum,
	importe,
	fecha
from pedido join repventa
on fcontrato = fecha
;

/*
select 	
	pednum,
	importe,
	fecha
from pedido join repventa
on fcontrato = fecha
;
-- con \x como explicación
*/

/*
12. Obtener una lista con parejas de representantes y oficinas en donde la
cuota del representante es mayor o igual que el objetivo de la oficina, sea o
no la oficina en la que trabaja. Mostrar Nombre del representante, cuota
del mismo, Ciudad de la oficina, objetivo de la misma.
*/

select 
	r.repcod,
	r.nombre,
	cuota,
	ciudad,
	objetivo
from repventa r cross join oficina o
where cuota >= objetivo
order by 1 
;


/*
13. Muestra el nombre, las ventas y la ciudad de la oficina de cada representante
de la empresa.
*/

select
	nombre,
	r.ventas "ventas representante",
	o.ventas "ventas oficina",
	ciudad
from repventa r left join oficina o
	on r.ofinum = o.ofinum
;

/*
14. Obtener una lista de la descripción de los productos para los que existe
algún pedido en el que se solicita una cantidad mayor a las existencias de
dicho producto.
*/

select
	descrip
from producto pr join pedido p
	on p.fabcod||p.prodcod = pr.fabcod||pr.prodcod
where cant > exist
;

/*
15. Lista los nombres de los representantes que tienen una cuota superior a la
de su director.
*/

-- si consideramos director el director definido en oficina y sus
-- subordinados los que trabajan en su misma oficina
select
	r.repcod,
	r.nombre
from repventa r left join oficina o
	on r.ofinum = o.ofinum
join repventa d
	on o.director = d.repcod
where r.cuota > d.cuota
;
-- si consideramos como director simplemente el jefe
select
	r.repcod,
	r.nombre
from repventa r left join repventa d
	on r.jefe = d.repcod
where r.cuota > d.cuota
;
/*
16. Obtener una lista de los representantes que trabajan en una oficina distinta
de la oficina en la que trabaja su director, mostrando también el nombre
del director y el código de la oficina donde trabaja cada uno de ellos.
*/
-- se interpret
select 
	r.nombre empleado, 
	r.ofinum "oficina representante", 
	j.nombre jefe, 
	j.ofinum "oficina jefe"
from repventa r left join repventa j
	on r.jefe = j.repcod
where j.ofinum != coalesce(r.ofinum,0) -- coalesce no V
;
-- consideramos que el que no tiene oficina no ha de salir en el 
-- resultado como no tiene oficina no puede estar en una oficina 
-- diferente:

select 
	r.nombre empleado, 
	r.ofinum "oficina representante", 
	j.nombre jefe, 
	j.ofinum "oficina jefe"
from repventa r left join repventa j
	on r.jefe = j.repcod
where j.ofinum != r.ofinum
;
/*
17. El mismo ejercicio anterior, mostrando en lugar de su código de la 
oficina la ciudad del representante y su jefe.
*/
select 
	r.nombre representante, 
	cr.ciudad "ciudad oficina representante",  
	j.nombre director,
	cj.ciudad "ciudad oficina director"
from repventa r left join repventa j
	on r.jefe = j.repcod
left join oficina cr
	on r.ofinum = cr.ofinum
join oficina cj -- no hace falta left join ya que todos los jefes tienen 
-- oficina
	on j.ofinum = cj.ofinum
where j.ofinum != r.ofinum -- quitar el coalesce para descartar rep sin
-- oficina
;

-- si no se hace un tercer join con la oficina del jefe, no se puede
-- determinar la ciudad del jefe 
 
 
/*
18. Mostrar el nombre y el puesto de los que son jefe.
*/
select
	distinct j.nombre,
	j.puesto
from repventa r left join repventa j
	on r.jefe= j.repcod
where j.repcod = r.jefe
;
-- hace repetición de cada jefe sin el distinct
