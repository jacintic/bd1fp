\c template1
drop database if exists carreteres;
create database carreteres;
\c carreteres

--
-- Name: carretera; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE carretera (
    idCarretera smallint,
    tramsNum smallint,
    longitud numeric(8,3),
    tipus varchar(15),
    CONSTRAINT PK_CARRETERA_IDCARRETERA PRIMARY KEY(idCarretera),
    CONSTRAINT CH_CARRETERA_TIPUS CHECK ( lower(tipus) in ('comarcal', 'regional', 'nacional') )
);


--
-- Name: area; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE area (
    idArea smallint,
    descripcio varchar(350),
    CONSTRAINT PK_AREA_IDAREA PRIMARY KEY(idArea)
);


--
-- Name: termeMunicipal; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE termeMunicipal (
    idMunicipi smallint,
    nom varchar(150),
    CONSTRAINT PK_TERMEMUNICIPAL_IDMUNICIPI PRIMARY KEY(idMunicipi)
);


--
-- Name: tram; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE tram (
    idTram smallint,
    carretera smallint,
    area smallint,
    CONSTRAINT PK_TRAM_IDTRAM PRIMARY KEY(idTram),
    CONSTRAINT FK_TRAM_CARRETERA FOREIGN KEY(carretera) REFERENCES carretera(idCarretera),
    CONSTRAINT FK_TRAM_AREA FOREIGN KEY(area) REFERENCES area(idArea)   
);


--
-- Name: tramXmunicipi; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE tramXmunicipi (
    tram smallint,
    municipi smallint,
    kmEntrada numeric(7,3),
    kmSortida numeric(7,3),
    CONSTRAINT PK_TRAMXMUNICIPI_TM PRIMARY KEY(tram,municipi),
    CONSTRAINT FK_TTRAMXMUNICIPI_TRAM FOREIGN KEY(tram) REFERENCES tram(idTram),
    CONSTRAINT FK_TRAMXMUNICIPI_MUNICIPI FOREIGN KEY(municipi) REFERENCES termeMunicipal(idMunicipi)   
);



--
-- insert carretera
--
INSERT INTO carretera 
VALUES(1, 3, 250.50, 'comarcal');
INSERT INTO carretera 
VALUES(2, 2, 150.50, 'regional');
INSERT INTO carretera 
VALUES(3, 5, 600, 'nacional');

--
-- insert area
--
INSERT INTO area 
VALUES(1, 'Area de descans');
INSERT INTO area 
VALUES(2, 'Area de trafic');
INSERT INTO area 
VALUES(3, 'Area de intersecció');

--
-- insert termeMunicipal
--
INSERT INTO termeMunicipal 
VALUES(1, 'Montserrat');
INSERT INTO termeMunicipal 
VALUES(2, 'Barcelona');
INSERT INTO termeMunicipal 
VALUES(3, 'Hospitalet');

--
-- insert tram
--
INSERT INTO tram 
VALUES(1, 1,1);
INSERT INTO tram 
VALUES(2, 1,2);
INSERT INTO tram 
VALUES(3, 2,1);
INSERT INTO tram 
VALUES(4, 3,1);

--
-- insert tramXmunicipi
--
INSERT INTO tramXmunicipi 
VALUES(1,1,0,100.50);
INSERT INTO tramXmunicipi 
VALUES(1,2,100.51,200.50);
INSERT INTO tramXmunicipi 
VALUES(2,3,25.51,47.50);
INSERT INTO tramXmunicipi 
VALUES(4,3,60,95.32);
