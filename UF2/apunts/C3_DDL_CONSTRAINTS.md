# Clase 3 DDL 

* **FK** a nivell de fila
`nomCamp smallint constraint fk_nomtaula_nomcamp references nomtaulareferenciada(clauprimaria"opcional")`  

Si utilizamos un mismo nombre de constraint `fk_nomtaula_nomcamp` en dos campos diferentes, en **Oracle**, nos dara error.  


**Noms de BDs en minuscules.**  

  
* **Constraint UNIQUE:** evita que els valors es repeteixin i **admet NULLs**.  
* **Constraint PRIMARY KEY:** controla que els valors no es repeteixin i **no admet NULLS**.  
* **Constraint NOT NULL:** fa que un camp sigui **obligatori**. **NOMES ES POT UTILITZAR A NIVELL DE FILA**  


**Una FK pot ser composta si fa referencia a una PK que es a la seva vegada composta.**
**Una PK composta s'ha de declarar com a CONSTRAINT a nivell de taula.**
```
training=> \d pedido
       Table "public.pedido"
 Column  |     Type     | Modifiers 
---------+--------------+-----------
 pednum  | integer      | not null
 fecha   | date         | not null
 cliecod | smallint     | not null
 repcod  | smallint     | 
 fabcod  | character(3) | not null
 prodcod | character(5) | not null
 cant    | smallint     | not null
 importe | numeric(7,2) | not null
Indexes:
    "pk_pedido_pednum" PRIMARY KEY, btree (pednum)
Foreign-key constraints:
    "fk_pedido_cliecod" FOREIGN KEY (cliecod) REFERENCES cliente(cliecod)
    "fk_pedido_fp" FOREIGN KEY (fabcod, prodcod) REFERENCES producto(fabcod, prodcod)
    "fk_pedido_repcod" FOREIGN KEY (repcod) REFERENCES repventa(repcod)

training=> \d producto
           Table "public.producto"
 Column  |         Type          | Modifiers 
---------+-----------------------+-----------
 fabcod  | character(3)          | not null
 prodcod | character(5)          | not null
 descrip | character varying(20) | not null
 precio  | numeric(7,2)          | not null
 exist   | integer               | 
Indexes:
    "pk_producto_fp" PRIMARY KEY, btree (fabcod, prodcod)
Referenced by:
    TABLE "pedido" CONSTRAINT "fk_pedido_fp" FOREIGN KEY (fabcod, prodcod) REFERENCES producto(fabcod, prodcod)
```  

**Si intentem fer un DELETE de una tabla que es referenciada per FK de altres tables que la fan referencia.**

```
training=> delete from producto;
ERROR:  update or delete on table "producto" violates foreign key constraint "fk_pedido_fp" on table "pedido"
DETAIL:  Key (fabcod, prodcod)=(rei, 2a45c) is still referenced from table "pedido".
```  

**Pel contrari, una taula que fa referencia (FK) a una PK d'una altra taula i no es referenciada per cap altre taula, la podem borra sense problemes.**
```
training=> delete from pedido;
DELETE 30
```

**Crear (CREATE TABLE)** i **inserir (INSERT INTO):**  
* Primer la part 1 despres la par N.  

**Esborrar(DELETE i DROP):**  
* Primer la part N i despres la part 1. 

**Si una taula te la seva PK referenciada per una FK d'altra taula pero esta buida, no hi haura cap problema;**  

  
`ALTER TABLE  emp add constraint emp_deptno_fk foreign key (deptno) references dept;`  
`ALTER TABLE  emp DROP constraint emp_deptno_fk foreign key (deptno) references dept;`  

```
ALTER TABLE X ADD camp tipus_dades;  
ALTER TABLE X DROP COLUMN camp;
```  
  
`ALTER TABLE X ENABLE|DISABLE CONSTRAINT Y;`  


