# Apuntes Creació i Gestió de Taules DDL EF (Esquema Fiìsic).
[link a pdf tema](https://gitlab.com/jandugar/m02-bd/blob/master/UF2_SQL/1_DDL_Taules_PostgreSQL.pdf
)  
## 1.

**SQL:** Es un llenguatge informatic.  

**SQL**  

* DDL  
* DML  
* DCL  
* Select  

**DDL**  

* Taules  
* Vistes  
* Índex  
  
**Sentencies SQL**  

* Create  
* Drop  
* Alter  
  

DDL|DML
-|:-:
CREATE|INSERT
DROP|DELETE
ALTER|UPDATE
Objectes|Informació




**No es pot duplicar el nom d'un objecte (taula/vista) ni es pot possar un nom reservat a un objecte (create...)**  


El tipus de dades es el com s'enmagatzemen les dades i el tamany d'enmagatzematge.  

**string**, cadena, VARCHAR - bytes tipos de datos de cadena de caracteres **de longitud acotada**  



* VARCHAR  
* INT  
* NUMERIC(e,p) (escala, precisió) (longitud total, part decimal)  
* DATE, TIMESTAMP (epoch, unix time internally)  
⋅⋅* current_date  
⋅⋅* current_timestamp  

**Exemple:** 
**ename VARCHAR(10),**  
nombre de campo, tipo de datos (cadena de caracteres), tamaño(10 bytes)  

INT, 4 bytes  
smallint, 2 bytes  
bigint, 8 bytes  

numeric, exemple  
NUMERIC(5,2) => 0 - 999.99  

hiredate date DEFAULT current_date,  
nom camp, tipus data  



**Reglas de PK**  
* No es pot repetir  
* No pot ser null  
* Ha de ser minima  


**Restriccions**  
* PRIMARY KEY  
* FOREIGN KEY  
* NOT NULL  
* UNIQUE  
* CHECK  


**Exemple constraint:**  
create table dept (
    deptno INTEGER CONSTRAINT dept_deptno_pk PRIMARY KEY,  
...);  
Explicació:  
nom camp, tipus de dades, constraint nom constraint, tipus de constraint  
- constraint a nivell de camp ^  

- contraint a nivell de taula v  

deptno INTEGER,  
...  
CONSTRAINT dept_deptno_pk PRIMARY KEY (deptno)  

**PK compuesta:**  
Se pone a nivel de tabla  
CONSTRAINT animal_codZooCodAnimal_pk PRIMARY KEY (codZoo,codAnimal)  


**FK**  
FK a nivell de camp no menciona FOREIGN KEY, sino REFERENCES  
`... 
deptno INTEGER CONSTRAINT emp_deptno_fk REFERENCES dept (deptno),
...  `
  
CONSTRAINT emp_deptno_fk FOREIGN KEY (deptno) REFERENCES dept (deptno),  

**FK composta**  
CONSTRAINT pedido_fabprod_fk FOREIGN KEY (fabcod,prodcod REFERENCES producto (fabcod,prodcod)  



~Apunte:  
Un telefono, si contempla prefijos o numeros extranjeros, se almacena como varchar, por los mas y parentesis.  

Un nom minim 50 bytes de varchar.