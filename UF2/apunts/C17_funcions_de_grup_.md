# Funcions de grup (aggregate functions/ funcions agregades). 2.  
  
## Group by.  
**Group by se utiliza cuando mezclamos campos normales con campos de función de grupo.**  
Si hacemos lo siguiente (funcion de grupo que resume varios campos mas campo normal):  
```
select deptno, avg(sal)
from emp
;
ERROR:  column "emp.deptno" must appear in the GROUP BY clause or be used in an aggregate function
LINE 1: select deptno, avg(sal)

```
**Todas las columnas mencionadas en la  
SELECT que no son funciones de grupo,  
tienen que estar en la cláusula GROUP BY.**  
  
Por lo tanto, hacemos:  
```
select deptno, avg(sal)
from emp
group by 1;
 deptno |          avg          
--------+-----------------------
     30 | 1566.6666666666666667
     20 | 2175.0000000000000000
     10 | 2916.6666666666666667
(3 rows)

```
Y para que quede mas bonito lo de los decimales, hacemos un **round(field,n decimals)**.
```
select deptno, round(avg(sal),2)
from emp
group by 1;
 deptno |  round  
--------+---------
     30 | 1566.67
     20 | 2175.00
     10 | 2916.67
(3 rows)

```
### Uso de la Cláusula GROUP BYsobre Múltiples Columnas  
Simplemente se incluyen todos los campos que no son de funciones de grupo en el group by.  
```
select deptno, ename, round(avg(sal),2) 
from emp
group by 1,2 
order by 1;
 deptno | ename  |  round  
--------+--------+---------
     10 | CLARK  | 2450.00
     10 | KING   | 5000.00
     10 | MILLER | 1300.00
     20 | FORD   | 3000.00
     20 | SMITH  |  800.00
     20 | JONES  | 2975.00
     20 | ADAMS  | 1100.00
     20 | SCOTT  | 3000.00
     30 | TURNER | 1500.00
     30 | JAMES  |  950.00
     30 | ALLEN  | 1600.00
     30 | BLAKE  | 2850.00
     30 | MARTIN | 1250.00
     30 | WARD   | 1250.00
(14 rows)

```
# Enllaç a les commandes de postgres:  
Llistat de comandes [=>](C2_DDL.md)

#### Exercici: Mostrar per departament el numero d'empleat, salari mes alt, mes baix y la suma dels salaris.  
```
select
        deptno,
        empno,
        max(sal) "Salari mes alt",
        min(sal) "Salari mes baix",
        sum(sal)
from emp
group by 1,2
order by 1
;

```
### Exercici: Mostrar pel tipus de treball, el numero d'empleats que hi ha y el salari mes alt y el mes baix.  
```
select
    job,
    count(*) "Numero d'empleats",
    max(sal) "Salari mes alt",
    min(sal) "Salari mes baix"
from emp
group by 1
order by 2
;
    job    | Numero d'empleats | Salari mes alt | Salari mes baix 
-----------+-------------------+----------------+-----------------
 PRESIDENT |                 1 |        5000.00 |         5000.00
 ANALYST   |                 2 |        3000.00 |         3000.00
 MANAGER   |                 3 |        2975.00 |         2450.00
 CLERK     |                 4 |        1300.00 |          800.00
 SALESMAN  |                 4 |        1600.00 |         1250.00
(5 rows)

```
**Nota:** Order by es la penultima clausula, antes de `limit`.  

**Nota2:** El orden de evaluación del profe para la consulta es (1) el group by es correcto, (2) tiene las funciones de grupo requeridas, (3) todo esta bien compuesto.  
  
#### Cuan utilitzo la clausula ORDER BY es generaran tantes files com valors DIFERENTS tingui el camp d'agrupació.  

```
select deptno, avg(sal)
from emp;
ERROR:  column "emp.deptno" must appear in the GROUP BY clause or be used in an aggregate function
LINE 1: select deptno, avg(sal)
---
select avg(sal)
from emp;
          avg          
-----------------------
 2073.2142857142857143
(1 row)
---
select deptno from emp;
 deptno 
--------
     20
     30
     30
     20
     30
     30
     10
     20
     10
     30
     20
     30
     20
     10
(14 rows)
---------------------
select 
    deptno, 
    round(avg(sal),2) "mitjana salari"
from emp
group by deptno
order by 1;
 deptno | mitjana salari 
--------+----------------
     10 |        2916.67
     20 |        2175.00
     30 |        1566.67
(3 rows)

```
**Nota:** Entran en conflicto la 1 fila del avg con las 14 del deptno. Es decir, las funciones agregadas requieren un **GROUP BY** para hacer subgrupos con los **campos simples** con los que **COMPARTAN EL SELECT**.  



--- V consulta sin enunciado, pero con join ;).  

```
select dname, count(*) "Numero d'empleats"
from emp e join dept d
on e.deptno = d.deptno
group by job, dname;

   dname    | Numero d'empleats 
------------+-------------------
 SALES      |                 1
 SALES      |                 4
 SALES      |                 1
 ACCOUNTING |                 1
 RESEARCH   |                 2
 RESEARCH   |                 2
 ACCOUNTING |                 1
 RESEARCH   |                 1
 ACCOUNTING |                 1
(9 rows)


```
### Consultas con mas de una función de 1 order by.  
```
select 
    deptno, 
    job, 
    sal 
from emp 
order by 1, 2
;
 deptno |    job    |   sal   
--------+-----------+---------
     10 | CLERK     | 1300.00
     10 | MANAGER   | 2450.00
     10 | PRESIDENT | 5000.00
     20 | ANALYST   | 3000.00
     20 | ANALYST   | 3000.00
     20 | CLERK     | 1100.00
     20 | CLERK     |  800.00
     20 | MANAGER   | 2975.00
     30 | CLERK     |  950.00
     30 | MANAGER   | 2850.00
     30 | SALESMAN  | 1250.00
     30 | SALESMAN  | 1250.00
     30 | SALESMAN  | 1500.00
     30 | SALESMAN  | 1600.00
(14 rows)
---
select 
        deptno, 
        job, 
        sum(sal) 
from emp
group by 1,2 
order by 1, 2
;
 deptno |    job    |   sum   
--------+-----------+---------
     10 | CLERK     | 1300.00
     10 | MANAGER   | 2450.00
     10 | PRESIDENT | 5000.00
     20 | ANALYST   | 6000.00
     20 | CLERK     | 1900.00
     20 | MANAGER   | 2975.00
     30 | CLERK     |  950.00
     30 | MANAGER   | 2850.00
     30 | SALESMAN  | 5600.00
(9 rows)


```



## Having para group by.  

No se puede filtrar con un **WHERE** el resultado de una **FUNCIÓN DE GRUPO**, puesto a que aun no se ha ejecutado cuando llegamos al **WHERE**, la solución es usar **HAVING**  
  
**Ordre d'execució:**  
**
1 WHERE  
2 GROUP BY  
3 FUNCIONS DE GRUP  
4 HAVING  
**
  
**Ordre d'execució COMPLET:**  
**
1 FROM  
2 WHERE  
3 GROUP BY  
4 FUNCIÓNS DE GRUP (calcul, pot estar al SELECT o al HAVING)
5 HAVING (filtra els resultat de FUNCIONS DE GRUP AMB LES CONDICIONS)
6 ORDER BY
7 LIMIT
8 SELECT
**
  
```
select 
    deptno, 
    sum(sal) 
from emp 
where sum(sal) > 500 
group by 1
;
ERROR:  aggregate functions are not allowed in WHERE
LINE 1: select deptno, sum(sal) from emp where sum(sal) > 500 group ...
---

select 
    deptno, 
    sum(sal) 
from emp 
group by 1
having sum(sal) > 9000
;
 deptno |   sum    
--------+----------
     30 |  9400.00
     20 | 10875.00
(2 rows)

```
```
SELECT
    deptno, 
    avg(sal)
FROM emp
GROUP BY deptno
HAVING avg(sal)>2000
;
 deptno |          avg          
--------+-----------------------
     20 | 2175.0000000000000000
     10 | 2916.6666666666666667
(2 rows)

```
### Exercici: Mostreu els empleats que mes cobren (top 5).  
```
select ename, sal
from emp
order by 2 desc
limit 5
;
```
### Exercici: escriu l'enunciat per a aquesta consulta.  
```
SELECT job, SUM(sal) “Suma de salaris”
FROM emp
WHERE job NOT LIKE 'SALES%'
GROUP BY job
HAVING SUM(sal)>5000
ORDER BY SUM(sal);
```
**Enunciat proposat:** <ins>dels empleats</ins>, mostra els treballs que no comencin per "Sales" y en els cuals es gasti ento tal per totls els treballadors d'aquest treball mes de 5000€, mostrar els resultats ordenats per el total de salari per treball ascendentment.  

### Exercici: proveu la seguent consulta:  
```
select max(avg(sal))
from emp
group by deptno
;
---
ERROR:  aggregate function calls cannot be nested
LINE 1: select max(avg(sal))

```
**EN POSTGRESQL** no es permet la anidació de **FUNCIONS DE GRUP**. Solució:  

```
SELECT round(max(mitjana),2) "mitjana de departament mes gran"
from ( select avg(sal) mitjana
FROM emp
GROUP BY deptno) taula;
---
 mitjana de departament mes gran 
---------------------------------
                         2916.67
(1 row)


--- ojo:
select avg(sal) mitjana
FROM emp
GROUP BY deptno
 ;
        mitjana        
-----------------------
 1566.6666666666666667
 2175.0000000000000000
 2916.6666666666666667
(3 rows)
---- ^ a estos tres se aplica un max por eso se pasa de 3 a un solo resultado

```
# Ejercicios 5 primeros y 5 ultimos de representantsVendes.pdf, Funcions de grup.