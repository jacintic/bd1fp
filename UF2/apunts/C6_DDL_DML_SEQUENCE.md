# SEQUENCE  

## CREATE SEQUENCE, SYNTAX.  
  
### Ejemplo generico.  
```
create sequence seq_(+nombre campo)  
start with ("n" numero inicial) increment by ("n" numero de incremento);  
```  
### Ejemplo practico, DB habitatge, tabla vivenda, campo idcatastre.  
```
create sequence seq_idcatastre
start with 1 increment by 1;
``` 

#### Notas de create sequence.  

* Es necesario **estar conectado a la base de datos en la que se usara el sequence**.
* Se ha de nombrar sequence como **seq_** + **nombre del campo**.

## NEXT VALUE/ CURR VALUE, SYNTAX.

### Ejemplo generico  
```
(nextval('seq_nombrecampo')
currval('seq_nombrecampo')
```  

### Ejemplo practico, DB habitatge, tabla vivenda, campo idcatastre.  
```
(nextval('seq_idCatastre')  
(currval('seq_idCatastre')  
```  

#### Notas next/currvalue.  

* En el primer `nextval` se aplica el numero original especificado en `create sequence`.
* Es necesario **mantener orden** entre `nextval` y `currval`, de manera que **primero aplicamos/incrementamos el valor**, y seguidamente lo **aplicamos a los campos** en los que la **FK** referencia a ese campo.