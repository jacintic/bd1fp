# INSERT  

* **Insert normal, resultado.**
* * `INSERT 01`  

```
insert into emp (empno,ename,deptno)
values  (1,'jordi',30),
        (2,'anna',20),
        (3,'joanna',30)
;
INSERT 0 3
```

```
insert into dept
values  (nextval('seq_deptno'),'BOOGALOO',' NEW YORK'),
        (nextval('seq_deptno'),'ROCKET SCIENCE','DALLAS'),
        (nextval('seq_deptno'),'SPACE LASERS', 'CHICAGO')
;
```

* **Insert multiple, resultado.**
    * `INSERT 0 "n"`
        * "n" siendo el numero de filas insertadas.

## \d.  

Cuando creamos una tabla, por defecto se asigna al **Schema** **public**.  

```
scott=> \d
            List of relations
 Schema |   Name   | Type  |    Owner    
--------+----------+-------+-------------
 public | dept     | table | iaw14270791
 public | emp      | table | iaw14270791
 public | empleat  | view  | iaw14270791
 public | salgrade | table | iaw14270791
(4 rows)

```
## INSERT MASIU.  

* Resultado de un select como valor de insert a una tabla.  
```
INSERT INTO managers(id,name, salary,hiredate)
    SELECT  empno, ename, sal, hiredate
    FROM    emp
;
```

# CREATE TABLE AS y hacer un insert a la vez.  
```
create table managers(id, nom, salari, data)
    as SELECT  empno, ename, sal, hiredate
    FROM    emp
;
```

### Continuación desde create table as con insert.  
```
delete from managers;
```
* Insert en managers con el contenido de emp.  
  * Version standard SQL (o apta para ORACLE).  
```
INSERT INTO managers (id, nom, salari, data)
    SELECT  empno, ename, sal, hiredate
    FROM    emp
;
```
  * Versión apta para PostgreSQL.
```
INSERT INTO managers
    SELECT  empno, ename, sal, hiredate
    FROM    emp
;
```
### CREATE TABLE AS SELECT. CREAR TABLA NUEVA COMO VACIA FORZANDO UN SELECT VACIO.  

```
create table x
as
        select *
        from emp
        where 1=0
;
```
**Clave:** `where 0=1`, es la condición imposible, no se cumple por lo tanto **el resultado del `select` sale vacio**.
# CREATE SEQUENCE.  
```
create sequence seq_deptno
start with 50 increment by 10
;
```


# UPDATE.  

* Definición: Modifica un campo.  

```
update  emp
ser     deptno = 20
where   empno = 7782
;
```
```
update  emp
ser     deptno = 20
where   empno = 7782
;
```
Afecta a **1 fila** (empno es **PK**).
```
update  emp
ser     deptno = 20
;
```
Afecta a **todas las filas**.
update vivendaxpersona
set vivenda = 3
where vivenda = 2
;

## CONCLUSION UPDATE.  
**! SI VUI MODIFICAR UNA SOLA FILA HE DE FILTRAR (WHERE) POR UNA PK O POR UNA UK.**  

## Ejemplo de update con potencial de error:  
```
update emp
set     deptno = 55
where deptno = 10
;
```
En este insert, si el `deptno` del `set` no existe, obtendremos un error de **integridad referencial**.
## Ejemplo de traducción a idioma humano de un select.
```
update emp
set sal=2000, deptno = 20
where empno = 7782
;
```

^Al empleado con numero de empleado 7782 le modificamos el salario a 2000 y le asignamos al departamento 20.
```
delete from dept
where   deptno = 40
;
```

^Borramos el departamento 40.  

```
selete from     dept
;
```
^Borramos todos los departamentos.  


# DELETE tablas con PK referenciada por otras tablas. Integritat referencial.

```
scott=> delete from dept
scott-> where deptno = 10;
ERROR:  update or delete on table "dept" violates foreign key constraint "emp_deptno_fk" on table "emp"
DETAIL:  Key (deptno)=(10) is still referenced from table "emp".

```

# DDL y DML, similitud.
Las reglas de **Integritat referencial se cumplen en los dos lenguajes**.
* DDL
* * create
* * alter
* * drop
* DML
* * insert
* * update
* * delete

#### Atajo teclado Atom para previsualizar markdown `Ctrl + Shift + m`
