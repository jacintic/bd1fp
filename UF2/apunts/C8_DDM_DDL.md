# Comportament de le CAS  

```
INSERT
VALUES
```
```
DELETE
WHERE
```
```
UPDATE 
SET
WHERE
```
```
SELECT
FROM
WHERE
```  

Cada linea es una **CLAUSULA**.  

* Insert tipo **implicito**
```
insert into emp (ename, empno, deptno,sal)
values 			('SANZ', 1657, 30, 3000)
;
```

* Insert tipo **explicito**
```
insert into emp (ename, empno, deptno,sal,job, hiredate, comm)
values 			('SANZ', 1657, 30, 3000, NULL, NULL, NULL)
;
```


* El **upper(campo)/ lower(campo)** el profe lo quiere ne la clausula **WHERE**.


* Insert sin lista de campos funciona en postgres pero no en la mayoria de otros **SGBD**
    * Por lo tanto la sequencia de valores es **OBLIGATORIA**.


* En **psql** con el atributo **-c** podemos incluir una consulta a ejecutar al final del script.

```

```

* **\x** expand, sale la información por fila separada en columnas, de forma que no se corta en una ventana pequeña.

[Link to \commands for PostgreSQL](C2_DDL.md)  



* Els camps sense valors o **NULL** son problematics quan fem:
    * comparacions
    * operacions aritmetiques
    * joins
```
camp * NULL = NULL
```
* Com evito aixo?

Funció **coalesce**.
```
 coalesce(camp, valor neutre)
```
```
select 
    ename, 
    sal, 
    coalesce(comm,0) 
from emp
;

```

* **Error d'integritat referencial**.

```
delete from dept
where deptno = 30
;
```

* No pots borrar **PK** deptno = 30 per que es una **FK** en us a la taula emp, es a dir la taula **emp** conte files amb **deptno = 30**.
* Per tant, s'ha de borrar les files que fan referencia a aquest departament abans de poder borrar el departament en si (esborro la part **N** i despres esborro la part **1**).

```
delete from emp
where deptno = 30
;
delete from dept
where deptno = 30
;
```

## Comportament de CAs

* ON UPDATE/DELETE de les claus primaries, afectara a les claus alienes (part 1 => part N):
    * RESTRICT(default)
    * CASCADE(propagació a claus alienes)
    * SET NULL
* ON DELETE/UPDATE CASCADE
    * cuan hi ha un UPDATE/DELETE

**Exercici:** l'esborrat dels departaments sigui posar a null y que l'actualitazció dels departaments sigui en cascada.



