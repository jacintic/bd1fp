# Clausules  
  
* where     -camps y operacions aritmetiques
* from      -tablas
* where     -condicions

# Diferencia entre operadores y operandos  
  
* Operadors
    * aritmètics    
        * +
        * -
        * *
        * /
    * logics
        * AND
        * OR
        * NOT
    * relacionals o de comparació
        * =
        * != 
        * `<>`
        * <
        * >

* Operadors en funció d'operants
    * unaris
        * -
        * +
        * NOT
        * ...
    * binaris
        * +
        * -
        * `*`
        * AND
        * IN
        * ...
  
## AND y OR  
  
* OR    -suma
* AND   -multiplica (te mes prioritat que el OR)

### AND OR EXCEPTIONS  
Esta consulta solo aplicara el AND a la condición anterior, es decir, solo a PRESIDENT, SALESMAN puede cobrar menos de 1500.  
```
select ename, job, sal
from emp
where job = 'SALESMAN'
or job = 'PRESIDENT'
and sal > 1500
;
Esta consulta solo aplicara el AND a los dos, PRESIDENT y SALESMAN.  
```
select ename, job, sal
from emp
where (job = 'SALESMAN'
or job = 'PRESIDENT')
and sal > 1500
;
```
```
## OPERANTS  
`sal <= 1000`  
* operants:
    * sal
    * 1000

# DISTINCT  
Filtra repetidos (filas).  
```
select distinct deptno 
from dept 
order by 1
;
```
  
## Exercici.  
  
Mostreu els empleats que cobren entre 2000 i 4000.  
```
select *
from emp
where sal >= 2000 and sal <= 4000
;
```
#### Amb BETWEEN  
```
select *
from emp
where sal BETWEEN 2000 AND 4000
;
```
# BETWEEN  
```
select *
from emp
where sal BETWEEN 2000 AND 4000
;
```
# IN  
```
select *
from emp
where sal = 2975 or sal = 2850 or sal = 2450
```
```
select *
from emp
where sal IN (2975 ,2850 ,2450)
```
  
  
## IN = ANY  
## NOT IN != ALL  
* No funciona pero es el ejemplo de sintaxis.
```
select ename, job
from emp
where job != ALL ('CLERK', 'MANAGER', 'ANALYST')
;
```
# ORDER BY  
```
select distinct(deptno) 
from dept 
order by 1
;
```
  
# LIKE  
Opeador busqueda de cadenas de caracteres.  
  
## Metacaracter `%`
`%` Representa cualquier caracter  
```
select ename
from emp
where ename like 'A%'
;
```
## Metacaracter `_`  
```
select ename
from emp
where ename like '_A%'
;
```
  
# NULL
* IS NULL
* IS NOT NULL