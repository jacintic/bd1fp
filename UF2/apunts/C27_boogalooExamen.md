# Ejercicios examen.  
  
```
repventa left join pedido
```
saldran las 30 columnas de pedido mas las peduidas de los que NO han hecho pedidos, por lo que hay que hacer es un count especifico de p.*



## Tabla temporal  
```
select 	e.nombre "Nombre empleado",
		coalesce(j.nombre, 'Sin jefe') "Nombre jefe de empleado",
		count(p.*) "Numero de pedidos",
    cli.clients "Numero de clientes contactados"       
from repventa e
	left join repventa j
		on e.jefe = j.repcod
	left join pedido p
		on p.repcod = e.repcod
  left join (select repcod, count(*) clients
  from cliente
  group by repcod) cli  on cli.repcod = e.repcod
group by e.nombre, j.nombre, cli.clients
 order by 1;
```
## Mostrar per cada lloc de treball el numero d'empleats.  

```
select
        analyst,
        clerk,
        manager,
        president,
        salesman
from (  select count(*) analyst
        from emp
        where lower(job) = 'analyst') a,
     (  select count(*) manager
        from emp
        where lower(job) = 'manager') b,
    (   select count(*) clerk
        from emp
        where lower(job) = 'clerk') c,
    (   select count(*) president
        from emp
        where lower(job) = 'president') d,
     (  select count(*) salesman
        from emp
        where lower(job) = 'salesman') e;

```
#### Con CASE.  

```
select sum(case
when lower(job) = 'analyst'
then 1
else 0
end) analyst,
sum(case
when lower(job) = 'clerk'
then 1
else 0
end) clerk,
sum(case
when lower(job) = 'manager'
then 1
else 0
end) manager,
sum(case
when lower(job) = 'president'
then 1
else 0
end) president,
sum(case
when lower(job) = 'salesman'
then 1
else 0
end) salesman
from emp;
```
### Con CASE y COUNT (cuenta si no es NULL).  
```
select count(case
when lower(job) = 'analyst'
then 1
else null
end) analyst,
sum(case
when lower(job) = 'clerk'
then 1
else 0
end) clerk,
sum(case
when lower(job) = 'manager'
then 1
else 0
end) manager,
sum(case
when lower(job) = 'president'
then 1
else 0
end) president,
sum(case
when lower(job) = 'salesman'
then 1
else 0
end) salesman
from emp;
```
