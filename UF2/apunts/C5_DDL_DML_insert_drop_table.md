# Clase 3 DDL i DML  

## Castings.  

* implicit
    Fet pel sistema gestor.  
* explicit
    Fet per l'administrador de base de dates.  

## Inserts sin valores.  

* Postgres admite inserts que omiten un valor en una tabla.
* Oracle, no admite omitir valores.
  
**Por lo tanto** cuando hacemos un **INSERT**, hemos de introducir **la misma cantidad de valores que campos hay en la tabla**.

## Inserts en solo algunos campos especificos.  

* Insert con valores y NULLs.
* Insertr con valores relaccionados a campos.

```
insert into emp  
    (empno,ename,deptno)  
    values (9234, 'Francis', 10);
```  

## Mensaje `INSERT 0 1`  
`0` significa OK, todo correcto, `1` significa 1 fila insertada.  
  
  
## Practicar `DROP TABLE` antes de hacer un `DROP DATABASE`. **!En el examen caera segurisimo.**  
  
```
drop table if exists vivendaxpersona ;  
drop table if exists persona ;  
drop table if exists vivenda ;  
drop table if exists municipi;
```  
  
Primero eliminamos las partes con **FK** que no son referenciadas por otras tablas, borrar una tabla que se auto referenica es posible sin problemas.
  
  
## `INSERT` Implicito vs Explicito.
  
* Implicito:
* * Especificar los campos que si introducimos y omitir los que no (que seran NULL).

```
insert into emp  
    (empno,ename,deptno)  
    values (9234, 'Francis', 10);
```  
* Explicito:
* * Especificar el valor NULL en el campo, no listaremos el nombre o orden de los campos (por lo que el orden sera el definido pro el create table), especificaremos el valor NULL de cada campo.

```
insert into emp  
    values (9234,NULL, NULL, 'Francis', 10);
```  
  
  
## `current_date` i `current_timestamp`  
En **Oracle** el equivalente a `current_date` es `SYSDATE`.  
- Cliente de **sqlplus**.
  
### **INSERT** en campo tipo **date**.  
```
insert into emp  
(empno,ename,deptno,hiredate)  
values (576, 'Manolo', 10, current_date);
```  
  
  
