# Clase 4 DDL i DML  

* create sequence  
* DML => inso7  

## DROP TABLE EN ORDRE  
  
**Ordres SQL multi valor SQL equivalents** in es mas optimo para multi valor
* `estat='O' OR estat='T'`  
* `estat in ('O','T')  
  
**Cometes dobles `""` nomes s'utilitzen per alias de camp, cometes simples "''" per a strings**  


## INSERTS  

* **Exemple**  
```
INSERT INTO zoo 
VALUES (1, 'Barcelona', 'Espanya', 3500.20, 6000000.54);
```  


### DDL, DML i DCL son!... llenguatges  

* **DML:** Llenguatge SQL utilitzat per gestionar Informació (contingut)
* * **insert**
* * **update**
* * **delete**
* **DDL:** Llenguatge SQL utilitzat per gestionar estructures
* * **create**
* * **alter**
* * **drop**

```
insert into taula  
values (llista, de , valors);
```

* **format de dades dins de l'insert**
* * **text o data:** `''`
* * **numero:** `no`
* * **boolean:** `no`


* **ERROR INSERT**
Este error se da cuando hacemos un insert de un campo que es una clave ajena, la cual no existe.  
```
    idCatastre int,  
    direcio varchar(150),  
    m2 numeric(6,2),  
    municipi int,  
    CONSTRAINT PK_VIVENDA_IDCATASTRE PRIMARY KEY(idCatastre),  
    CONSTRAINT FK_VIVENDA_MUNICIPI FOREIGN KEY(municipi) REFERENCES municipi(idMunicipi)
```  
Este error se da cuando hacemos un insert de un tipo varchar en un campo tipo int.  
```
psql:/home/users/inf/hiaw1/iaw14270791/Desktop/Clase/git/bd/iglesiasjacint/UF2/scripts/habitatge.sql:79: ERROR:  invalid input syntax for integer: "Barcelona"
LINE 2: VALUES(69, 'Atic 4ª', 60.50, 'Barcelona');
```  