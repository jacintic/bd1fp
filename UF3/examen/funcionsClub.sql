--() { :; }; exec psql club -f "$0"

-- iaw14270791
-- funcions club
-- Jacint Iglesias
-- UF3

\c club

/*
- funció reservarPista (p_idusuari,p_dia, p_hora, p_pista, p_durada numeric(2.1)) Reserva una p_pista
    segons el que s'explica a continuació.
    - funció confirmar (p_idusuari). Quan l'usuari arriba poc abans al club i passa pel
    mostrador, s'identifica i es cobra la reserva (s'afegeix el preu de la reserva)
    - funció anularPista (p_id)
*/


/*
Un club de pàdel vol gestionar les reserves telemàticament a través d'una app web.

Cada pista té un id i dos preus segons si es hora vall o hora punta.

- La franja vall representa una hora amb menys afluència, amb el què serà més barata.
Va de dilluns a divendres de 7 del matí fins abans de les 17 h i caps de
 setmana des de 14 a 21 hores. Els festius tenen la mateixa consideració que el cap de
 setmana.

- L'hora punta serà més cara per una demanda més alta a aquelles hores. Va de
dilluns a divendres de des les 17 a 21 hores, caps de setmana de 7 fins
 abans de les 14 hores. Els festius tenen la mateixa consideració que el cap de
 setmana.

Una reserva de padel podrà ser d'una hora o d'hora i mitja, segons el que desitgi
l'usuari del club.

Les franges de reserva son de 30 minuts. La taula franja

D'usuaris hi ha de dos tipus, els que son abonats i els que no.
- Els que son NO abonats podran reservar  48 hores abans del dia i hora a que vol jugar.
- Els abonats podran reservar fins a 72 hores abans del dia i hora a que vol jugar.

Un soci podrà fer una reserva per un dia de la setmana si en aquella setmana no
té una reserva que encara no ha consumit. Amb un exemple seria el següent:

- Un usuari el dilluns reserva pel dimecres i el dijous vol reservar pel dissabte.
Com ja ha consumit la seva reserva, el sistema li deixa reservar (evidentment altra cosa
és que hi hagi disponibilitat)
- Un usuari el dilluns reserva pel divendres per jugar. El dijous vol reservar pel
dissabte però l'app li ha de contestar que no pot perquè té una reserva pendent de
consumir.

IMPORTANT (1) No us poso les restriccions de clau aliena per tenir major llibertat a l'hora
de programar, però s'han de tenir en compte. Podeu fer JOINS entre els camps que
es diuen igual.

IMPORTANT (2): Si hi ha errors sintàctics no es corregirà l'examen
*/


--  idpista | preuhoravall | preuhorapunta 
/*
- La franja vall representa una hora amb menys afluència, amb el què serà més barata.
Va de dilluns a divendres de 7 del matí fins abans de les 17 h i caps de
 setmana des de 14 a 21 hores. Els festius tenen la mateixa consideració que el cap de
 setmana.

- L'hora punta serà més cara per una demanda més alta a aquelles hores. Va de
dilluns a divendres de des les 17 a 21 hores, caps de setmana de 7 fins
 abans de les 14 hores. Els festius tenen la mateixa consideració que el cap de
 setmana.
*/



-- mirar si usuari es abonat o no
-- en funcio de si ho es aplicar comprovació horaria de 48 hores avans o 72 hores avans

--mirar reserva NO CONSUMIDA en aquesta semana
/*
\d reserva
                                         Table "public.reserva"
   Column    |            Type             |                          Modifiers                          
-------------+-----------------------------+-------------------------------------------------------------
 idreserva   | integer                     | not null default nextval('reserva_idreserva_seq'::regclass)
 idpista     | integer                     | 
 dia         | date                        | 
 horaini     | character varying(4)        | 
 durada      | numeric(2,1)                | 
 idusuari    | integer                     | 
 datareserva | timestamp without time zone | 
 import      | numeric(4,2)                | 

*/

--select  setval('reserva_idreserva_seq', 1); -- provar sin int cast



-- funcio comprova si l'usuari esta abonat
create or replace function usuariAbonat(p_idusuari integer) 
returns boolean
as $$
	declare 
		v_abonat boolean;
	begin
		begin
			select abonat
			into strict v_abonat
			from usuari
			where idusuari = p_idusuari;
			exception
				when no_data_found then
					v_abonat := false;
		end;
	return v_abonat;
	end;
$$ language plpgsql;

-- funcio comprova que la reserva estigui dins del rang de reserva per l'usuari
create or replace function horaReservaCheck(p_dia date, p_hora varchar(4),v_abonat boolean) 
returns boolean
as $$
	declare
		v_dies int;
		v_hores int;
	begin
		select (to_char(current_date,'dd')::int - to_char(p_dia,'dd')::int)::int ,
			(to_char(current_timestamp,'hh24')::int - substr(p_hora,1,2)::int) 
		into v_dies,v_hores;
		case v_abonat
			when true then
				if v_dies > 3 then
					return false;
				elsif v_dies = 3 and v_hores > 1 then
					return false;
				end if;
			when false then
				if v_dies > 2 then
					return false;
				elsif v_dies = 2 and v_hores > 1 then
				end if;
			else
		end case;
		return true;
	end;
$$ language plpgsql;



-- funcio comprova que no hi hagi una altr areserva no consumida dins de la mateixa setmana
create or replace function reservaSameweek(p_idusuari integer, p_dia date) 
returns boolean
as $$
	declare
		v_num_reserves int;
	begin
		select count(*) 
		into v_num_reserves
		from reserva
		where idusuari = p_idusuari
		and to_char(datareserva,'mmyyyy') = to_char(p_dia,'mmyyyy')
		and to_char(datareserva,'w') = to_char(p_dia,'w');
	return v_num_reserves = 0;	
	end;
$$ language plpgsql;


/*
 
-------------+-----------------------------+------------------------------------------------------------
-
 idreserva   | integer                     | not null default nextval('reserva_idreserva_seq'::regclass)
 idpista     | integer                     | 
 dia         | date                        | 
 horaini     | character varying(4)        | 
 durada      | numeric(2,1)                | 
 idusuari    | integer                     | 
 datareserva | timestamp without time zone | 
 import      | numeric(4,2)                | 
Indexes:

*/


-- comprovar col·lisions amb reserva
create or replace function reservaColision(p_dia date, p_hora varchar(4), p_pista integer, p_durada numeric(2,1)) 
returns boolean
as $$
	declare
		v_hora1 varchar(4);
		v_hora2 varchar(4);
		v_idreserva int;
		v_resutl boolean := false;
		v_hora_reservada varchar(4);
		v_message varchar(500);
		v_durada_reserva_colisio numeric(3,1);
	begin
	
	--select to_timestamp('0830','HH24MI');
	-- select to_char(to_timestamp('0830','HH24MI'),'HH24MI');
		
		select 
			to_char(to_timestamp(p_dia || p_hora,'yyyy-mm-ddHH24MI') + interval '30 minutes','HH24MI'),
			to_char(to_timestamp(p_dia || p_hora,'yyyy-mm-ddHH24MI') + interval '1 hour','HH24MI')
		into v_hora1,v_hora2;
		
		case p_durada
			when 1.5 then
				begin	
					select idreserva
					into strict v_idreserva
					from franja
					where idpista = p_pista
					and dia = p_dia
					and hora in (p_hora,v_hora1,v_hora2)
					and idreserva is not null
					limit 1;
					exception 
						when no_data_found then
							v_resutl := true;
				end;
			when 1 then
				begin	
					select idreserva
					into strict v_idreserva
					from franja
					where idpista = p_pista
					and dia = p_dia
					and hora in (p_hora,v_hora1)
					and idreserva is not null
					limit 1;
					exception 
						when no_data_found then
							v_resutl := true;
				end;
			else 
				v_message := e' No es pot determinar si la pista esta lliure per reservar.\n Aquesta es la informació:\nData: '|| p_dia|| ' ,Hora: '|| p_hora||' ,Pista: '|| p_pista||' ,Durada: '|| p_durada;
				raise exception '%',v_message;
		end case;
		if not v_resutl then
			select hora 
			into v_hora_reservada
			from franja
			where idreserva = v_idreserva
			limit 1;
			select durada
			into v_durada_reserva_colisio
			from reserva 
			where idreserva = v_idreserva;
			v_message := ' hi ha una pista ja reservada dins d''aquest horari. Hora de reserva amb col·lisió hora d''inici '|| v_hora_reservada || ' amb una durada de ' || v_durada_reserva_colisio || ' hores.';
			raise exception '%',v_message;
		end if;
		return v_resutl;
	end;
$$ language plpgsql;



-- funcion get valley or peak price
create or replace function getPreu(p_preu varchar(20),p_pista int) 
returns numeric
as $$
	declare
		v_preu_final numeric(4,2);
	begin
		case p_preu
			when 'preuhoravall' then 
				select preuhoravall
				into v_preu_final
				from pista
				where idpista = p_pista;
			else 
				select preuhorapunta
				into v_preu_final
				from pista
				where idpista = p_pista;
		end case;
	return v_preu_final;
	end;
$$ language plpgsql;

--  main function
create or replace function reservarPista(p_idusuari integer,p_dia date, p_hora varchar(4), p_pista integer, p_durada numeric(2,1)) 
returns varchar
as $$
	declare
		v_abonat boolean;
		v_datareserva_in_range boolean;
		v_reserva_same_week boolean;
		v_hora int;
		v_minut int;
		v_pista_reservada boolean;
		v_message varchar(200) = 'Tot correcte';
		v_preu numeric(4,2);
		v_diafestiu date;
		v_reserva_colision boolean;
		v_hora1 varchar(4);
		v_hora2 varchar(4);
		v_pista int;
		v_dia date;
		v_hora_2 varchar(4);
		v_update boolean := true;
		v_idreserva int;
	begin
		-- enmagatzema si l'usuari esta abonat (true false)
		v_abonat := usuariAbonat(p_idusuari);
		-- comprova si l'horari de reserva esta dins del rang (depenent si usuari es abonat ono)
		v_datareserva_in_range := horaReservaCheck(p_dia,p_hora,v_abonat);
		-- comprovar cas de reserva no consumida la mateixa setmana
		v_reserva_same_week := reservaSameweek(p_idusuari,p_dia);
		-- comprovar si la reserva es pot realitzar (no hi han hores reservades que col·lisionin)
		-- Nota: si hi ha col·lisió s'aborta l'operació amb un raise exception
		v_reserva_colision := reservaColision(p_dia,p_hora,p_pista,p_durada);
/*
- La franja vall representa una hora amb menys afluència, amb el què serà més barata.
Va de dilluns a divendres de 7 del matí fins abans de les 17 h 

i caps de
 setmana des de 14 a 21 hores. Els festius tenen la mateixa consideració que el cap de
 setmana.

- L'hora punta serà més cara per una demanda més alta a aquelles hores. Va de

dilluns a divendres de des les 17 a 21 hores, 

caps de setmana de 7 fins
 abans de les 14 hores. 
 
 Els festius tenen la mateixa consideració que el cap de
 setmana.
*/
		/* validar si el dia es cap de setmana */
		begin
			select diafestiu
			into strict v_diafestiu
			from festius
			where diafestiu = p_dia;
			-- aquest error no s'hauria dentrar mai per la condició
			-- del select mateix que sempre retornara true or false
			exception
				when no_data_found then
					v_diafestiu := (current_date - interval '100 years')::date;
		end;
		case 
			-- hora vall entre setmana
			when to_char(p_dia,'d')::int between 2 and 6 and substr(p_hora,1,2)::int between 7 and 16 then
				v_preu := getPreu('preuhoravall', p_pista);
			-- hora vall cap de setmana o festiu
			when to_char(p_dia,'d')::int in (1,7) and substr(p_hora,1,2)::int between 14 and 20 or
			-- hora vall i festiu
			to_char(p_dia,'d')::int in (1,7) and substr(p_hora,1,2)::int between 14 and 20 and v_diafestiu != (current_date - interval '100 years')::date then
				v_preu := getPreu('preuhoravall', p_pista);
			-- hora punta dia entre setmana
			when to_char(p_dia,'d')::int between 2 and 6 and substr(p_hora,1,2)::int between 17 and 20 then
				v_preu := getPreu('preuhorapunta', p_pista);
			-- hora punta cap de setmana o festiu
			when to_char(p_dia,'d')::int in (1,7) and substr(p_hora,1,2)::int between 7 and 13 or
			-- hora punta i festiu
			to_char(p_dia,'d')::int in (1,7) and substr(p_hora,1,2)::int between 7 and 13 and v_diafestiu != (current_date - interval '100 years')::date then
				v_preu := getPreu('preuhorapunta', p_pista);
			-- hora i dia fora de rang
			else 
				v_message := 'Error: Dia i hora estan fora de rang de l''horari club. Horari del club:';
				v_message := v_message || e'\n. Dilluns a divendres i caps de setmana de 7 fins les 21 hores.\n'|| 'Has intentat reservar el dia ' ||p_dia || ' a l''hora '||p_hora;
				return v_message;			
		end case;
		case 
			when not v_datareserva_in_range then
				v_message := 'Error: la reserva es fora del termini maxim de reserva (48 horas per no abonats i 72 per abonats)';
			when not v_reserva_same_week then
				v_message:= 'Error: aquest usuari ja te una altra reserva activa aquesta setmana';
			else
				-- fer l'insert de reserva
				insert into reserva (idreserva,idpista,dia,horaini,durada,idusuari,datareserva,import)
					(select nextval('reserva_idreserva_seq'),p_pista,p_dia,p_hora,p_durada,p_idusuari,current_timestamp,null);
				-- get the value of the reserva id
				select idreserva
				into v_idreserva
				from reserva
				where idpista = p_pista and dia = p_dia and horaini = p_hora and durada = p_durada and idusuari = p_idusuari;
				--  operacions per fer l'insert de franja
				-- obtenció de les hores que cobreix la reserva
				select 
					to_char(to_timestamp(p_dia || p_hora,'yyyy-mm-ddHH24MI') + interval '30 minutes','HH24MI'),
					to_char(to_timestamp(p_dia || p_hora,'yyyy-mm-ddHH24MI') + interval '1 hour','HH24MI')
				into v_hora1,v_hora2;
				-- borrat de hores especifiques per evitar xequejar update o insert i fer sempre insert
				delete from franja
				where idpista = p_pista and dia = p_dia and hora in (p_hora,v_hora1);
				-- borrat en cas de durada maxima
				if p_durada = 1.5 then
					delete from franja
					where idpista = p_pista and dia = p_dia and hora in (v_hora2);
				end if;
				-- insert de franja
				insert into franja (idpista,dia,hora,idreserva)
					values	(p_pista,p_dia,p_hora,v_idreserva),
							(p_pista,p_dia,v_hora1,v_idreserva);
				-- insert de franja en cas de durada maxima			
				if p_durada = 1.5 then
						insert into franja (idpista,dia,hora,idreserva)
							values	(p_pista,p_dia,v_hora2,v_idreserva);
				end if;
		end case;
	return v_message;
	end;
$$ language plpgsql;

/*
- La franja vall representa una hora amb menys afluència, amb el què serà més barata.
Va de dilluns a divendres de 7 del matí fins abans de les 17 h i caps de
 setmana des de 14 a 21 hores. Els festius tenen la mateixa consideració que el cap de
 setmana.

- L'hora punta serà més cara per una demanda més alta a aquelles hores. Va de
dilluns a divendres de des les 17 a 21 hores, caps de setmana de 7 fins
 abans de les 14 hores. Els festius tenen la mateixa consideració que el cap de
 setmana.
*/

/*
- funció confirmar (p_idusuari). Quan l'usuari arriba poc abans al club i passa pel
  mostrador, s'identifica i es cobra la reserva (s'afegeix el preu de la reserva)
  - funció anularPista (p_id)
*/    
create or replace function confirmar(p_idusuari integer) 
returns varchar
as $$
	declare
		v_message varchar(500) := 'Pista reservada correctament.';
		v_pista int;
		v_dia date;
		v_hora varchar(4);
		v_durada numeric(2,1);
		v_diafestiu date;
		v_preu numeric(4,2);
		v_hora1 varchar(4);
		v_hora2	varchar(4);
	begin
		-- get reserva values
		select idpista,dia, horaini, durada
		into v_pista, v_dia, v_hora, v_durada
		from reserva
		where idusuari = p_idusuari
		and dia = current_date;
		if v_dia is null then
			return 'Error: L''usuari no te cap reserva a dia d''avui.';
		end if;
		
		-- get hours (relative to one hour or hour and a half duration)
		select 
			to_char(to_timestamp(v_dia || v_hora,'yyyy-mm-ddHH24MI') + interval '30 minutes','HH24MI'),
			to_char(to_timestamp(v_dia || v_hora,'yyyy-mm-ddHH24MI') + interval '1 hour','HH24MI')
		into v_hora1,v_hora2;
		-- condició de dia festiu
		begin
			select diafestiu
			into strict v_diafestiu
			from festius
			where diafestiu = v_dia;
			-- aquest error no s'hauria dentrar mai per la condició
			-- del select mateix que sempre retornara true or false
			exception
				when no_data_found then
					v_diafestiu := (current_date - interval '100 years')::date;
		end;
		case v_durada
			-- reserva amb condició durada hora i mitja (comprova que l'hora de fi es dins del rang)
			when 1.5 then 
				case 
					-- hora vall entre setmana
					when to_char(v_dia,'d')::int between 2 and 6 and substr(v_hora,1,2)::int between 7 and 16 and  substr(v_hora2,1,2)::int between 7 and 16 then
						v_preu := getPreu('preuhoravall', v_pista);
					-- hora vall cap de setmana o festiu
					when to_char(v_dia,'d')::int in (1,7) and substr(v_hora,1,2)::int between 14 and 20 and  substr(v_hora2,1,2)::int between 14 and 20  or
					-- hora vall i festiu
					to_char(v_dia,'d')::int in (1,7) and substr(v_hora,1,2)::int between 14 and 20 and  substr(v_hora2,1,2)::int between 14 and 20  and v_diafestiu != (current_date - interval '100 years')::date then
						v_preu := getPreu('preuhoravall', v_pista);
					-- hora punta dia entre setmana
					when to_char(v_dia,'d')::int between 2 and 6 and substr(v_hora,1,2)::int between 17 and 20 or  substr(v_hora2,1,2)::int between 17 and 20  then
						v_preu := getPreu('preuhorapunta', v_pista);
					-- hora punta cap de setmana o festiu
					when to_char(v_dia,'d')::int in (1,7) and substr(v_hora,1,2)::int between 7 and 13 or  substr(v_hora2,1,2)::int between 7 and 13  or
					-- hora punta i festiu
					to_char(v_dia,'d')::int in (1,7) and substr(v_hora,1,2)::int between 7 and 13 or  substr(v_hora2,1,2)::int between 7 and 13  and v_diafestiu != (current_date - interval '100 years')::date then
						v_preu := getPreu('preuhorapunta', v_pista);
					-- hora i dia fora de rang
					else 
						v_message := 'Error: Dia i hora estan fora de rang de l''horari club. Horari del club:';
						v_message := v_message || e'\n. Dilluns a divendres i caps de setmana de 7 fins les 21 hores.\n'|| 'Has intentat reservar el dia ' ||p_dia || ' a l''hora '||p_hora;
						return v_message;			
				end case;
			-- reserva amb condició d'horari dintre
			else 
				case 
					-- hora vall entre setmana
					when to_char(v_dia,'d')::int between 2 and 6 and substr(v_hora,1,2)::int between 7 and 16 and  substr(v_hora1,1,2)::int between 7 and 16 then
						v_preu := getPreu('preuhoravall', v_pista);
					-- hora vall cap de setmana o festiu
					when to_char(v_dia,'d')::int in (1,7) and substr(v_hora,1,2)::int between 14 and 20 and  substr(v_hora1,1,2)::int between 14 and 20  or
					-- hora vall i festiu
					to_char(v_dia,'d')::int in (1,7) and substr(v_hora,1,2)::int between 14 and 20 and  substr(v_hora1,1,2)::int between 14 and 20  and v_diafestiu != (current_date - interval '100 years')::date then
						v_preu := getPreu('preuhoravall', v_pista);
					-- hora punta dia entre setmana
					when to_char(v_dia,'d')::int between 2 and 6 and substr(v_hora,1,2)::int between 17 and 20 or  substr(v_hora1,1,2)::int between 17 and 20  then
						v_preu := getPreu('preuhorapunta', v_pista);
					-- hora punta cap de setmana o festiu
					when to_char(v_dia,'d')::int in (1,7) and substr(v_hora,1,2)::int between 7 and 13 or  substr(v_hora1,1,2)::int between 7 and 13  or
					-- hora punta i festiu
					to_char(v_dia,'d')::int in (1,7) and substr(v_hora,1,2)::int between 7 and 13 or  substr(v_hora1,1,2)::int between 7 and 13  and v_diafestiu != (current_date - interval '100 years')::date then
						v_preu := getPreu('preuhorapunta', v_pista);
					-- hora i dia fora de rang
					else 
						v_message := 'Error: Dia i hora estan fora de rang de l''horari club. Horari del club:';
						return v_message;			
				end case;
		end case;
		-- afegir el preu a la reserva si les dades son valides
			update reserva
				set import = v_preu
			where idusuari = p_idusuari
			and dia = v_dia
			and horaini = v_hora;
	return v_message;
	end;
$$ language plpgsql;
