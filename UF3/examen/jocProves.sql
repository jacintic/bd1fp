--() { :; }; exec psql club -f "$0"

-- iaw14270791
-- funcions triggers
-- Jacint Iglesias
-- UF3

\c club

-- no m'ha donat temps
-- reservarPista(p_idusuari integer,p_dia date, p_hora varchar(4), p_pista integer, p_durada numeric(2,1)) 

select reservarPista(1,current_date, '1030',1,1);

-- comprovació col·lisió reserves
select reservarPista(1,current_date, '1100',1,1.5);

-- horari punta

select reservarPista(2,current_date, '1800',2,1.5);

select import "preu resrva hora punta" from reserva where idreserva in (2,3);


-- usuari no abonat 48 hores avans o 72 hores avans
-- no abonat (48 hores)

-- fora de rang
select reservarPista(3,(current_date - interval '49 hours')::date, '1800',2,1.5);



-- dins de rang
select reservarPista(3,(current_date - interval '46 hours')::date, '1800',2,1.5);

-- fen insert per usuari abonat
insert into usuari (idusuari,nom,cognoms,abonat) 
	values(4,'Pep','Romani',true);
	
-- fora de rang 72 hores
select reservarPista(4,(current_date - interval '73 hours')::date, '1800',2,1.5);	

-- dins de rang (72 hores)
select reservarPista(4,(current_date - interval '71 hours')::date, '1800',2,1.5);	



-- reserva same week
select reservarPista(5,current_date, '1200',2,1.5);	
select reservarPista(5,(current_date + interval '5 days')::date, '1200',2,1.5);	
-- reserva different week
select reservarPista(5,(current_date + interval '7 days')::date, '1200',2,1.5);	



-- hora vall
select reservarPista(6,current_date, '1600',2,1);	

-- hora punta
select reservarPista(7,(current_date + interval '1 day')::date, '0800',2,1);	
-- hora fora de rang
select reservarPista(8,current_date, '2300',2,1);


select * from reserva where idusuari in (6,7,8);


-- TEST CONFRIMAR RESERVA -- 
select confirmar(1);
select confirmar(6);
select confirmar(5);
select confirmar(2);

-- aquest salta per que no esta feta avui la reserva
select confirmar(4);
