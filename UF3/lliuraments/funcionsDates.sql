-- iaw14270791
-- funcions dates
-- Jacint Iglesias
-- UF3
\c bdfuncions


-- 1. funció myEpoch()

create or replace function myEpoch()
returns varchar
as $$
	declare
		interval_ts varchar;
		result_time varchar;
	begin
		interval_ts := (current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::text;
		result_time := (date_part('days',current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::numeric * 60 * 60 * 24 + date_part('hours',current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::numeric * 60 * 60 + date_part('minutes',current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::numeric * 60 + date_part('seconds',current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::numeric - 3600)::text;
		return result_time;
	end;
$$ language plpgsql;


-- 2. funció darrerDiaMes()

create or replace function darrerDiaMes()
returns varchar
as $$
	begin
		return to_char((current_date + interval '1 month')::date - (to_char(current_date,'dd'))::int,'dd');
	end;
$$ language plpgsql;

select darrerDiaMes();

-- 3. funció darrerDiaMes2()

create or replace function darrerDiaMes2(myMonth numeric)
returns varchar
as $$
	declare
		myYear varchar;
		myDay varchar;
	begin
	myYear := '/'||to_char(current_date,'yyyy');
	myDay := to_char(current_date,'dd')||'/';
	return to_char((to_date(myDay||myMonth::text||myYear,'dd/mm/yyyy') + interval '1 month')::date - (to_char(current_date,'dd'))::int,'dd');
	end;
$$ language plpgsql;

select darrerDiaMes2(2);


-- 4. funció darrerDiaMes3()

create or replace function darrerDiaMes3(myMonth varchar)
returns varchar
as $$
	declare
		myYear varchar;
		myDay varchar;
		myMonthParsed int;
	begin
	if lower(myMonth) in ('enero','january') then
		myMonthParsed := 1;
	elsif lower(myMonth) in ('febrero','february') then
		myMonthParsed := 2;
	elsif lower(myMonth) in ('marzo','march') then
		myMonthParsed := 3;
	elsif lower(myMonth) in ('abril','april') then
		myMonthParsed := 4;
	elsif lower(myMonth) in ('mayo','may') then
		myMonthParsed := 5;
	elsif lower(myMonth) in ('junio','june') then
		myMonthParsed := 6;
	elsif lower(myMonth) in ('julio','july') then
		myMonthParsed := 7;
	elsif lower(myMonth) in ('agosto','august') then
		myMonthParsed := 8;
	elsif lower(myMonth) in ('setiembre','september') then
		myMonthParsed := 9;
	elsif lower(myMonth) in ('octubre','october') then
		myMonthParsed := 10;
	elsif lower(myMonth) in ('noviembre','november') then
		myMonthParsed := 11;
	elsif lower(myMonth) in ('diciembre','december') then
		myMonthParsed := 12;
	else
		return 'Wrong input for month, valids months are spelled in english or spanish.';
	end if;
	myYear := '/'||to_char(current_date,'yyyy');
	myDay := to_char(current_date,'dd')||'/';
	return to_char((to_date(myDay||myMonthParsed||myYear,'dd/mm/yyyy') + interval '1 month')::date - (to_char(current_date,'dd'))::int,'dd');
	end;
$$ language plpgsql;

select darrerDiaMes3('March');


-- 4. funció darrerDiaMesV2()

create or replace function darrerDiaMesV2(myMonth numeric)
returns varchar
as $$
	begin
		
		return darrerDiaMes2(myMonth);
	end;
$$ language plpgsql;

select darrerDiaMesV2(2);





