--() { :; }; exec psql scott -f "$0"

-- iaw14270791
-- funcions triggers
-- Jacint Iglesias
-- UF3


-- Crear un trigger t_auditoria  que anotara en una tabla, quien hace 
-- una operación DML  sobre la tabla EMP (bd scott)  y cuando la realiza. 
-- Para ello necesito crear una tabla que se llamara control. La estructura 
-- de la qual tendra id (tipo serial), fecha (tipo timestamp), tipo (tipo 
-- texto, longitud 1, podra tener 3 valores, I,U o D), usuario (tipo varchar(50)).


\c scott


-- create table
CREATE TABLE control (
    id serial CONSTRAINT PK_CONTROL_ID PRIMARY KEY,
    fecha timestamp,
    tipo varchar(1),
    usuario varchar(50),
    CONSTRAINT CK_CONTROL_TIPO CHECK(tipo in('I','U','D'))
);


-- trigger function cos/cuerpo
drop trigger f_audtoria on emp;
create or replace function f_auditoria()
returns trigger
as $$
begin
	insert into control (id,fecha,tipo,usuario)
	values (default,current_timestamp,upper(substr(tg_op,1,1)),current_user); -- puedes oviar el campo para que gestione serial por defecto
	return null; -- en triggers a nivel de statement hay que hacer return null
end;
$$ language plpgsql;



-- cabecera trigger
CREATE TRIGGER t_auditoria
AFTER DELETE or update or insert ON emp -- nota tiene que ser AFTER por si falla la operación
for statement -- o for each row
EXECUTE PROCEDURE f_auditoria();


-- prueva de fuego
insert into emp(empno) values(555);
update emp set ename = 'James Jams'
where empno = 444;

-- resultado
select * from control;
