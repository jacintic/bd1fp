--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3

\c biblioteca

-- exemplar disponible, joc de proves
\echo 'Joc de proves d''exemplar disponible';

\echo 'Exemplar amb alguns disponibles i altres en lloguer';
select codiExemplarDisponible('Highway to hell'); -- disponible 
select codiExemplarDisponible('Fundamentos de bases de datos'); -- disponible 
\echo 'Exemplar en lloguer'; 
select codiExemplarDisponible('La La Land'); -- no disponible  
\echo 'Exemplar mai en prestec i disponible'; 
select codiExemplarDisponible('Lonely Planet traveller');


-- exemplar disponibleV2, joc de proves
\echo 'Joc de proves d''exemplar disponible2';

\echo 'Exemplar amb alguns disponibles i altres en lloguer';
select codiExemplarDisponible2('Highway to hell'); -- disponible 
select codiExemplarDisponible2('Fundamentos de bases de datos'); -- disponible 
\echo 'Exemplar en lloguer'; 
select codiExemplarDisponible2('La La Land'); -- no disponible  
\echo 'Exemplar mai en prestec i disponible'; 
select codiExemplarDisponible2('Lonely Planet traveller');


-- prestecDocument joc de proves

\echo 'usuari erroni';
select prestecDocument(99999,'highway to hell','Fundamentos de bases de datos');

\echo 'document erroni';
select prestecDocument(1,'asodjsdiasodd');

\echo 'dades corrects';
select prestecDocument(1,'highway to hell');
