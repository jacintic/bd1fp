--() { :; }; exec psql scott -f "$0"

-- iaw14270791
-- funcions scottEmpleat
-- Jacint Iglesias
-- UF3

\c scott

-- create sequence for emp
create sequence if not exists seq_emp
increment by 1 start with 1; -- can't start with 0

--set sequence default value 
select  setval('seq_emp', (select max(empno) FROM emp)::int);


--creacte sequence for dept

create sequence if not exists seq_deptno
increment by 10 start with 10; -- can't start with 0

--set sequence default value 
select  setval('seq_deptno', (select max(deptno) FROM dept)::int);


-- funcio
drop trigger fEmpleat on empleat;
create or replace function fEmpleat()
returns trigger
as $$
declare
	v_deptno int;
begin
	begin
		-- obtenir nombre de departament si existeix
		select deptno
		into strict v_deptno
		from dept
		where lower(dname) = lower(new.nomdept);
		exception
			when no_data_found then
			-- inserir departament si no existeix
			insert into dept (deptno,dname,loc)
				values(nextval('seq_deptno'),new.nomdept,new.localitat);
			-- obtenir nom de nou departament
			select max(deptno)
			into v_deptno
			from dept;
	end;
	insert into emp (empno,ename,sal,job,deptno)
		values(nextval('seq_emp'),new.nomemp,new.salari,new.ofici,v_deptno);
return new;
end;
$$ language plpgsql;

-- trigger (capçalera)
drop tirgger tEMpleat on empleat;
create trigger tEmpleat
instead of insert on empleat
for each row -- sempre per triggers de vistes
execute procedure fEMpleat();


-- joc de proves empleat de departament existent
insert into empleat (nomemp,salari,ofici,nomdept,localitat)
    values('Tester',60000,'Testeroo','sales','chicago');
    
-- empleat de departament no existent
insert into empleat (nomemp,salari,ofici,nomdept,localitat)
    values('Test2',60000,'Testero2','marketings','chicago');
  
  
  
    
-----------------------------------------------------
------------------ correció -------------------------
-----------------------------------------------------


create or replace function fEmpleatC()
returns trigger
as $$
declare
	v_deptno int;
begin
	begin
		-- obtenir nombre de departament si existeix
		select deptno
		into strict v_deptno
		from dept
		where lower(dname) = lower(new.nomdept);
		exception
			when no_data_found then
			raise exception 'Error: el departamento no existe.';
	end;
	insert into emp (empno,ename,sal,job,deptno)
		values(nextval('seq_emp'),new.nomemp,new.salari,new.ofici,v_deptno);
return new;
end;
$$ language plpgsql;

-- trigger (capçalera)
create trigger tEmpleatC
instead of insert on empleat
for each row -- sempre per triggers de vistes
execute procedure fEMpleatC();


-- joc de proves empleat de departament existent
insert into empleat (nomemp,salari,ofici,nomdept,localitat)
    values('Tester',60000,'Testeroo','sales','chicago');
    
-- empleat de departament no existent
insert into empleat (nomemp,salari,ofici,nomdept,localitat)
    values('Test2',60000,'Testero2','marketings','chicago');
    
    
-----------------------------------------------------
------------------ correció2 ------------------------
-----------------------------------------------------


create or replace function fEmpleatC2()
returns trigger
as $$
declare
	v_deptno int;
begin
	begin
		-- obtenir nombre de departament si existeix
		select deptno
		into strict v_deptno
		from dept
		where lower(dname) = lower(new.nomdept);
		exception
			when no_data_found then
				-- get deptno 
				v_deptno := nextval('seq_deptno');
				-- inserir departament si no existeix
				insert into dept (deptno,dname,loc)
					values(v_deptno,new.nomdept,new.localitat);
	end;
	insert into emp (empno,ename,sal,job,deptno)
		values(nextval('seq_emp'),new.nomemp,new.salari,new.ofici,v_deptno);
return new;
end;
$$ language plpgsql;

-- trigger (capçalera)
create trigger tEmpleatC2
instead of insert on empleat
for each row -- sempre per triggers de vistes
execute procedure fEMpleatC2();


-- joc de proves empleat de departament existent
insert into empleat (nomemp,salari,ofici,nomdept,localitat)
    values('Tester',60000,'Testeroo','sales','chicago');
    
-- empleat de departament no existent
insert into empleat (nomemp,salari,ofici,nomdept,localitat)
    values('Test2',60000,'Testero2','marketings','chicago');    
    
    
    
    
    
