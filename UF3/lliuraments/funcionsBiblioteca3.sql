--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- funcions bibblioteca
-- Jacint Iglesias
-- UF3

-- correcció

-- funció codiExemplarDisponible
\c biblioteca
\echo 'codiExemplarDisponible';
drop function codiExemplarDisponible(varchar);
create or replace function codiExemplarDisponible(p_titol varchar)
returns integer
as $$
declare
	v_idExemplar int;
begin
	-- s'obte el codi de l'exemplar
	select idExemplar
	into strict v_idExemplar
	from exemplar e
	inner join document d
	on e.idDocument = d.idDocument
	where lower(titol) = lower (p_titol)
	and lower(estat) = 'disponible'
	and idExemplar not in (	select idExemplar
							from prestec
							where datadev is null)
	limit 1; -- solo coje el primero
	-- ^ si el select falla se salta directamente a exception puenteando todo el codigo
	-- es comunica que s'ha trobat l'exemplar
	-- es retorna l'exemplar
	return v_idExemplar;
	-- exception cuan no es troba un exemplar 
	exception
		when no_data_found then
			return 0;
end;
$$ language plpgsql;


-- exemplar disponible V2

\echo 'codiExemplarDisponible2';

drop function codiExemplarDisponible2(varchar);
create or replace function codiExemplarDisponible2(p_titol varchar)
returns integer
as $$
declare
		cur_exemplars cursor for 
			select e.idexemplar miexemplar
			from exemplar e
			join document d
			on e.iddocument = d.iddocument
			where lower(titol) = lower (p_titol)
			and lower(estat) = 'disponible'
			and e.idexemplar not in (	select idExemplar
									from prestec
									where datadev is null)
			order by 1;
	v_exempar record;
begin
	for v_exempar in cur_exemplars
	   LOOP
			return v_exempar.miexemplar;
		end loop;		
	return 0;
end;
$$ language plpgsql;

-- simplificar (solo una fila)
-- un sol return

drop function codiExemplarDisponible3(varchar);
create or replace function codiExemplarDisponible3(p_titol varchar)
returns integer
as $$
declare
		cur_exemplars cursor for 
			select e.idexemplar miexemplar
			from exemplar e
			join document d
			on e.iddocument = d.iddocument
			where lower(titol) = lower (p_titol)
			and lower(estat) = 'disponible'
			and e.idexemplar not in (	select idExemplar
									from prestec
									where datadev is null)
			order by 1;
	v_exemplar record;
	v_codiexemplar int;
begin
	open cur_exemplars; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_exemplars INTO v_exemplar;
	v_codiexemplar := v_exemplar.miexemplar;
	close cur_exemplars;
	return coalesce(v_codiexemplar,0);
end;
$$ language plpgsql;

-- datadev is not null


drop function codiExemplarDisponible4(varchar);
create or replace function codiExemplarDisponible4(p_titol varchar)
returns integer
as $$
declare
		cur_exemplars cursor for 
			select e.idexemplar miexemplar
			from exemplar e
			join document d
			on e.iddocument = d.iddocument
			join prestec p on e.idexemplar = p.idexemplar
			where lower(titol) = lower (p_titol)
			and lower(estat) = 'disponible'
			and datadev is not null
			order by 1;
	v_exemplar record;
	v_codiexemplar int;
begin
	open cur_exemplars; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_exemplars INTO v_exemplar;
	v_codiexemplar := v_exemplar.miexemplar;
	close cur_exemplars;
	return coalesce(v_codiexemplar,0);
end;
$$ language plpgsql;


-- corrección
-- int en vez de record (por que se almacena un solo campo que es int)

drop function codiExemplarDisponible5(varchar);
create or replace function codiExemplarDisponible5(p_titol varchar)
returns integer
as $$
declare
		cur_exemplars cursor for 
			select e.idexemplar miexemplar
			from exemplar e
			join document d
			on e.iddocument = d.iddocument
			join prestec p on e.idexemplar = p.idexemplar
			where lower(titol) = lower (p_titol)
			and lower(estat) = 'disponible'
			and datadev is not null
			order by 1;
	v_exemplar int;
begin
	open cur_exemplars; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_exemplars INTO v_exemplar;
	close cur_exemplars;
	return coalesce(v_exemplar,0);
end;
$$ language plpgsql;



-- funcio get tipus
\echo 'getTipus';

drop function getTipus(varchar);
create or replace function getTipus(p_titol varchar)
returns varchar
as $$
declare
	v_tipusExemplar varchar;
begin	

if codiExemplarDisponible(p_titol) != 0 then
	select format
	into strict v_tipusExemplar
	from document d inner join
	exemplar e on e.iddocument = d.iddocument
	where lower(titol) = lower(p_titol)
	limit 1;
	return lower(v_tipusExemplar);
else
	return 0;
end if;
return 0;
exception
		when no_data_found then
			return 0;
end;
$$ language plpgsql;

-- funció prestecDocument

-- Per simplificació, considerarem que un usuari podrà tenir simultàniament un
-- màxim de 4 docs:

-- 2 docs del tipus llibre/revista
-- 1 DVD
-- 1 CD

-- er la funció prestecDocument, hem de tenir un exemplar disponible i 
-- evidentment un usuari que no tingui blocat el carnet degut a 
-- penalitzacions acumulades.

\echo 'prestecDocument2';




-- prestec document correcció
--
--



drop function prestecDocument2(integer,varchar);
create or replace function prestecDocument2(	p_idusuari usuari.idusuari%type, p_titol document.titol%type) -- parametre formal
returns boolean
as $$
declare
	v_usuariBloquejat boolean;
	v_datadev date;
	v_totalLlibresIRevistes int = 0;
	v_totalCd int = 0;
	v_totalDvd int = 0;
	v_total_documents int = 0;
	v_totalLlibresIRevistes_prellogats int = 0;
	v_totalCd_prellogats int = 0;
	v_totalDvd_prellogats int = 0;
begin
	-- no esta blocat
	begin
		select bloquejat, datadesbloqueig
		into strict v_usuariBloquejat, v_datadev
		from usuari
		where idusuari = p_idusuari;
		if 	v_usuariBloquejat then
			raise notice 'Usuari %  esta bloquejat, data de desbloqueig %', p_idusuari,coalesce(v_datadev::varchar,'(data desconeguda)');
			return false;
		end if;
		-- codi per l'excepció (usuari no trobat o bloquejat)
		exception
			when no_data_found then
				raise notice 'Usuari % no esta donat d''alta', p_idusuari;
				return false;
	end;
	-- comprovem el tipus d'exemplar i que esta disponible
	case 
		when getTipus(p_titol) in ('revista','llibre') then
			v_totalLlibresIRevistes := v_totalLlibresIRevistes + 1;
		when getTipus(p_titol) = 'cd' then
			v_totalCd := v_totalCd + 1;
		when getTipus(p_titol) = 'dvd' then
			v_totalDvd := v_totalDvd + 1;
		else
			raise info 'Document % no disponible.',p_titol;
			return false;
	end case;
	
	-- control de documents previament llogats, quantitat
	-- llibres/revistes
	begin
		select
				sum(case when lower(format) in ('llibre','revista') then 1 else 0 end) llibres,
				sum(case when lower(format) = 'cd' then 1 else 0 end) cds,
				sum(case when lower(format) = 'dvd' then 1 else 0 end) pelis
		into strict v_totalLlibresIRevistes_prellogats,v_totalCd_prellogats,v_totalDvd_prellogats
		from document d
		join exemplar e on e.iddocument= d.iddocument
		join prestec p on p.idexemplar = e.idexemplar
		where idusuari = p_idusuari and
		datadev is null;
		exception
			when no_data_found then
				raise notice 'Error: problema amb el recompte de documents prellogats.';
	end;
	-- afegim prellogats a llibres a llogar
	v_totalLlibresIRevistes := 	v_totalLlibresIRevistes + v_totalLlibresIRevistes_prellogats;
	v_totalCd := v_totalCd + v_totalCd_prellogats;
	v_totalDvd := v_totalDvd + v_totalDvd_prellogats;
	-- comprovem la quantitat de numero de documents disponibles esta 
	-- d'acord amb el numero maxim de documents (2 llibre/revista,
	-- 1 DVD, 1 CD)
	-- de lo contrari retorna false i fa un raise notice informant
	if v_totalLlibresIRevistes > 2 OR v_totalCd > 1 OR v_totalDvd > 1 then
		raise notice 'Nomes es poden llogar 2 llibres/revistes, 1 dvd i 1 cd. Has tractat de llogar el següent:';
		raise notice '% llibres/revistes: ',v_totalLlibresIRevistes;
		raise notice '% cds: ',v_totalCd;
		raise notice '% dvds: ',v_totalDvd;
		raise notice 'Titols dels documents: %',p_titol;
		return false;
	end if;
	-- s'efectua el lloguer
	insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesRenovat)
	values	(codiExemplarDisponible(p_titol),current_timestamp,null,p_idusuari,0);
	-- informació
	raise notice 'En prestec el document %',p_titol;
	v_total_documents := v_totalLlibresIRevistes + 	v_totalCd + v_totalDvd;
	raise notice '% documents en prestec.',v_total_documents;
	raise notice '% llibres/revistes.',v_totalLlibresIRevistes;
	raise notice '% CDs.',v_totalCd;
	raise notice '% Pelicules.',v_totalDvd;
	return v_total_documents > 0;
end;
$$ language plpgsql;


-- Retorn de prèstecs i penalització (sistema de punts)
-- funció retornarDocument
-- Estudieu les taules usuari i penalització.
-- Per al bon funcionament del servei de préstec et recordem que has de ser puntual
-- a l'hora de retornar els documents. Per cada dia de retard i document en
-- préstec, el teu carnet rebrà 1 punt de penalització. Per cada 50 punts, el
-- carnet quedarà bloquejat durant 15 dies. Passat aquest temps, el carnet tornarà
-- a estar operatiu.
-- Quan un usuari superi els 50 punts de demèrit, és reiniciarà el comptador de
-- punts.
-- Hi ha llibres que no es presten (són  només de consulta a la biblioteca).
-- Si l'exemplar que es retorna està reservat, NO ha d'aparèixer com a disponible
-- pel públic en general.

-- 1 punt penalització per dia tard
-- 50 punts per bloqueig
-- 15 dies bloqueig
-- despres de 15 dies punts a 0

-- documents que no son de prestec

-- exemplat retornat que esta reservat => no disponible => Reservat

-- data devolució 30 dies, renovació 30 dies

drop function retornarDocument(integer,varchar);
create or replace function retornarDocument(	p_idusuari usuari.idusuari%type, p_titol document.titol%type) -- parametre formal
returns boolean
as $$
declare
	v_titol boolean;
	v_days int;
	v_idexemplar int;
	v_punts int;
	v_extra_days int DEFAULT = 0;
	v_total_punts int;
	v_basura int;
begin
-- condició d'existencia d'usuari
begin
	select u.idusuari
	into strict v_basura
	from usuari u
	join prestec p on u.idusuari = p.idusuari
	where u.idusuari = p_idusuari
	and datadev is null
	limit 1;
	exception
		when no_data_found then
			raise info 'Usuari no existeix o no te documents en prestec.';
			return false;
end;
-- comprovar que el document esta en prestec
begin
select lower(d.titol) = lower(p_titol) 
into strict v_titol
from document d
join exemplar e on e.iddocument = d.iddocument
join prestec p on p.idexemplar = e.idexemplar
where datadev is null and
p.idusuari = p_idusuari;
-- condició de document no trovat en prestec
if v_titol is null then
		raise notice 'Aquest document no esta en prestec.';
		return false;
end if;
exception
	when too_many_rows then
		raise info 'Error: Mes d''un exemplar del mateix document en prestec. Retornant el primer';
end;
-- obtenim el numero d'exemplar
begin
select p.idexemplar
into strict v_idexemplar
from prestec p
join exemplar e on p.idexemplar = e.idexemplar 
join document d on d.iddocument = e.iddocument
where p.idusuari = p_idusuari and
lower(titol) = lower(p_titol) and
datadev is null
order by 1 limit 1;
-- control d'error per document en prestec no trobat
exception
	when no_data_found then
		raise notice 'Error, no s''ha trobat l''exemplar en prestec.';
		return false;
end;
-- comprovació de dies mes tard de datadev
-- obtenció de total dies de prestec
select sum(to_char(current_timestamp - datapres,'dd')::int)::int
into v_days
from prestec
where idusuari = p_idusuari
and datadev is null
and idexemplar = v_idexemplar;
-- control d'errors si data prestec es null
if v_days is null then
	raise notice 'Hi ha un error amb la data de prestec, no es troba al sistema.';
	return false;
end if;
--aplicació de penalitzacións
if v_days > 30 then
	v_extra_days := v_days - 30;
	-- control de punts actuals
	begin
		select punts::int
		into strict v_punts
		from usuari
		where idusuari = p_idusuari;
		exception
			when no_data_found then
			raise info 'Error amb els punts d''usuari, no trobats';
			return false;
	end;
	
	-- calcul de punts
	v_total_punts := v_punts + v_extra_days;
	case v_total_punts >= 50
		when true then
			v_extra_days := 50;
		when false then
			v_extra_days := v_total_punts;
		else
			raise info 'Error amb calcul de punts';
			return false;
	end case;
	-- insert/update a penalització de punts
	begin	
		select idusuari
		into strict v_basura
		from penalitzacio
		where idusuari = p_idusuari
		limit 1;
		-- si l'usuari ja esta definit a penalització es fa un update
		update penalització
		set demerit = v_extra_days
		where idusuari = p_idusuari and
		idexemplar = v_idexemplar;
		-- si no esta definit es fa un insert
		exception
			when no_data_found then
				insert into penalitzacio (idusuari, idexemplar, data, demerit)
				values (p_idusuari,v_idexemplar,current_timestamp,v_extra_days);
	end;
	-- bloquejar si es necesari
	if v_extra_days = 50 then
		update usuari 
		set bloquejat = true,
		datadesbloqueig = (current_date + interval '15 days')::date,
		punts = case when (punts + v_extra_days) > 50 then 50 else(punts + v_extra_days) end
		where idusuari = p_idusuari;
		raise info 'Usuari bloquejat, data desbloqueig en 15 dies des d''avui';
	end if;
end if;
-- s'efectua el retorn de document
update prestec
set datadev = current_timestamp
where idusuari = p_idusuari and
idexemplar = v_idexemplar;
raise info 'S''ha efectuat la devolució';
return true;
end;
$$ language plpgsql;


-- funció renovarDocument
-- Es poden renovar fins a tres vegades, la qual cosa es pot fer en persona,
-- telèfon o internet.
-- Els documents es poden renovar sempre i quan:

-- no s'hagin renovat ja tres vegades
-- no hagi expirat la data de retorn
-- no es tingui el carnet de préstec bloquejat per algun motiu
-- no estiguin reservats per un altre usuari

-- La renovació és de 30 dies naturals sobre la data de prèstec.

drop function renovarDocument(integer);
create or replace function renovarDocument(	p_idexemplar exemplar.idexemplar%type) -- parametre formal
returns varchar
as $$
declare
	v_message varchar(300) := 'Document renovat amb exit.';
	v_vegadesrenovat int;
	v_dies varchar;
	v_bloquejat boolean;
	v_estat varchar;
	v_MAXDIES int := 35;
	v_idusuari int;
begin
	begin
	-- obtenció d'usuari
		select idusuari 
		into strict v_idusuari
		from prestec
		where idexemplar = p_idexemplar
		and datadev is null;
		exception
			when no_data_found then
				v_message := 'No s''ha trovat l''usuari.';
	end;
	begin
	-- recollida de variables pe'l control de renovació
		select vegadesrenovat, to_char((current_timestamp) - (datapres),'dd')::int, bloquejat, estat
		into strict v_vegadesrenovat, v_dies, v_bloquejat, v_estat 
		from prestec p join usuari u on p.idusuari = u.idusuari
		join  exemplar e on p.idexemplar = e.idexemplar
		where p.idexemplar = p_idexemplar
		and datadev is null
		and p.idusuari = v_idusuari;
		exception
			when no_data_found then
				v_message := 'Error: No s''ha trobat el document a retornar.';
				
	end;
	case
		-- opcions que eviten la renovació
		when v_vegadesrenovat > 2 then
			v_message := 'Error: El document s''ha renovat ja 3 vegades i no es pot renovar.';
		when v_dies::int >= v_MAXDIES then
			v_message := 'Error: El document no es pot renovar, fora de plaç';
		when v_bloquejat then
			v_message := 'Error: L''usuari esta bloquejat i no pot renovar.';
		when lower(v_estat) = 'reservat' then
			v_message := 'Error: El document esta reservat i no es pot renovar';
		else
			-- opció on s'efectua la renovació
			update prestec 
			set vegadesrenovat = vegadesrenovat +1,
				datapres = current_timestamp
			where idexemplar = p_idexemplar
			and idusuari = v_idusuari
			and datadev is null;
	end case;
	return v_message;
end;
$$ language plpgsql;
