--() { :; }; exec psql scott -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3
\c scott
\echo 'update emp set ename = Francisdeptno= 30 where empno in (7698,7844);';
update emp set ename = 'Francis',deptno= 30 where empno in (7698,7844);

\echo 'insert into emp (empno,ename, deptno) values (654,Juaquin,20),(652,Manolo,30);';
insert into emp (empno,ename, deptno)
	values (654,'Juaquin',20),
	(652,'Manolo',30);
\echo 'delete from emp where empno in(7900,7521);';		
delete from emp where empno in(7900,7521);
