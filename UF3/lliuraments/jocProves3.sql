--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3

\c biblioteca


-- prestecDocument joc de proves


\echo 'CodiExemplarV3';
select codiexemplardisponible5('hooba');
select codiexemplardisponible5('highway to hell');

-- prestecV2

-- distinguir entre usuari que no existeix i usuari bloquejat
\echo 'usuari erroni';
select prestecDocument2(99999,'highway to hell');

\echo 'document erroni';
select prestecDocument2(1,'asodjsdiasodd');

\echo 'masses llibres';



-- usuari bloquejat (amb datadev i sense)
update usuari 
set (bloquejat,datadesbloqueig) = (true,(current_date + interval '1 week')::date)
where idusuari = 4;

update usuari 
set bloquejat = true
where idusuari = 5;

select prestecDocument2(4,'highway to hell');
select prestecDocument2(5,'highway to hell');


-- excess de llibres
\echo 'que pasa con este';
select prestecDocument2(1,'Fundamentos de bases de datos');

-- excess de revistes
select prestecDocument2(1,'Lonely Planet traveller');


-- exces de cd + llibre


-- excess de cds
select prestecDocument2(3,'highway to hell');

-- excess de dvds

select prestecDocument2(2,'Pi');

--\echo 'document correcte';
select prestecDocument2(2,'highway to hell');



-- renovar document

\echo 'Renovar document';

\echo '----------------';


\echo 'fora de plaç';
-- fora de plaç
select renovarDocument(1);

\echo 'document no existeix';
-- document no existeix
select renovarDocument(12312);


\echo 'tot correcte';
-- tot correcte
select renovarDocument(5);
