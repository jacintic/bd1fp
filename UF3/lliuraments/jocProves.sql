--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3

\c biblioteca


-- marcaReserva
\echo '---------------------------------';
\echo 'Joc de proves de marcarReserva';
\echo '---------------------------------';
-- altre usuari vol el mateix exemplar
\echo 'Altre usuari reserva el mateix document';
insert into reserva values(1,3,current_timestamp,null);
insert into reserva values(6,4,current_timestamp,null);
select * from reserva;
select * from prestec;

\echo 'Fem un retornar document de l''exemplar 1 que es disputen 2 usuaris';
-- select retornarDocument(8); <= tienes que adaptar la funcion a idexemplar
select retornarDocument(1,1);

/*
\echo '---------------------------------';
\echo 'Joc de proves de reservarDocument';
\echo '---------------------------------';
-- reservarDocument joc de proves


-- documents disponibles per prestec
\echo 'condició documents que no es troven tots en prestec.';

select reservarDocument(1,1);

-- documents no disponibles per prestec pero mateix usuari que esta prestant

\echo 'Usuari de reserva mateix que de prestec.';

-- es possen tots els exemplars en prestec
insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesrenovat)
        values  (1,current_timestamp,null,1,0),
                (2,current_timestamp,null,2,0),
                (3,current_timestamp,null,3,0);

select reservarDocument(1,1);

\echo 'Tot correcte.';

select reservarDocument(1,4);


\echo '---------------------------------';
\echo 'Joc de proves de recollirReserva';
\echo '---------------------------------';

--select recollirReserva();
select * from reserva;
select * from prestec;
-- en prestec
select recollirReserva(2,5);
-- document no reservat
--  tot correcte
update prestec
	set datadev = current_timestamp
where idexemplar = 1 and idusuari = 1;
-- document no reservat
select recollirReserva(1,5);
update exemplar
	set estat = 'Reservat'
where idexemplar = 1;
select recollirReserva(1,5);
*/
