--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3

\c biblioteca


-- prestecDocument joc de proves


\echo 'CodiExemplarV3';
select codiexemplardisponible5('hooba');
select codiexemplardisponible5('highway to hell');

-- prestecV2

-- distinguir entre usuari que no existeix i usuari bloquejat
\echo 'usuari erroni';
select prestecDocument2(99999,'highway to hell');

\echo 'document erroni';
select prestecDocument2(1,'asodjsdiasodd');

\echo 'masses llibres';



-- usuari bloquejat (amb datadev i sense)
update usuari 
set (bloquejat,datadesbloqueig) = (true,(current_date + interval '1 week')::date)
where idusuari = 4;

update usuari 
set bloquejat = true
where idusuari = 5;

select prestecDocument2(4,'highway to hell');
select prestecDocument2(5,'highway to hell');


-- excess de llibres
\echo 'que pasa con este';
select prestecDocument2(1,'Fundamentos de bases de datos');

-- excess de revistes
select prestecDocument2(1,'Lonely Planet traveller');


-- exces de cd + llibre


-- excess de cds
select prestecDocument2(3,'highway to hell');

-- excess de dvds

select prestecDocument2(2,'Pi');

--\echo 'document correcte';
select prestecDocument2(2,'highway to hell');



\echo 'Retorn document';


\echo '----------------------------';
\echo '';

\echo 'Exemplar que no esta en prestec';
select retornarDocument(3,'Lonely Planet traveller');

\echo 'Usuari no existeix';
select retornarDocument(123123,'Fundamentos de bases de datos');

-- s'actualitza la data de prestestec per que sigui retornable.
update prestec set datapres = current_timestamp where idusuari = 1 and idexemplar = 1;
\echo 'Mes d''un exemplar del mateix document en prestec';
select retornarDocument(1,'Fundamentos de bases de datos');


-- fen un update de l'exemplar per que la data de devolució sigui tarda
update prestec set datapres = current_timestamp - interval '1 month 15 days' where idusuari = 1 and idexemplar = 2;
\echo 'usuari bloquejat';

select retornarDocument(1,'Fundamentos de bases de datos');

\echo 'tot correcte';
select retornarDocument(3,'highway to hell');

select * from prestec where idexemplar = 4;
select * from usuari where idusuari = 1;
select * from penalitzacio where idusuari = 1;


-- joc de proves obtenir qui te l'exemplar en prestec

insert into prestec (idexemplar, datapres,datadev,idusuari,vegadesrenovat)
values (1,(current_timestamp - interval '1 week'),current_timestamp,5,0);

insert into prestec (idexemplar, datapres,datadev,idusuari,vegadesrenovat)
values (1,(current_timestamp - interval '1 week'),current_timestamp,1,0);


