--() { :; }; exec psql scott -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3
\c scott


-- sentence before


drop trigger updateChanges_sentence_before on emp;
create or replace function updateChanges_sentence_before()
returns trigger
as $$
begin
	raise info 'SENTENCE ABANS';
	return null; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;



drop trigger t_updateChanges_sentence_before on emp;
CREATE TRIGGER t_updateChanges_sentence_before
BEFORE UPDATE or insert or delete
ON emp
FOR statement
EXECUTE PROCEDURE updateChanges_sentence_before();





-- sentence after
drop trigger updateChanges_sentence_after on emp;
create or replace function updateChanges_sentence_after()
returns trigger
as $$
begin
	raise info 'SENTENCE DESPRES';
	return null; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;



drop trigger t_updateChanges_sentence_after on emp;
CREATE TRIGGER t_updateChanges_sentence_after
after UPDATE or insert or delete
ON emp
FOR statement
EXECUTE PROCEDURE updateChanges_sentence_after();












-- row before
drop trigger f_updateChanges_row_before on emp;
create or replace function f_updateChanges_row_before()
returns trigger
as $$
begin
	raise info 'ROW AVANS';
	raise info 'Nom i dept (new) nom:% , deptno: % . (old) nom:%, deptno: %', coalesce(new.ename,'No te nom(new)'),coalesce(new.deptno::varchar,'No te depnto(new)'),coalesce(old.ename,'No te nom(old)'),coalesce(old.deptno::varchar,'No te depnto(old)');
	return new ; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;
 
 
 
drop trigger t_updateChanges_row_before on emp;
CREATE TRIGGER t_updateChanges_row_before
BEFORE UPDATE or insert or delete
ON emp
FOR each row
EXECUTE PROCEDURE f_updateChanges_row_before();






-- row after
drop trigger updateChanges_row_after on emp;
create or replace function updateChanges_row_after()
returns trigger
as $$
begin
	raise info 'ROW DESPRES';
	raise info 'Nom i dept (new) nome: % , deptno: % . (old) nom: %, deptno: %', coalesce(new.ename,'No te nom(new)'),coalesce(new.deptno::varchar,'No te depnto(new)'),coalesce(old.ename,'No te nom(old)'),coalesce(old.deptno::varchar,'No te depnto(old)');
	--raise info 'Nom i dept (new) '|| coalesce(new.ename,'No te nom(new)') ||' ' || coalesce(new.deptno::varchar,'No te deptno(new)')||' '|| 'Nom i dept (old) '|| coalesce(old.ename,'No te nom(old)') ||' ' || coalesce(old.deptno::varchar,'No te deptno(old)');
	return new; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;

drop trigger t_updateChanges_row_after on emp;
CREATE TRIGGER t_updateChanges_row_after
after UPDATE or insert or delete
ON emp
FOR each row
EXECUTE PROCEDURE updateChanges_row_after();



