--() { :; }; exec psql training -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3
\c training


-- training

-- triggers ventes fora d'horari


-- trigger actualitzarVendes
drop trigger fControlProducte on pedido;
create or replace function fControlProducte ()
returns trigger
as $$
begin
	case 
	when to_char(current_timestamp,'dy') in ('mon','tue','wed','thu','fri') and to_char(current_timestamp,'hh24')::int < 9 or to_char(current_timestamp,'hh24')::int < 9  and substr(to_char(current_timestamp,'min'),1,2)::int < 14 and  to_char(current_timestamp,'hh24')::int > 17 or to_char(current_timestamp,'hh24')::int > 17  and substr(to_char(current_timestamp,'min'),1,2)::int > 20 then
		raise exception 'Error: Horari valid per dilluns - divendres es de 9 -14 hores';
	when to_char(current_timestamp,'dy') in ('sat','sun') and to_char(current_timestamp,'hh24')::int < 10 and to_char(current_timestamp,'hh24')::int > 14 and substr(to_char(current_timestamp,'min'),1,2)::int > 0  then
		raise exception 'Error: Horari valid per disabte - diumenge es de 10 - 14 hores.';
	else
		raise info 'Dins d''horari.';
	end case;
	return null; 
end;
$$ language plpgsql;


---- 17-20, dissabtes de 10-14h)

-- cabecera
drop trigger tControlProducte on pedido;
CREATE TRIGGER tControlProducte
before INSERT or update or delete
ON pedido 
for statement -- si hem de fer servir old i new es row
EXECUTE PROCEDURE fControlProducte ();  


/*

-- drop trigger fControlProducte on pedido;
-- create or replace function fControlProducte ()
-- returns trigger
-- as $$
-- begin
	-- case 
	-- when to_char(current_timestamp,'day') in ('monday','tuesday','wednesday','thursday','friday') and (to_char(current_timestamp,'hh24')::int < 9) or (to_char(current_timestamp,'hh24')::int > 14) then
		-- raise exception 'Error: Horari valid per dilluns - divendres es de 9 -14 hores';
	-- when to_char(current_timestamp,'day') in ('satruday','sunday') and (to_char(current_timestamp,'hh24')::int < 17  or to_char(current_timestamp,'hh24')::int = 17 and  to_char(current_timestamp,'mm')::int <20) or (to_char(current_timestamp,'hh24')::int >22 or (to_char(current_timestamp,'hh24')::int =22 and to_char(current_timestamp,'mm')::int >14) then
		-- raise exception 'Error: Horari valid per disabte - diumenge es de 17:20 - 10:14 hores';
	-- else
		-- raise info 'Dins d''horari. % , % ',to_char(current_timestamp,'day'),to_char(current_timestamp,'hh24');
	-- end case;
	-- return null; -- <= esto es nuevo triggers a nivel de row
--end;
--$$ language plpgsql;

*/
