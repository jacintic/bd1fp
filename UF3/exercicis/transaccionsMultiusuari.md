
#### Jacint Iglesias iaw14270791  
#### Josthyn Loma iaw54708991  
#### Sergio Polonio iaw53336673  

# Transacciones (Multiusuari)

7. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor) VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) FROM punts; -- Connexió 2
```

### Explicació:  
La operació de conexió 1, no te commit, per la qual cossa, els canvis no son visibles desde conexió 2.

### Previsió:  
`1 liena`  

### Resultat:  

```
1 linea.
```



8. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 4 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 81; -- Connexió 2

UPDATE punts SET valor = 10 WHERE id = 81; -- Connexió 1

UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 80; -- Connexió 0
```
|Usuari   |Bloquejat/desbloquejar   |Motiu   |
|---|---|---|
|  1 |Bloquejat   | Modificar un camp previament modificat per usuari 2 (modificant un recurs/fila en us de conexió 1) |
|  2 |Deadlock, bloquejat  | Modificar un camp previament per usuari 1 i donat que usuari 1 ja esta bloquejat, totes les operacions de conexió 2 en la seva transacció seran desfetes  |
| 2  |COMMIT => ROLLBACK, desblocat  | COMMIT dins de Deadlock fa ROLLBACK, deslbocat   |
| 1  | desblocat, COMMIT => COMMIT   | Usuari 2 fora de joc usuari 1 te via lliure per efectuar els cambis, es desbloca despres del rollback the conexió 2 |


### Explicació:  
Quan l'usuari 2 vol escriure sobre un valor que l'usuari 1 ha reescrit entra en `deadlock`, pero quan fa un commit (l'usuari 2) s'efectua un commit. Qui modifica el camp primer Rollback de qui provoca el deadlock.
Rollback: un blockat, no pasa res, pero si els dos es blocan (es a dir els dos modifiquen els registres de l'altre) el sgbd desfa l'ultim en completar el cicle de deadlock. Totes les operacions efectuades per conexió 1 es realitzen. SGBD desfa la transacció de l'usuari que probca el `deadlock`.   

```
UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2
error, deadlock detected // conexió 2 modificant fila modificada per conexió 1 cuan conexió 1 esta ja blocada
```    
### Previsió:  
`80,4`  

### Resultat:  

```
80,4
```
9. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2
INSERT INTO punts (id, valor) VALUES (91,9); -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 91; -- Connexió 0
```
|Usuari   |Bloquejat/desbloquejar   |Motiu   |
|---|---|---|
|  2 |desblocat  | postgres es bloca a nivell de fila |
|  1 | desblocat | postgres es bloca a nivell de fila  |  

### Explicació:  

Tot i que es fa un commit primer dun insert i despres d'un delete, d'alguna manera es mante l'ordre de les operacions, de manera que prime es registra el `DELETE` i despres l'`INSERT`. Encara que l'ordre dels `COMMIT`s sigui a l'inreves, es martre l'ordre d'operacions. **Imepra la temporalitat**. No es bloca per que el **nivell de granularitat** a **Postgres** es a nivell de **fila**.       

### Previsió:  
`0 rows`  

### Revisió de previsió:  

Sembla que les operacions tenen un orde cronologic i que s'executan d'aquesta manera. L'ordre dels commits no altera el producte.
### Resultat:  

```
 valor
-------
     9
```
10. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (100,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 100; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 100; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 100; -- Connexió 0
```


### Explicació:  
Conexió 2 es queda bloquejada despres de modificar el valor que ja habia modificat connexió 1. Connexió 1 fa commit i es queda en espera, cuan con1 fa commit el commit de com2 s'executa. El valor final segueix l'ordre logic cronologic.

|Usuari   |Bloquejat/desbloquejar   |Motiu   |
|---|---|---|
| 2 |Bloquejat   | Modificar un camp previament modificat per usuari 1  |
| 2 |COMMIT en espera => blocat   | El COMMIT no s'executa encara, es queda en espera o bloquejat  |
| 1  |COMMIT => COMMIT , desblocat| COMMIT s'executa   |
| 2  |COMMIT => COMMIT, desblocat | El COMMIT s'executa al efectuarse el desbloqueig (que s'efectua degut al commit de con1) |

### Previsió:  
`100,7`  

### Resultat:  

```
lineas=> SELECT valor FROM punts WHERE id = 100;
 valor
-------
     7
```
11. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 110; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 111; -- Connexió 0
```

|Usuari   |Bloquejat/desbloquejar   |Motiu   |
|---|---|---|
| 2 |Bloquejat   | Modificar un camp previament modificat per usuari 1  (UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2), l'usuari realitza 5 instruccions estant blocat, no s'executaran fins que no es desbloqui|
| 2 |COMMIT en espera   | El COMMIT no s'executa encara, es queda en espera o bloquejat  |
| 1  |COMMIT | COMMIT s'executa   |
| 2  |COMMIT | El COMMIT s'executa al efectuarse el desbloqueig  |


### Explicació:  
Con2 modifica el valor que ha modificat con1, a partir d'aquest punt es queda bloquejada. Seguidament con2 efectua operacions de transacció, totes queden en espera o bloquejades. Quan con1 fa el COMMIT, les operacions de com2 s'executen todes incluint el commit, totes s'efectuen exitossament. L'orden cronologic s'aplica.  

### Previsió:  
`111,7`  

### Resultat:  

```
lineas=> SELECT valor FROM punts WHERE id = 111; -- Connexió 0
 valor
-------
     7

```
#### I la 110?  

```

```


12. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts SET valor = 9 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 121; -- Connexió 0
```

|Usuari   |Bloquejat/desbloquejar   |Motiu   |
|---|---|---|
| 2 |Bloquejat   | Modificar un camp previament modificat per usuari 1  (UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2|
| 1 |ROLLBACK to a   | Desfa operació deixant cami obert a con 2  |
| 2  |desblocat | s'executa la operació que tenia en espera (UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2)  |

### Explicació:  

Com a destacar, com1 modifica valor a, com2 modifica valor a, es queda bloquejada. Quan com1 fa un rollback to a, con1 desfa la edició del valor que con2 a modificat, per tant con2 es desbloqueja.

### Previsió:  
`121,6`  

### Resultat:  

```
lineas=>  SELECT valor FROM punts WHERE id = 121; -- Connexió 0
 valor
-------
     6

```
