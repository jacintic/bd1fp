-- iaw14270791
-- Jacint Iglesias
-- exercici taules de sistema
select tablename "nom taula",tableowner "propietari taula", nspname "nom de schema", usename "propietari schema"
from pg_tables t join pg_namespace n on nspname = schemaname
join  pg_user on nspowner = usesysid
where lower(tableowner) = 'iaw14270791'
order by 3 desc,2 desc,1 desc;
