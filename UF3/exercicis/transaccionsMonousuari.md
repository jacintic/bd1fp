# Funcions de transaccions (MONOUSUARI)


1. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  

```
INSERT INTO punts (id, valor) VALUES (10,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 10;
ROLLBACK;
SELECT valor FROM punts WHERE id = 10;
```  
### Explicació:  
Es realitza un insert fora del begin, l'auto-commit fa que el canvi de l'insert sigui permanent i no es pugui desfer. Seguidament s'initcia la transacció amb un begin, a partir d'ara totes les operacions realitzades es poden pontencialment desfer. La primera acció que compleix les condicions explicades es un update, on es cambia el valor anteriorment asignat amb l'insert fora del begin, per tant, si es fa un commit es sobreescriura. Seguidament es fa un rollback, la qual cosa desfa tots el cambis fets fins ara, i l'update ha, com efecte del rollback, deixat de reescriure el valor a id = 10. Es a dir l'update ha sigut desfet.  
  
### Previsió:  
`valor = 5`  
  
### Resultat:  
  
```
lineas=> insert into punts (id,valor) values (10,5);
INSERT 0 1
lineas=> begin;
BEGIN
lineas=> update punts set valor = 4 where id = 10;
UPDATE 1
lineas=> rollback;
ROLLBACK
lineas=> select valor from punts where id = 10;
 valor 
-------
     5
```


2. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
```
INSERT INTO punts (id, valor) VALUES (20,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 20;
COMMIT;
SELECT valor FROM punts WHERE id = 20;
```
  
### Explicació:  

La primera operació es a fora del begin, per la qual cosa el valor de 20 es 5. Seguidament s'initcia la transacció. Es fa un update que reescriura el valor establert pe'l primer select. Seguidament es fa un commit, que fa permanent el cambi de l'update, per la qual cossa el valor d'id 20 ha sigut reescrit.
  
### Previsió:  
`valor = 4`  

### Resultat:  

```
lineas=> INSERT INTO punts (id, valor) VALUES (20,5);
INSERT 0 1
lineas=> BEGIN;
BEGIN
lineas=> UPDATE punts SET valor = 4 WHERE id = 20;
UPDATE 1
lineas=> COMMIT;
COMMIT
lineas=> SELECT valor FROM punts WHERE id = 20;
 valor 
-------
     4
```

3. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (31,7);
ROLLBACK;
SELECT valor FROM punts WHERE id = 30;
```
  
### Explicació:  
  
La primera operació es a fora del begin, per la qual cosa el valor de 30 es 5. Seguidament s'initcia la transacció. Es fa un update que reescriura el valor establert pe'l primer select. Seguidament es fa savepoint, que fa possible un rollback al momment just despres de l'update (fent `rollback to a`). Tot seguit es fa un insert, un valor nou. El rollback, al no especificar un savepoint, retorna al moment del begin, de forma que cap operació persisteix.
  
### Previsió:  
`valor = 5`  
  
### Resultat:  

```
lineas=> INSERT INTO punts (id, valor) VALUES (30,5);
INSERT 0 1
lineas=> BEGIN;
BEGIN
lineas=> UPDATE punts SET valor = 4 WHERE id = 30;
UPDATE 1
lineas=> SAVEPOINT a;
SAVEPOINT
lineas=> INSERT INTO punts (id, valor) VALUES (31,7);
INSERT 0 1
lineas=> ROLLBACK;
ROLLBACK
lineas=> SELECT valor FROM punts WHERE id = 30;
 valor 
-------
     5
```
  
4. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;
```
  
### Explicació:  
  
Les dues primeres operacions son permanents, per tant la taula punts nomes te un valor, 40,5. Seguidament es comença la transacció. Es fa un update on es cambia l'anterior valor a 40,4. Seguidament s'estableix un ssave point, de nom `a`. Seguidament es fa un altre insert. Despres es fa un rollback al punt `a`. Per tant el valor que perdura es el de l'update (40,4).
  
### Previsió:  
`40, 4`  
##### Revisió de previsió:  
No habia tingut en compte que era un count, el valor es el que perdura a la taula es el que jo habia previst.  
  
### Resultat:  
  
```
lineas=> DELETE FROM punts;
DELETE 11
lineas=> INSERT INTO punts (id, valor) VALUES (40,5);
INSERT 0 1
lineas=> BEGIN;
BEGIN
lineas=> UPDATE punts SET valor = 4 WHERE id = 40;
UPDATE 1
lineas=> SAVEPOINT a;
SAVEPOINT
lineas=> INSERT INTO punts (id, valor) VALUES (41,7);
INSERT 0 1
lineas=> ROLLBACK TO a;
ROLLBACK
lineas=> SELECT COUNT(*) FROM punts;
 count 
-------
     1
(1 row)

lineas=> select * from punts;
 id | valor 
----+-------
 40 |     4
(1 row)

```
  
5. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
INSERT INTO punts (id, valor) VALUES (50,5);
BEGIN;
SELECT id, valor WHERE punts;
UPDATE punts SET valor = 4 WHERE id = 50;
COMMIT;
SELECT valor FROM punts WHERE id = 50;
```
  
### Explicació:  
  
Resumint, el commit no te efecte ja que s'ha realitzat una operació DML que provoca un error. Si hi ha un error tot el bloc resulta invalidat i no es pot fer el commit.
  
### Previsió:  
`50, 5`  
  
### Resultat:  
  
```
lineas=> INSERT INTO punts (id, valor) VALUES (50,5);
INSERT 0 1
lineas=> BEGIN;
BEGIN
lineas=> SELECT id, valor WHERE punts;
ERROR:  no existe la columna «id»
LINE 1: SELECT id, valor WHERE punts;
               ^
lineas=> UPDATE punts SET valor = 4 WHERE id = 50;
ERROR:  transacción abortada, las órdenes serán ignoradas hasta el fin de bloque de transacción
lineas=> COMMIT;
ROLLBACK
lineas=> SELECT valor FROM punts WHERE id = 50;
 valor 
-------
     5
```
  
6. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;
```
  
### Explicació:  
  
Despres del savepoint b, hi ha una operació corrupta. Despres d'aixo, hi ha un rollback al savepoint b. D'espres s'executa el commit. Per lo tant l'update i el primer insert es quedan grabats.  
  
### Previsió:  
`12`  
  
### Resultat:  
  
```
lineas=> DELETE FROM punts;
DELETE 2
lineas=> INSERT INTO punts (id, valor) VALUES (60,5);
INSERT 0 1
lineas=> BEGIN;
BEGIN
lineas=> UPDATE punts SET valor = 4 WHERE id = 60;
UPDATE 1
lineas=> SAVEPOINT a;
SAVEPOINT
lineas=> INSERT INTO punts (id, valor) VALUES (61,8);
INSERT 0 1
lineas=> SAVEPOINT b;
SAVEPOINT
lineas=> INSERT INTO punts (id, valor) VALUES (61,9);
ERROR:  llave duplicada viola restricción de unicidad «id_punts_pk»
DETAIL:  Ya existe la llave (id)=(61).
lineas=> ROLLBACK TO b;
ROLLBACK
lineas=> COMMIT;
COMMIT
lineas=> SELECT SUM(valor) FROM punts;
 sum 
-----
  12
```
