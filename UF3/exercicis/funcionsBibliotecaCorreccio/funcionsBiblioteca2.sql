--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- funcions bibblioteca
-- Jacint Iglesias
-- UF3

-- correcció

-- funció codiExemplarDisponible
\c biblioteca
\echo 'codiExemplarDisponible';
drop function codiExemplarDisponible(varchar);
create or replace function codiExemplarDisponible(p_titol varchar)
returns integer
as $$
declare
	v_idExemplar int;
begin
	-- s'obte el codi de l'exemplar
	select idExemplar
	into strict v_idExemplar
	from exemplar e
	inner join document d
	on e.idDocument = d.idDocument
	where lower(titol) = lower (p_titol)
	and lower(estat) = 'disponible'
	and idExemplar not in (	select idExemplar
							from prestec
							where datadev is null)
	limit 1; -- solo coje el primero
	-- ^ si el select falla se salta directamente a exception puenteando todo el codigo
	-- es comunica que s'ha trobat l'exemplar
	-- es retorna l'exemplar
	return v_idExemplar;
	-- exception cuan no es troba un exemplar 
	exception
		when no_data_found then
			return 0;
end;
$$ language plpgsql;


-- exemplar disponible V2

\echo 'codiExemplarDisponible2';

drop function codiExemplarDisponible2(varchar);
create or replace function codiExemplarDisponible2(p_titol varchar)
returns integer
as $$
declare
		cur_exemplars cursor for 
			select e.idexemplar miexemplar
			from exemplar e
			join document d
			on e.iddocument = d.iddocument
			where lower(titol) = lower (p_titol)
			and lower(estat) = 'disponible'
			and e.idexemplar not in (	select idExemplar
									from prestec
									where datadev is null)
			order by 1;
	v_exempar record;
begin
	for v_exempar in cur_exemplars
	   LOOP
			return v_exempar.miexemplar;
		end loop;		
	return 0;
end;
$$ language plpgsql;





-- funcio get tipus
\echo 'getTipus';

drop function getTipus(varchar);
create or replace function getTipus(p_titol varchar)
returns varchar
as $$
declare
	v_tipusExemplar varchar;
begin	

if codiExemplarDisponible(p_titol) != 0 then
	select format
	into strict v_tipusExemplar
	from document d inner join
	exemplar e on e.iddocument = d.iddocument
	where lower(titol) = lower(p_titol)
	limit 1;
	return lower(v_tipusExemplar);
else
	return null;
end if;
return null;
exception
		when no_data_found then
			return null;
end;
$$ language plpgsql;

-- funció prestecDocument

-- Per simplificació, considerarem que un usuari podrà tenir simultàniament un
-- màxim de 4 docs:

-- 2 docs del tipus llibre/revista
-- 1 DVD
-- 1 CD

-- er la funció prestecDocument, hem de tenir un exemplar disponible i 
-- evidentment un usuari que no tingui blocat el carnet degut a 
-- penalitzacions acumulades.

\echo 'prestecDocument';

drop function prestecDocument(integer,varchar);
create or replace function prestecDocument(	p_idusuari usuari.idusuari%type, p_titol document.titol%type,
											p_titol2 varchar default null,
											p_titol3 varchar default null,
											p_titol4 varchar default null,
											p_titol5 varchar default null) -- parametre formal
returns boolean
as $$
declare
	v_usuariBloquejat boolean;
	v_totalLlibresIRevistes int = 0;
	v_totalCd int = 0;
	v_totalDvd int = 0;
	v_total_documents int = 0;
	v_totalLlibresIRevistes_prellogats int = 0;
	v_totalCd_prellogats int = 0;
	v_totalDvd_prellogats int = 0;
	-- variables condicio prellogat
	v_renovacio1 boolean = false;
	v_renovacio2 boolean = false;
	v_renovacio3 boolean = false;
	v_renovacio4 boolean = false;
	v_renovacio5 boolean = false;
begin
	-- no esta blocat
	begin
		select bloquejat
		into strict v_usuariBloquejat
		from usuari
		where idusuari = p_idusuari
		and bloquejat = false;	
		-- codi per l'excepció (usuari no trobat o bloquejat)
		exception
			when no_data_found then
				raise notice 'Usuari % no esta donat d''alta o esta bloquejat', p_idusuari;
				return false;
	end;
	-- comprovem el numero de exemplars disponibles
	if getTipus(p_titol) in ('revista','llibre') then
		v_totalLlibresIRevistes := v_totalLlibresIRevistes + 1;
	elsif getTipus(p_titol) = 'cd' then
		v_totalCd := v_totalCd + 1;
	elsif getTipus(p_titol) = 'dvd' then
		v_totalDvd := v_totalDvd + 1;
	end if;
	if getTipus(p_titol2) in ('revista','llibre') then
		v_totalLlibresIRevistes := v_totalLlibresIRevistes + 1;
	elsif getTipus(p_titol2) = 'cd' then
		v_totalCd := v_totalCd + 1;
	elsif getTipus(p_titol2) = 'dvd' then
		v_totalDvd := v_totalDvd + 1;
	end if;
	if getTipus(p_titol3) in ('revista','llibre') then
		v_totalLlibresIRevistes := v_totalLlibresIRevistes + 1;
	elsif getTipus(p_titol3) = 'cd' then
		v_totalCd := v_totalCd + 1;
	elsif getTipus(p_titol3) = 'dvd' then
		v_totalDvd := v_totalDvd + 1;
	end if;		
	if getTipus(p_titol4) in ('revista','llibre') then
		v_totalLlibresIRevistes := v_totalLlibresIRevistes + 1;
	elsif getTipus(p_titol4) = 'cd' then
		v_totalCd := v_totalCd + 1;
	elsif getTipus(p_titol4) = 'dvd' then
		v_totalDvd := v_totalDvd + 1;
	end if;	
	if getTipus(p_titol5) in ('revista','llibre') then
		v_totalLlibresIRevistes := v_totalLlibresIRevistes + 1;
	elsif getTipus(p_titol5) = 'cd' then
		v_totalCd := v_totalCd + 1;
	elsif getTipus(p_titol5) = 'dvd' then
		v_totalDvd := v_totalDvd + 1;
	end if;
	-- control d'errors hi ha algun document a fer prestec
	if v_totalLlibresIRevistes + v_totalDvd + v_totalCd = 0 then
		raise notice 'Cap document esta disponible o existeix';
		return false;
	end if;
	-- control de documents previament llogats, quantitat
	-- llibres/revistes
	select count(*)
	into v_totalLlibresIRevistes_prellogats
	from prestec p
	join exemplar e on p.idexemplar = e.idexemplar
	join document d on e.iddocument = d.iddocument
	where idusuari = 1 and
	lower(format) in ('llibre','revista') and
	titol not in (p_titol,coalesce(p_titol2,''),coalesce(p_titol3,''),coalesce(p_titol4,''),coalesce(p_titol5,''));
	-- cd
	select count(*)
	into v_totalCd_prellogats
	from prestec p
	join exemplar e on p.idexemplar = e.idexemplar
	join document d on e.iddocument = d.iddocument
	where idusuari = 1 and
	lower(format) = 'cd'
	and
	titol not in (p_titol,coalesce(p_titol2,''),coalesce(p_titol3,''),coalesce(p_titol4,''),coalesce(p_titol5,''));
	-- dvd
	select count(*)
	into v_totalDvd_prellogats
	from prestec p
	join exemplar e on p.idexemplar = e.idexemplar
	join document d on e.iddocument = d.iddocument
	where idusuari = 1 and
	lower(format) = 'dvd'
	and
	titol not in (p_titol,coalesce(p_titol2,''),coalesce(p_titol3,''),coalesce(p_titol4,''),coalesce(p_titol5,''));
	-- comprovació de condició prellogat
	if (select lower(titol) = lower(p_titol)
	from document d 
	join exemplar e on d.iddocument = e.iddocument
	join prestec p on p.idexemplar = e.idexemplar 
	where titol = lower(p_titol)) then
	v_renovacio1 := true;
	end if; 
	-- prellogat dos
	if (select lower(titol) = lower(p_titol2)
	from document d 
	join exemplar e on d.iddocument = e.iddocument
	join prestec p on p.idexemplar = e.idexemplar 
	where titol = lower(p_titol2)) then
	v_renovacio2 := true;
	end if; 
	if (select lower(titol) = lower(p_titol3)
	from document d 
	join exemplar e on d.iddocument = e.iddocument
	join prestec p on p.idexemplar = e.idexemplar 
	where titol = lower(p_titol3)) then
	v_renovacio3 := true;
	end if; 
	if (select lower(titol) = lower(p_titol4)
	from document d 
	join exemplar e on d.iddocument = e.iddocument
	join prestec p on p.idexemplar = e.idexemplar 
	where titol = lower(p_titol4)) then
	v_renovacio4 := true;
	end if; 
	if (select lower(titol) = lower(p_titol5)
	from document d 
	join exemplar e on d.iddocument = e.iddocument
	join prestec p on p.idexemplar = e.idexemplar 
	where titol = lower(p_titol5)) then
	v_renovacio5 := true;
	end if; 
	-- fi condició prellogats
	-- afegim prellogats no repetits: 
	v_totalLlibresIRevistes := 	v_totalLlibresIRevistes + v_totalLlibresIRevistes_prellogats;
	v_totalCd := v_totalCd + v_totalCd_prellogats;
	v_totalDvd := v_totalDvd + v_totalDvd_prellogats;
	-- comprovem la quantitat de numero de documents disponibles esta 
	-- d'acord amb el numero maxim de documents (2 llibre/revista,
	-- 1 DVD, 1 CD)
	-- de lo contrari retorna false i fa un raise notice informant
	if v_totalLlibresIRevistes > 2 OR v_totalCd > 1 OR v_totalDvd > 1 then
		raise notice 'Nomes es poden llogar 2 llibres/revistes, 1 dvd i 1 cd. Has tractat de llogar el següent:';
		raise notice '% llibres/revistes: ',v_totalLlibresIRevistes;
		raise notice '% cds: ',v_totalCd;
		raise notice '% dvds: ',v_totalDvd;
		raise notice 'Titols dels documents: %, %, %, %, % ',p_titol,p_titol2,p_titol3,p_titol4,p_titol5;
		return false;
	-- s'efectua el lloguer
	else
		if codiExemplarDisponible(p_titol) != 0 then
			if not v_renovacio1 then
				insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesRenovat)
				values	(codiExemplarDisponible(p_titol),current_timestamp,null,p_idusuari,0);
				raise notice 'En prestec el document %',p_titol;
			else 
				update prestec set (vegadesrenovat, datapres) = (vegadesrenovat::int + 1, current_timestamp)
				where idexemplar in
					(	select idexemplar
						from prestec p
						join exemplar e on p.idexemplar = e.idexemplar
						join document d on d.iddocument = e.iddocument
						and idusuari = p_idusuari
						and datadev is null 
						and titol = p_titol);
			end if;	
		end if;
		if codiExemplarDisponible(p_titol2) != 0 then
			if not v_renovacio2 then
				insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesRenovat)
				values	(codiExemplarDisponible(p_titol2),current_timestamp,null,p_idusuari,0);
				raise notice 'En prestec el document %',p_titol2;
			else 
				update prestec set (vegadesrenovat, datapres) = (vegadesrenovat::int + 1, current_timestamp)
				where idexemplar in
					(	select idexemplar
						from prestec p
						join exemplar e on p.idexemplar = e.idexemplar
						join document d on d.iddocument = e.iddocument
						and idusuari = p_idusuari
						and datadev is null 
						and titol = p_titol2);
			end if;	
		end if;
		if codiExemplarDisponible(p_titol3) != 0 then
			if not v_renovacio3 then	
				insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesRenovat)
				values	(codiExemplarDisponible(p_titol3),current_timestamp,null,p_idusuari,0);
				raise notice 'En prestec el document %',p_titol3;
			else 
				update prestec set (vegadesrenovat, datapres) = (vegadesrenovat::int + 1, current_timestamp)
				where idexemplar in
					(	select idexemplar
						from prestec p
						join exemplar e on p.idexemplar = e.idexemplar
						join document d on d.iddocument = e.iddocument
						and idusuari = p_idusuari
						and datadev is null 
						and titol = p_titol3);
			end if;	
		end if;
		if codiExemplarDisponible(p_titol4) != 0 then
			if not v_renovacio4 then	
				insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesRenovat)
				values	(codiExemplarDisponible(p_titol4),current_timestamp,null,p_idusuari,0);
				raise notice 'En prestec el document %',p_titol4;
			else
				update prestec set (vegadesrenovat, datapres) = (vegadesrenovat::int + 1, current_timestamp)
				where idexemplar in
					(	select idexemplar
						from prestec p
						join exemplar e on p.idexemplar = e.idexemplar
						join document d on d.iddocument = e.iddocument
						and idusuari = p_idusuari
						and datadev is null 
						and titol = p_titol4);
			end if;	
		end if;
		if codiExemplarDisponible(p_titol5) != 0 then
			if not v_renovacio5 then			
				insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesRenovat)
				values	(codiExemplarDisponible(p_titol5),current_timestamp,null,p_idusuari,0);
				raise notice 'En prestec el document %',p_titol5;
			else
				update prestec set (vegadesrenovat, datapres) = (vegadesrenovat::int + 1, current_timestamp)
				where idexemplar in
					(	select idexemplar
						from prestec p
						join exemplar e on p.idexemplar = e.idexemplar
						join document d on d.iddocument = e.iddocument
						and idusuari = p_idusuari
						and datadev is null 
						and titol = p_titol5);
			end if;	
		end if;
	end if;
	v_total_documents := v_totalLlibresIRevistes + 	v_totalCd + v_totalDvd;
	raise notice '% documents en prestec.',v_total_documents;
	return v_total_documents > 0;
end;
$$ language plpgsql;

-- prestec document correcció
--
--

/*

drop function prestecDocument2(integer,varchar);
create or replace function prestecDocument2(	p_idusuari usuari.idusuari%type, p_titol document.titol%type) -- parametre formal
returns boolean
as $$
declare
	v_usuariBloquejat boolean;
	v_totalLlibresIRevistes int = 0;
	v_totalCd int = 0;
	v_totalDvd int = 0;
	v_total_documents int = 0;
	v_totalLlibresIRevistes_prellogats int = 0;
	v_totalCd_prellogats int = 0;
	v_totalDvd_prellogats int = 0;
begin
	-- no esta blocat
	begin
		select bloquejat
		into strict v_usuariBloquejat
		from usuari
		where idusuari = p_idusuari
		and bloquejat = false;	
		-- codi per l'excepció (usuari no trobat o bloquejat)
		exception
			when no_data_found then
				raise notice 'Usuari % no esta donat d''alta o esta bloquejat', p_idusuari;
				return false;
	end;
	-- comprovem el numero de exemplars disponibles
		-- pasar a case
	if getTipus(p_titol) in ('revista','llibre') then
		v_totalLlibresIRevistes := v_totalLlibresIRevistes + 1;
	elsif getTipus(p_titol) = 'cd' then
		v_totalCd := v_totalCd + 1;
	elsif getTipus(p_titol) = 'dvd' then
		v_totalDvd := v_totalDvd + 1;
	end if;
	-- control d'errors hi ha algun document a fer prestec
	if v_totalLlibresIRevistes + v_totalDvd + v_totalCd = 0 then
		raise notice 'Cap document esta disponible o existeix';
		return false;
	end if;
	-- control de documents previament llogats, quantitat
	-- llibres/revistes
	
	-- hacer una función generica que cuente documentos pasandole el 
	-- tipo de formato y usuario, guardar resultados en variable
	
	
	-- revistas/llibres
	select count(p.*)
	into v_totalLlibresIRevistes_prellogats
	from prestec p
	join exemplar e on p.idexemplar = e.idexemplar
	join document d on e.iddocument = d.iddocument
	where idusuari = p_idusuari and
	lower(format) in ('llibre','revista')
	and datadev is null;
	-- cd
	select count(p.*)
	into v_totalCd_prellogats
	from prestec p
	join exemplar e on p.idexemplar = e.idexemplar
	join document d on e.iddocument = d.iddocument
	where idusuari = p_idusuari and
	lower(format) = 'cd'
	and datadev is null;
	-- dvd
	select count(p.*)
	into v_totalDvd_prellogats
	from prestec p
	join exemplar e on p.idexemplar = e.idexemplar
	join document d on e.iddocument = d.iddocument
	where idusuari = p_idusuari and
	lower(format) = 'dvd'
	and datadev is null;
	-- afegim prellogats a llibres a llogar
	v_totalLlibresIRevistes := 	v_totalLlibresIRevistes + v_totalLlibresIRevistes_prellogats;
	v_totalCd := v_totalCd + v_totalCd_prellogats;
	v_totalDvd := v_totalDvd + v_totalDvd_prellogats;
	-- comprovem la quantitat de numero de documents disponibles esta 
	-- d'acord amb el numero maxim de documents (2 llibre/revista,
	-- 1 DVD, 1 CD)
	-- de lo contrari retorna false i fa un raise notice informant
	if v_totalLlibresIRevistes > 2 OR v_totalCd > 1 OR v_totalDvd > 1 then
		raise notice 'Nomes es poden llogar 2 llibres/revistes, 1 dvd i 1 cd. Has tractat de llogar el següent:';
		raise notice '% llibres/revistes: ',v_totalLlibresIRevistes;
		raise notice '% cds: ',v_totalCd;
		raise notice '% dvds: ',v_totalDvd;
		raise notice 'Titols dels documents: %',p_titol;
		return false;
	-- s'efectua el lloguer
	else
		insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesRenovat)
		values	(codiExemplarDisponible(p_titol),current_timestamp,null,p_idusuari,0);
		raise notice 'En prestec el document %',p_titol;
	end if;
	v_total_documents := v_totalLlibresIRevistes + 	v_totalCd + v_totalDvd;
	raise notice '% documents en prestec.',v_total_documents;
	return v_total_documents > 0;
end;
$$ language plpgsql;


*/
