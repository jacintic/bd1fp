-- Nom: Jacint 
-- Cognoms: Iglesias Casanova
-- Codi: iaw14270791
-- Nom projecte: iglesiasjacint
-- Data: 13-02-2020

-- example starting query:
-- psql -U postgres -d template1 -i 


\c template1
set role yo;
drop database if exists rols;
set role yo;
create database rols;
\c rols

/* remove previous roles/privileges*/

revoke all privileges on all tables in schema public from alumne_role,profesor_role;
drop role if exists alumne,alumne_role,profesor,profesor_role, tutor, tutor_role;

set role yo;

CREATE TABLE profe (
	idprof smallint  constraint profe_pk primary key,
    nom varchar(25)
);

CREATE TABLE modulxprofe (
    idprof smallint,
    idmodul smallint unique,
    CONSTRAINT PK_MODULXPROFE_PM PRIMARY KEY(idprof,idmodul),
    CONSTRAINT FK_MODULXPROFE_IDPROF FOREIGN KEY(idprof) REFERENCES profe(idprof)
);
CREATE TABLE faltes (
    idmodul smallint,
    dia date,
    hora time,
    codialumne varchar(22),
    justificada boolean,    
    CONSTRAINT PK_FALTES_MDC PRIMARY KEY(idmodul,dia,codialumne),
    CONSTRAINT FK_FALTES_IDMODUL FOREIGN KEY(idmodul) REFERENCES modulxprofe(idmodul)    
);

insert into profe (idprof,nom) (
values	(1,'Francesc Bibes'),
		(2,'Antoni Comins'),
		(3,'Alex Ferrer')
);

insert into modulxprofe (idprof,idmodul) (
values	(1,1),
		(2,2),
		(3,3)
);

insert into faltes (idmodul,dia,hora,codialumne,justificada) (
values	(1,'2020-03-03','08:00:00', 1,true),
		(2,'2020-03-03','08:00:00', 2,false),
		(3,'2020-03-03','08:00:00', 3,false),
		(1,'2020-03-03','08:00:00', 4,false)
);

set role postgres;
create user profesor;

create user tutor;

create user alumne;


/* ALUMNE */

create role alumne_role;
set role yo;


grant select on faltes to alumne_role;

set role postgres;

grant alumne_role to alumne;
set role alumne;

select * from faltes;
select * from modulxprofe ;

/* PROFE */

set role postgres;

create role profesor_role;

set role yo;

grant select,update,insert on faltes to profesor_role;

grant select on modulxprofe,profe to profesor_role;



set role postgres;

grant profesor_role to profesor;

set role profesor;


\echo 'aquesta operació ha de donar error';

insert into profe values (4,'test');


update faltes set hora = current_time where codialumne='1' and idmodul=1;


select * from profe;




-- /* TUTOR */

set role postgres;
 create role tutor_role;
set role yo;
grant select,update,insert,delete on faltes,modulxprofe,profe to tutor_role;


set role postgres;

grant tutor_role to tutor;

set role tutor;
update profe set nom= 'Joan Bibes' where idprof = 1;


-- -- añadir drop if exists
-- --drop role myroleHere if exists;
