# UF. Examen correcció.  

#### V com comprovar que el codi te errors o no.  

```
psql -d     -v ON_ERROR_STOP = 1   -f
            -t                     -c
```   
`\t` o `-t` en psql: treu la capçalera i la informació al peu de numero de files.  


#### Com entregar l'examen.  

* sense errors
* amb el nom que toca
* al lloc que toca
* comentaris --
* cada exercici "--Num"  

##### Males practiques:  

* possar `\echo`
* fer `\i`   

##### Bonus:     

`comm` compares two sorted files line by line  
