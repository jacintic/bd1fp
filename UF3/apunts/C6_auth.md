# UF3. PostgreSQL authentication methods.  
  
### Exercici:  
Hi hauran 3 rols:  
  
rols > tutor, profe i alumne  
  
taula > faltes : modul, dia, hora, codiAlumne(iaw), justificada(s/n- true/false)  
  
taula > profe: idprofe, nom  
  
taula > modulxprofe: idmodul,idprofe  
  
L'alumne nomes pot veure la taula faltes.  
Els profes, poden veure la taula faltes, la pot actualitzar, insertar (inser update select- pero nomes del seu codi)- si alguna cosa no es
tutor, pot llegir, inserir i modificar   
Inserir minim dos alumnes i dos profes. De cada alumne minim dos moduls.  
De cada modul minim varies faltes (justificades i no justificades).   
Els alumnes tindran rol alumne, tots els profes tindran rol profe, nomes un tendra role tutor.  

Creacio d'usuaris, creacio de rols i asignar rols a usuaris.  

```
tutor modifica qualsevol falta independent del modul
profe nomes modifica files del seu modul

```

Roles:  
```
set role none;
reset role;
```
```
user - te permis implicit de conexio
role - no te permis de conexió per defecte pero pot englobar varis usuaris
```
```
grant privilegis to user/role;
grant role to user;
```

```
\c template1
drop database if exists escola;
create database escola;
\c escola

CREATE TABLE profe
       (idprof smallint  constraint profe_pk primary key,
        nome character varying(25),
);


CREATE TABLE modulxprofe (
    idprof smallint,
    idmodul smallint,
    CONSTRAINT PK_MODULXPROFE_PM PRIMARY KEY(idprof,idmodul),
    CONSTRAINT FK_MODULXPROFE_IDPROF FOREIGN KEY(idprof) REFERENCES profe(idprof)
);

```