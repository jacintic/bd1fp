# UF3. Transaccions multi usuari.  
```
postgres# create user username;
userOwnerofDB => grant select,update,delete,insert on tablename to user1,user2;  
userOwnerofDB => revoke select,update,delete,insert on tablename from user1,user2
```

## Granularitat: tipus desbloqueig, nivell:
* taula
* fila
* camp
A postgres el nivell de bloqueig es a nivell de fila.   

## Postgres tiene en cuenta las ordenes de forma cronologica.  

## Un `select` es podria arribar a blocar si la `granularitat` estigues posada a nivell de taula.    
  
# Exercicis:  
### Mètodes d'autentificació.  
  
<ins>trust</ins>: <ins>usuario de ordenador (sistema)</ins> Y base de datos.  
  
### Mirar:  
  
ident  
peer  
md5  
password  
  
^Alguns son locals i altres remots, classificarlos per aquest criteri i tambe per si necesiten contrasenya.  
  
Google alter user per afegir contrasenya.  
  
Hacer un archivo markdown.  
  
**Ident**: remote, no password. Failed to connect even after editing `/var/lib/pgsql/data/pg_ident.conf`  
  
**peer**: local, no password, requires user to have matching user in the database, doesn't allow user to connect as other user.   
  
**md5**: remote or local, requires password. Password is encrypted and stored in a hash in the server.  
  
**password**: remote or local, requires password, password is sent to the server in clear text (it's not encrypted).