# UF3.    

**Executar script desde vim:**  
`:!psql -f exerciciRols.sql -d template1`    

**Repas de parametres de `psql`:**  
* -d template1 (bd default a postgres)
* U user  
* f file
* c command
* h host
* p port   
* v ON_ERROR_STOP = 1  

### COMO LOGEARSE COMO POSTGRES EN EXAMENES:  

`psql -d template1 -U postgres`  

##### Considerem que postgres esta como a trust en local  
psql -d template1 -U posrgres -f elmeUscript.sql  
^ observacions  

#### com es pot fer lo de rols i creació de bd:  
`psql -U postgres -f rols.sql`  
`psql  -f bd.sql`  

#### como dar permiso select en todas las tablas a la vez.  

`grant select on all tables in schema public to myUser;`  

#### Esplicació d'`eschema`:  
T1  T2  T3  

    ^public    
Per defecte tot el que crees a postgres es crea a l'eschema public.  
Si creas un altre schema a una base de dades, que te per exemple una taula profes, pots crear una taula dins d'aquest altre eschema, amb el mateix nom de el de la taula que esta a public.  
No hi ha problema de duplicitat si les taules estan a diferents eschema.  

Oracle, cuan fas `create user` et fa un `create schema` implicit.  

A postgres, quans eschemas `public` tindre? Tants com bases de dades. Es a dir, **un esquema pertany a dins d'una bd**.  

### Como interactuar con el schema.  
1. crear eschema  
2. crear tabla e indicar esquema: `create table mySchema.produts (...);`  

#### Que es el `search_path` ?  
La ruta on buscara els objectes de la bd (aixo es PostgreSQL). Conjunt d'esquemas on buscara els objectes que tenim a la bd. Li diu quin es l'ordre i on s'ha de buscar.    
La ruta on buscara els fitxer executables (aixo es bash).    

```
create schema if not exists authorization iaw14270791;

create table cliente (
        id integer,
        nom varchar(20)
);
insert into cliente (id,nom)(
values (1,'Manolo'),
        (2,'Francisco'));

select * form cliente; -- mostra la taula del nostre eschema (iaw14270791)
select * from public.cliente; --  mostra la taula cliente de l'eschema public
```
^ deducción el schema con nombre del usuario conectado tiene preferencia ante public, por que:
```
   search_path   
-----------------
 "$user", public
(1 row)
```
```
set search_path to public; -- com alterar el search path al valor que volguem
```
