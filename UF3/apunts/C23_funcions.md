# UF3 funcions.  
  
## Triggers.  
### <ins>Definició formal:</ins> funcions que s'executen (per part del sistema gestor) quan pasa un esdeveniment del tipus DML (insert, update o delete... o qualsevol combinació dels tres).  
#### Funcions que s'executen quan detecten un esdeveniment DML (insert, update o delete).  
  
##### <ins>Documentació:</ins> https://www.postgresqltutorial.com/creating-first-trigger-postgresql/  

* triggers d'esdeveniment de sistema (gestor) (p.e. quan algu es connecta a la BD), solen ser estudiats per la part d'administració de sistemas.
* triggers DML  
  * sobre taules
  * sobre vistes
  
#### Trigger, elements.
* capçalera
* cos (la funció en si)
```
CREATE FUNCTION trigger_function() 
   RETURNS trigger AS
```  
##### Nota: ^cos  
```
CREATE TRIGGER trigger_name 
{BEFORE | AFTER | INSTEAD OF} {event [OR ...]}
   ON table_name
   [FOR [EACH] {ROW | STATEMENT}]
       EXECUTE PROCEDURE trigger_function
```
##### Nota: ^capçalera  
  
### Explicació trigger: Quan faig una operació DML avans d'executarla, el sistema gestor es va a la llibreria per veure si hi ha algun trigger associat a aquesta taula. Elst riggers es poden executar avans o despres d'executar l'ordre DML.  
  
##### Exemple: INSERT/UPDATE/DELETE fora d'horari comercial.  
  
##### BEFORE/AFER: el codi del trigger s'executara avans o despres de l'operació DML.  
  
#### Temporalitat (quan):  
* BEFORE
* ATER
  
#### Condició:
* INSERT
* UPDATE
* DELETE
* INSERT OR UPDATE OR DELETE
  
#### On (sobre quina taula):  
* TAULA
* VISTA 
  
#### Puc fer que s'executi una vegada per operació o tantes vegades com a files afectades.  
* per sentencia (1 vegada)
* per linea afectada o altres (n vegadas)  
    
```
insert into pedido ...
update pedido ...
delete from pedido ...
QUAN op_dml (INSERT OR UPDATE OR DELETE) ON taulaZ
for statement

```  
  
### Cabecera v:      
```
CREATE TRIGGER last_name_changes
  BEFORE UPDATE
  ON employees
  FOR EACH ROW
  EXECUTE PROCEDURE log_last_name_changes();
```
##### Nota: se ejecuta ANTES (BEFORE UPDATE) del UPDATE, se ejecutara N VECES (FOR EACH ROW)

##### Trigger que impide hacer delete sobre la tabla producto.    
  
Función/trigger/cuerpo que llamara el trigger:    
```
create or replace function impedirDelete()
returns trigger
as $$
begin
	raise exception 'Error. No se puede modificar esta tabla, XXX.'; 
	-- raise exception provoca un error que cancela lo que venga despues
end;
$$ language plpgsql;

``` 
  
Trigger/cabecera:   
```
CREATE TRIGGER t_impedirDelete
BEFORE DELETE ON producto 
for statement -- o for each row
EXECUTE PROCEDURE impedirDelete();
```  

Prueva de fuego (juego de pruevas):
```
training=> delete from producto;
ERROR:  Error. No se puede modificar esta tabla, XXX.
CONTEXT:  función PL/pgSQL impedirdelete() en la línea 3 en RAISE

```
  
### Ejemplo  con WHEN / for each row -  WHEN solo para FOR EACH ROW

```
Trigger/cabecera:   
```
CREATE TRIGGER t_impedirDelete
BEFORE DELETE ON producto
for each row
when lower(fabcod) = 'aci'
EXECUTE PROCEDURE impedirDelete();
```
#### Trigger: FOR STATEMENT / FOR EACH ROW - quando no sepas o no importen los valores que introduzca el usuario sera FOR STATEMENT.  
#### Trigger: FOR EACH - los productos DE ESTE FABRICANTE no se pueden borrar, entonces FOR EACH, por que importa el valor que introduce el usuario (FABRICANTE).  
  
### Crear un trigger t_auditoria  que anotara en una tabla, quien hace una operación DML en la tabla EMP (bd scott) y cuando la realiza. Para ello necesito crear una tabla que se llamara control. La estructura de la qual tendra id (tipo serial), fecha (tipo timestamp), tipo (tipo texto, longitud 1, podra tener 3 valores, I,U o D), usuario (tipo varchar(50)).

### Crear un trigger t_auditoria  que anotara en una tabla, quien hace 
una operación DML  sobre la tabla EMP (bd scott)  y cuando la realiza. 
Para ello necesito crear una tabla que se llamara control. La estructura 
de la qual tendra id (tipo serial), fecha (tipo timestamp), tipo (tipo 
texto, longitud 1, podra tener 3 valores, I,U o D), usuario (tipo varchar(50)).  
  
```

-- create table
CREATE TABLE control (
    id serial CONSTRAINT PK_CONTROL_ID PRIMARY KEY,
    fecha timestamp,
    tipo varchar(1),
    usuario varchar(50),
    CONSTRAINT CK_CONTROL_TIPO CHECK(tipo in('I','U','D'))
);

-- trigger function
create or replace function f_auditoria()
returns trigger
as $$
begin
	insert into control (id,fecha,tipo,usuario)
	values (default,current_timestamp,'U',current_user); -- puedes oviar el campo para que gestione serial por defecto
	return null; -- en triggers a nivel de statement hay que hacer return null
end;
$$ language plpgsql;


-- cabecera

CREATE TRIGGER t_auditoria
AFTER DELETE or update or insert ON emp 
for statement -- o for each row -- el EACH es opcional (lo introduce PG por defecto)
EXECUTE PROCEDURE f_auditoria();
```
  
##### Nota: como hacemos la columna tipo?  
`tg_op` se utiliza en los cursores  
enlace: https://www.postgresql.org/docs/9.1/plpgsql-trigger.html  
  
```
create or replace function f_auditoria()
returns trigger
as $$
begin
	insert into control (id,fecha,tipo,usuario)
	values (default,current_timestamp,upper(substr(tg_op,1,1)),current_user); -- puedes oviar el campo para que gestione serial por defecto
	return null; -- en triggers a nivel de statement hay que hacer return null
end;
$$ language plpgsql;
```  
  
  
### `\h create trigger`:  
```
 \h create trigger
Command:     CREATE TRIGGER
Description: define a new trigger
Syntax:
CREATE [ CONSTRAINT ] TRIGGER name { BEFORE | AFTER | INSTEAD OF } { event [ OR ... ] }
    ON table_name
    [ FROM referenced_table_name ]
    [ NOT DEFERRABLE | [ DEFERRABLE ] [ INITIALLY IMMEDIATE | INITIALLY DEFERRED ] ]
    [ FOR [ EACH ] { ROW | STATEMENT } ]
    [ WHEN ( condition ) ]
    EXECUTE PROCEDURE function_name ( arguments )

where event can be one of:

    INSERT
    UPDATE [ OF column_name [, ... ] ]
    DELETE
    TRUNCATE

```
##### Oracle: (procedure (function with void, no return) and functions (returns something)). Postgres: functions (can return void) or triggers.  
  
```
CREATE TRIGGER trigger_name



insert into emp values(...);
```
  

### Se hace reserva:  
* ejemplar no disponible
* se recoje cuando ha sido devuelto

#### Formato: funcionsBiblioteca4.sql (...)

```

```
