# UF3 funcions.  
    
## Functions returning void.  
  
* perform <= solo postgres, no utilizar.  
* returns void <=
  
```
create or replace function prova()
returns void
as $$
begin
	raise info 'Hola';
end;
$$ language plpgsql;
```

### BLocs anonims (per provar funcions).  
  
```
do $$
begin
	prova();
end; $$
```
  
### Funcion llama a otra funcion (void).  
  
```
create or replace function prova2()
returns varchar
as $$
declare
	basura varchar(1);
begin
	basura := prova();
	return 'ok';
end;
$$ language plpgsql;
```
  
##### Nota: hay que asignar la funcion void a una variable, sino da error
en postgres.
  
### Opción 2 (return function as varchar).  
  
```
create or replace function prova2()
returns varchar
as $$
begin
	return prova();
end;
$$ language plpgsql;
```
 
### Teoria de void.  
  
plsql, pascal, C, etc
  
* procedure <= 
* function <= paraula reservada return avans del end
  
  
## enable disable triggers.  
  
```
-- desactivar
alter table tablename disable trigger triggername;
-- activar
alter table tablename enable trigger triggername;

-- si es vista:  
drop trigger fempleat on empleat;

```
  
### Show view `\sv empleat`.  
  
### Sequencia a partir de maxim.  
  
```
create sequence if not exists seq_emp
increment by 1 start with 1; -- can't start with 0

--set sequence default value 
select  setval('seq_emp', (select max(empno) FROM emp)::int);
``` 
  

