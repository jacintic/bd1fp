# UF3 funcions.  
  
## Mostrar trigger.  
`\dt pedido` => mostra el trigger de la taula (capçalera)  
`\sf impedirdelete`  muestra el cuerpo del trigger  
  
## Una función declarada VOID puedes invocarla sin asignarla a una variable.
  
```  
void v
mifuncion()

returns
variable := mifuncion()
```
  
## Revisar retornarDocument (no funcion) => actualizar tablas de biblioteca a reserva timestamp  
  
## Rehacer prestecDocument (titol,format,idusuari)  
^ hacer un count en el caso que se neceise (fromat) => mirar case del tutorial (sum de case)  
  
https://www.postgresqltutorial.com/postgresql-case/  
  
Osea hacer 1 select con 3 sums revista/libro, cd i dvd
  
```
SELECT
	SUM (
		CASE rental_rate
		WHEN 0.99 THEN
			1
		ELSE
			0
		END
	) AS "Mass",
	SUM (
		CASE rental_rate
		WHEN 2.99 THEN
			1
		ELSE
			0
		END
	) AS "Economic",
	SUM (
		CASE rental_rate
		WHEN 4.99 THEN
			1
		ELSE
			0
		END
	) AS "Luxury"
FROM
	film;
```
  
## prestecDocument => evitar que un usuario alquile varios ejemplares del mismo documento.  
  
## Triggers.  
  
```
statement, before
row, before
	SENTENCIA DML
row, after
statement, after

```
  
### Triggers. statement => null row => old/new  
  
```
select * from emp;

update emp
set deptno = 20
where enname in ('John Smith','James Jams');

-- ORDEN DE EJECUCIÓN DE TRIGGERS--
--
sentencia, abans
--
fila abans (2 filas)
nom dept old i new
nom dept old i new
--
fila despres (2 filas)
nom dept old i new
nom dept old i new
--
sentencia, despres
--

```

##### Not: el trigger solo hace un raise info.  
##### Nota: si hacemos un insert i hay un valor null el new.esecampo = NULL

