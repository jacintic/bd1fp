# UF3 funcions.  
  
## Apuntes de funciones reserva, reserva=>prestec, comprova retron => reserva
Tots els llibres en prestec.  
Agafes un exemplar d'ells i el reservo
Ha sigut retornat, i te l'hem guardat  el seu estat no es DIsponible sino RESERVAT  
  
Ha d'estar disponible i tots en prestec.  
  
codiexemplarDisponible => si no esta prestado  
  
CodiexemplarDisponible => condició disponible  
  
reservar ejemplar mas antiguo [check] - o random  
  
cuando se devuelve un ejemplar se actualiza dataavis si esta reservado    
  
	pueden haber varios usuarios esperando el mismo ejemplar  
		se escoje el que hizo la reserva antes o un limit 1 i a tomar por culo  
			se elimina esa reserva  
  			
un usuari pot reservar varios llibres  
  
  
  
usuari es nou => un usuari diferent al que esta retornant al que esta reservant [check]  
  
  !!!!! Hay una función que combierte reserva en un prestec! => delete y insert  
  
## Tipus de triggers.  
  
* per esdeveniments (de sistema) (usuari es conecta)
* DML
	* taules
		* statement (sentencia)
		* row (fila)
	* vistes   
  
## TG_ functions  
  
### TG_OP (diu el tipus d'operació (delete,insert,update,truncate))  
  
### OLD/NEW  
  
En oracle es `:OLD`/`:NEW` <= las llaman PSEUDOREGISTROS.  
  
Valor viejo y valor nuevo.  
  
```
select * from dept;

update dept
set loc ='Barcelona'
where deptno = 40;
```
  
#### ^ valor VIEJO BOSTON, valor nuevo Barcelona.  
  
### De Cada camp sempre podre preguntar per l'OLD i NEW (deptno, loc...).  
  
### S'ha d'executar el trigger AVANS (AFTER) de l'operació per obtenir l'OLD i NEW.  
    

## En paper fer una taula de 3x3. 

| X  | OLD  | NEW  |
|:---:|:---:|:---:|
|UPDATE | V  | V  |
|INSERT   | X  | V  |
|DELETE   | V  | X  |


## Com veure els triggers  
`\dft` => functiones que son triggers  
`\sf fprova` => no sale cabecera, solo cuerpo  
`\dfS+`
    
## Drop trigger:
`drop trigger prova on emp;`  
  
## Exercici: crear un trigger que actualitci les vendes d'un representant cada vegada que es fagi una nova comanda.  
  
```
insert into pedido
values (nextval('pedido_seq'),current_date,2117,105,'rei','2a44l',7,4500 * 7);
```

```
-- training

-- trigger function
create or replace function actualitzarVendes()
returns trigger
as $$
begin
	update repventa set ventas = ventas + new.importe -- da error en el intro
	where repcod = new.repcod;
	
	update oficina
	set ventas = ventas + new.importe
	where ofinum = (select ofinum
					from repventa
					where repcod = new.repcod);
	
	return new; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;


-- cabecera

CREATE TRIGGER tactualitzarVendes
after INSERT ON pedido 
for each row -- si hem de fer servir old i new es row
EXECUTE PROCEDURE actualitzarVendes();  
```
```
-- \ds para ver nombres de sequencias
insert into pedido
values (nextval('pedido_seq'),current_date,2117,105,'rei','2a44l',7,4500 * 7);

-- OLD: 367911.00
-- NEW: 399411.00

-- validación del profe:

select repcod, nombre, ventas - (oldvalue + insert value)
from repventa
where repcod = 105;
```

#### Nota: OLD/NEW => for each row
