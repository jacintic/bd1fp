# UF3. DCL i llenguatge procedural.  
```
				grant
DCL {
				revoke

^
control
```

## <ins>DCL</ins> dona <ins>privilegis/permisos</ins> sobre <ins>taules/objectes</ins> a <ins>usuaris/roles</ins>.  

## Un rol es pot considerar
## Un usuari (es un conjunt de privilegis sobre una persona) sera sempre un rol. Un rol no sera sempre un usuari. Un rol pot ser un conjunt de permisos, o perfil.  

```
create user username;
				||
create role username login;
```

```
roles									professor		tutor		...

						julio					x				
						jordi					x		  x
usuaris				 	nestor	       			x
						rafel					x

```

`\du` (mostra usuaris i attributs)  

## Revoke.  
```
revoke privilegi on objecte from usuari/rol;

revoke select on repventa from iaw54708991;

revoke select on repventa from iaw53336673;
```  

## Un usuari no pertany a una base de dades. El propietari de la base de dades (qui la crea pot donar privilegis sobre aquesta). Create user solo DBA (postgres). Grant/Revoke puede ser el propietario de la BD (iaw14270791).  

```
(como user iaw14270791)

training=> select current_user, session_user;
 current_user | session_user
--------------+--------------
 iaw14270791  | iaw14270791


(com a user postgres)

training=# select current_user,session_user;
 current_user | session_user
--------------+--------------
 postgres     | postgres

```
```
training=# set role iaw14270791 ;
SET
training=> select current_user,session_user;
 current_user | session_user
--------------+--------------
 iaw14270791  | postgres

```
current_user es el role que t'has possat (set role (qui soc en aquest moment)), session_user es l'origen de sessio.  

Si el session_user te un rol superior pot cambiar cap a baix i cap a dalt, pero un rol baix no pot anar cap a dlat.

V aixo cambia el current_user al session user.  
```
set role none;
reset role;
```

# Transaccións.  
Cada operació DML es una transacció. Lest transaccions s'han de poder desfer.  

* begin;
* commit;
* rollback;
* savepoint;

```
		insert		update			delete



t0				t1				t2					t3
					^					^
		savepoint A  savepoint B
										^						^
					^					<====== rollback to B
^					<================ rollback to A
<=============================== rollback
```
```
begin;
insert ...;
savepoint A;
update ...;
savepoint B;
delete ...;
rollback;
```

a t3 puc fer un rollback to A; o un rollback to A o un rollback.

```
training=> delete from pedido;
DELETE 32
training=> select * from pedido;
 pednum | fecha | cliecod | repcod | fabcod | prodcod | cant | importe
--------+-------+---------+--------+--------+---------+------+---------
(0 rows)

training=> rollback;
WARNING:  there is no transaction in progress
ROLLBACK
```
Si no hi ha begin... ^
```
scott=> begin;
BEGIN
scott=> insert into dept(deptno,dname,loc) values(50,'RDI', 'BARCELONA');
INSERT 0 1
scott=> savepoint A;
SAVEPOINT
scott=> update dept set deptno=55 where deptno=50;
UPDATE 1
scott=> savepoint B;
SAVEPOINT
scott=> delete from dept where deptno=55;
DELETE 1
scott=> rollback to A;
ROLLBACK
scott=> commit;
COMMIT
scott=> select * from dept;
 deptno |   dname    |    loc    
--------+------------+-----------
     10 | ACCOUNTING | NEW YORK
     20 | RESEARCH   | DALLAS
     30 | SALES      | CHICAGO
     40 | OPERATIONS | BOSTON
     50 | RDI        | BARCELONA
```

Postgres NO AUTOCOMIT;
ORACLE AUTOCOMIT;
No se puede cambiar el funcionamiento pro defecto, se ha de operar de acuerdo con ell Sistema Gestor.  
**Nota:** Un rollback hacia el pasado no puede hacer rollback al futuro.  
**Como saber si estas en una transaccion:** Si estas trabajando en un objeto en el que otra persona esta trabajando, se te quedara bloqueado hasta que el no haga **COMMIT**.  
**Cuando hay mucha gente con la misma tabla:** El sistema gestor va dando turnos.  
**Dead lock:** Se queda todo bloqueado. El sistema tiene un mecanismo para desbloquear el **dead log** y dar turnos.  
  
