# UF3  Dates.  
  

``` 
declare
	nom varchar := 'jordi';
	
	funciion... (nom varchar)
				 ^parametre formal
declare
	num int
		^variable local
select nomfuncio(variable)
				  ^parametre actual
```

## Select en funcions:  
```
declare
	v_deptno integer;
(...)
select deptno
into strict v_deptno
from dept
where lower(dname) := lower(v_dname);
```

### INTO STRICT  
forçar a que returni nomes un valor  
into strict fa que les excepcions funcionin  

### tipus agafat de la taula:  
```
declare
	v_depnto dept.depnto%type;
```
  
### tipus agafat de la taula en el parametre  
```
... (p_dname dept.dname%type)
```

#### una vegada creada la funció puc modificar el cos de la funció (begin - declare), 
pero si modifiquem parametres hem d'esborrar la funció  
`drop function obtener_deptno(p_dname varchar);`
  
#### Podem crear una mateixa funció amb diferents parametres. Pero amb compte per que  
#### pot ser un embolic.  
  
## Excepcions.  
  
```

```
###### Mostreu el codi d'un empleat li passem el nom i volem el seu codi.  


-- scott
```
create or replace function obtenirempno(p_ename emp.ename%type) -- cadena a traduir 
returns emp.ename%type
as $$
	declare
		v_empno emp.empno%type;
	begin
		select empno
		into strict v_empno
		from emp
		where lower(ename) := lower(p_ename);
		return v_empno;
	end;
$$ language plpgsql;



select obtenirempno('smith');
```


```
### multiples variables

```
declare
	v_empno emp.empno%type;
	v_job varchar(30);
begin
	select empno, job
	into strict v_empno, v_job
	from emp
	where lower(ename) = lower(p_ename);
	
	return 'Empleado ' || p_ename || ' amb codi ' || v_empno || ' trabaja de ' || v_job;
end;
$$ language plpgsql;

```
  
### multiples camps, multiples variables? NO!, variables tipus fila/ variables compostes.  
  
```
create or replace function obtenirempno(p_ename emp.ename%type) -- cadena a traduir 
returns varchar
as $$
declare
	r_empleat emp%rowtype;
begin
	select *
	into strict r_empleat
	from emp
	where lower(ename) = lower(p_ename);
	
	return 'Empleado ' || p_ename || ' amb codi ' || r_empleat.empno || ' trabaja de ' || r_empleat.job;
end;
$$ language plpgsql;
```

### Excepcions.  
  
Evitar errors no procesats. Es a dir, si introdueix un nom erroni, jo recullo la excepció.  
  

```
create or replace function obtenirempno(p_ename emp.ename%type) -- cadena a traduir 
returns varchar
as $$
declare
	r_empleat emp%rowtype;
begin
	select *
	into strict r_empleat
	from emp
	where lower(ename) = lower(p_ename);
	
	return 'Empleado ' || p_ename || ' amb codi ' || r_empleat.empno || ' trabaja de ' || r_empleat.job;
exception
		when no_data_found then 
			return 'El empleat ' || p_ename || ' no trobat';
		--when too_many_rows then
			--retutn 'Too many rows dude.'; -- se esperaba una fila y se devuelven N filas
end;
$$ language plpgsql;
```

### Exercicis:  
  
##### estan al git. 4.
