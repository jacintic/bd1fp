# UF3  Funcions.   
  
### raise notice  
### raise exception  
### chr(10) -- retorno de carro
### exception too many rows
  
### raise documentation :   
[raise documentation](https://www.postgresql.org/docs/current/plpgsql-errors-and-messages.html)  
  
#### raise exception => per a trigers.  

### raise notice 'mensaje %, otra variable %',v_variable, v_variable2;  
  

##### Donat el codi d'un representat, obtenir el nom i el treball. Retorna raise notice.  
  
```
create or replace function representant(p_repcod integer) --  puentear smallint
returns varchar
as $$
declare 
	v_repventa repventa%rowtype;
begin
	select * -- es rowtype por lo tanto hay que incluir toda la fila
	into strict v_repventa
	from repventa
	where repcod = p_repcod;
	raise notice 'El nom del representat amb codi % es %, el seu treball es %',p_repcod,v_repventa.nombre,v_repventa.puesto;
	return 'Consulta realizada con exito';
exception
		when no_data_found then 
			return  'Error: No se encuentra el representante '|| p_repcod;
end;
$$ language plpgsql;

select representant(109);

select representant(1099);
```
  
### CREATE TYPE (seleccionar en una funcion solo los campos especificsos) -- se hacen desde fuera de la función.  

```

-- amb nomes dos camps V

-- create type (se crea fuera de la función)

create type tipus_repventa as(nombre varchar(50), puesto varchar(100));

create or replace function representant(p_repcod integer) --  puentear smallint
returns varchar
as $$
declare 
	v_repventa tipus_repventa;
begin
	select nombre, puesto -- create type
	into strict v_repventa
	from repventa
	where repcod = p_repcod;
	raise notice 'El nom del representat amb codi % es %, el seu treball es %',p_repcod,v_repventa.nombre,v_repventa.puesto;
	return 'Consulta realizada con exito';
exception
		when no_data_found then 
			return  'Error: No se encuentra el representante '|| p_repcod;
end;
$$ language plpgsql;



select representant(109);

select representant(1099);
```

### `\dT` show types
[commands](../../UF2/apunts/C2_DDL.md)  
  
#### raise exception.  
  
Lo que hace es provocar una excepción deliberadamente (se reservara en   
clase para triggers). Para abortar operaciones no deseadas.  
  

```
create type tipus_repventa as(nombre varchar(50), puesto varchar(100)); -- variable composta

create or replace function representant(p_repcod integer) --  puentear smallint
returns varchar
as $$
declare 
	v_repventa tipus_repventa;
begin
	select nombre, puesto -- create type
	into strict v_repventa
	from repventa
	where repcod = p_repcod;
	raise notice 'El nom del representat amb codi % es %, el seu treball es %',p_repcod,v_repventa.nombre,v_repventa.puesto;
	return 'Consulta realizada con exito';
exception
		when no_data_found then 
		raise exception '% not found', p_repcod; -- $? daria un valor 
												 --diferente de 0, es decir se comunica error
			--return  'Error: No se encuentra el representante '|| p_repcod;
end;
$$ language plpgsql;

select representant(1099);
-- comunicado del programa: 
-- ERROR:  1099 not found
-- CONTEXT:  función PL/pgSQL representant(integer) en la línea 13 en RAISE


```

### triggers per impedir operacions no permesses: DML!!!(raise exception)  
  
##### Trigger per impedir que modificació de la taula es pugui fer  
##### fora d'orari comercial. (raise exception when modification outside
##### allowed time). Esto se propaga a las transacciones, al tirar un  
##### error provoca una falla en el bloque de codigo y todo se cancela.  
  
##### funcions amb multiple defaults.  
  
###### veremos while, loop y for, utilitat mes gran es cursor.  
  
```
create or replace function foo(	p_one integer default null,
								p_two integer default 43,
								p_three varchar default 'foo') -- parametre formal
returns varchar
as $$
begin
	return format('p_one=%s, p_two=%s, p_three=%s',p_one,p_two,p_three);
end;
$$ language plpgsql;
select foo(p_two=>22);
select foo(p_one=>11,p_two=>22);
select foo(p_three=>'hola');
select foo(p_three=>'hola',p_one=>1);
drop funciton foo(integer, integer, varchar); -- para cuando no puedes borrar normal
```  
  


#### funcion multiple defaults y un required (p_zero)
```
create or replace function foo2(p_zero int,
								p_one integer default null,
								p_two integer default 43,
								p_three varchar default 'foo') -- parametre formal
returns varchar
as $$
begin
	return format('p_zero=%s,p_one=%s, p_two=%s, p_three=%s',p_zero,p_one,p_two,p_three);
end;
$$ language plpgsql;

select foo2(11); --  asigna p_zero
select foo(11,p_one=>11,p_two=>22);
select foo2(11,p_three=>'hola');
select foo2(p_three=>'hola',p_zero=>1);
drop funciton foo2(integer,integer, integer, varchar); -- para cuando no puedes borrar normal
```  
  
#### En funcions si falla uno falla todo lo demas, es como una transacción.  
  
### tipos de dato serial en tabla postgres es una sequence gestionada por el sistema.  

```
create table document(
	idDocument serial primary key ,
	titol varchar(200),
	descripcio varchar(200),
	format varchar(100),
	CONSTRAINT document_format_ck check (format in ('llibre','revista','CD','DVD'))
);
```
### Ejercicio:  
```
create or replace function docuemntDisponible(p_titol varchar) -- el documento esta disponible?
-- returns volean
-- podria ser 1 o 0 tambien, que se lee como boolean 1 = true 0 = false
```
Hay un juego de pruevas ya puesto, podemos aumentarlo.  
if not document disponible, error
funcionsBiblioteca.sql
Hacer el .sh o utilizar el que ya esta hecho
