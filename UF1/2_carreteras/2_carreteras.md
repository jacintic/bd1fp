# Enunciat.

## 1. Carreteres

Dissenyar una base de dades que contingui informació relativa a totes les carreteres d'un determinat país. Es demana realitzar el disseny en el model E/R, sabent que:

. En aquest paıs les carreteres es troben dividides en trams.
. Un tram sempre pertany a una única carretera i no pot canviar de carretera.
. Un tram pot passar per diversos termes municipals, sent una dada d'interès al km. del tram pel qual . entra en dit terme municipal i al km. pel qual surt.
. Hi ha una sèrie d'àrees en les que s'agrupen els trams, cada un dels quals no pot pertànyer a més d'una àrea.

Establiu els atributs que considereu oportuns i d'ells, trieu --identificador-- per a cada entitat.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas habitatge](https://drive.google.com/file/d/1Z2AmXOVD0DWAlVHANF1IoREhzRerg-m5/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas carreteres](../img/carreteres.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  **Carretera**(<ins>IdCarretera</ins>, tramsNum, longitud, tipus)  
  **Tram**(<ins>IdTram</ins>, *IdCarretera*, *IdArea*)  
  **Area**(<ins>IdArea</ins>, descripcio)  
  **TermeMunicipal**(<ins>IdMunicipi</ins>, nom)  
  **TramxMunicipi**(<ins>IdTram, IdMunicipi</ins>, kmEntrada, kmSortida)  
## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
Tram|IdCarretera|Carretera
Tram|IdArea|Area
TramxTerme|IdTram|Tram
TramxTerme|IdMunicipi|TermeMunicipal



# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script carreteres.sql](../../UF2/scripts/carreteres.sql)
