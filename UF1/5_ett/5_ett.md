# Enunciat.

## 1.ETT

Desitgem informatitzar la informació que manipula una ETT sobre empreses que ofereixen llocs de treball i persones que busquen feina. Les empreses ofereixen llocs de treball, informant de la professió sol·licitada, el lloc de treball destinat i les condicions exigides per a aquest lloc. De les persones que busquen feina tenim el seu DNI, nom, estudis i professió desitjada. Ens interessa saber quines persones poden optar a un lloc de treball, és a dir, poden participar en el procés de selecció. La persona interessada en un lloc es podrà apuntar per fer una entrevista per a aquest lloc. Per a cada lloc de treball es poden inscriure totes les persones interessades.
De cada entrevista, volem saber com ha anat, és a dir, emmagetzemar anotacions sobre si el candidat és idoni o no per al lloc de treball. En alguns casos es formalitzaran contractes entre les empreses i les persones, i emmagatzemarem la data de signatura, durada i sou del contracte. Se suposa que una persona només pot ser contractada per una empresa. de
l'empreses tenim les dades de CIF, nom i sector. A més, es distingiran pimes i multinacionals: de les primeres emmagatzemarem la ciutat en la qual s'ubica i de les segones el nombre de països en els quals té representació.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas habitatge](https://drive.google.com/file/d/11x52n2Q1ik2R0bni-ERlCtsBRFBj-oT8/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas ETT](../img/ETT.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  **Empresa**(<ins>CIF</ins>, nomEmpresa, sector, tipus, ciutat, nombrePaisos)  
  **LlocTreball**(<ins>idTreball</ins>, professio, condicions, *CIF*)  
  **Persona**(<ins>DNI</ins>, nom, esudis, professioDesitjada)
  **Contracte**(<ins>*CIF, DNI*</ins>, dataSignatura, durada, sou)  
  **Entrevista**(<ins>*IdTreball, DNI*</ins>, data, idoni)  
 
## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
LlocTreball|CIF|Empresa
Contracte|CIF|Empresa
Contracte|DNI|Persona
Entrevista|IdTreball|LlocTreball
Entrevista|DNI|Persona

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)