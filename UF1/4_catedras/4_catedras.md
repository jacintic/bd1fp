# Enunciat.

## 1.Càtedres d'Universitat

Dissenyar una base de dades que reculli l'organització d'una Universitat. Es considera que:

. Els departaments poden estar en una sola facultat o ser interfacultatius,
agrupant en aquest cas càtedres que pertanyen a facultats diferents.
. Una càtedra es troba en un únic departament.
. Una càtedra pertany a una sola facultat.
. Un professor està sempre assignat a un únic departament i adscrit a una o diverses càtedres, podent canviar de càtedra, però no de departament. Interessa la data en què un professor és adscrit a una càtedra.
. Hi ha àrees de coneixement, i tot departament tindrà una única àrea de coneixement.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas habitatge](https://drive.google.com/file/d/1v9rnCnetx881M8VBpy3a2jM6WF8cM7Sr/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas habitatge](../img/catedres.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  **Facultat**(<ins>IdFacultat</ins>, nomFacultat, *IdCatedra*)  
  **Departament**(<ins>IdDepartament</ins>, nomDepartament, *IdFacultat*, *IdAreaConeixement*)  
  **FacultatxDepartament**(<ins>IdFacultat, IdDepartament</ins>)
  **AreaConeixement**(<ins>IdAreaConeixement</ins>, nomArea)  
  **Professor**(<ins>IdProfessor</ins>, nomProfessor, *IdDepartament*)  
  **Catedra**(<ins>IdCatedra</ins>, nomCatedra, especialitat, *IdDepartament*)  
  **ProfessorxCatedra**(<ins>IdProfessor, IdCatedra</ins>, datainscripcio)  
## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
Facultat|IdCatedra|Catedra
Departament|IdFacultat|Facultat
Departament|IdAreaConeixment|AreaConeixement
FacultatxDepartament|IdFacultat|Facultat
FacultatxDepartament|IdDepartament|Departament
Professor|IdDepartament|Departament
Catedra|IdDepartament|Departament
ProfessorxCatedra|IdProfessor|Professor
ProfessorxCatedra|IdCatedra|Catedra

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)