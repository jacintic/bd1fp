# Enunciat.

## 1. Zoos

Es vol dissenyar una base de dades relacional per emmagatzemar informació relativa als zoos existents en el món, així com les espècies i animals que aquests alberguen.

. De cada zoo es vol emmagatzemar el seu codi, nom, la ciutat i país on es troba, mida (m2) i pressupost anual.
. Cada zoo codifica els seus animals amb un codi propi, de manera que entre zoos es pot donar el cas que es repeteixi.
. De cada espècie animal s'emmagatzema un codi d'espècie, el nom vulgar, el nom científic, família a la qual pertany i si es troba en perill d'extinció.
. A més, s'ha de guardar informació sobre cada animal que els zoos tenen, com el seu número d'identificació, espècie, sexe, any de naixement, país d'origen i continent.

Establiu els atributs que considereu oportuns i d'ells, trieu identificador per a cada entitat.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas zoos](https://drive.google.com/file/d/1fFa8Wxhh4dy_KWAlK2jmbR3UgZX4_nEQ/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![Esquema Conceptual cas zoo](../img/zoo.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  **Zoo**(<ins>IdZoo</ins>, ciutat, pais, lloc, pressupost)  
  **Animals**(<ins>*IdZoo*, IdAnimal</ins>, nom, sexe, anyNeixement, paisorigen, continent, *IdEspecie*)  
  **Especie**(<ins>IdEspecie</ins>, nomVulgar, nomCientific, familia, perillExtincio)  
## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
Animal|IdZoo|Zoo
Animal|codEspecie|Especie

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script zoos.sql](../../UF2/scripts/zoos.sql)

