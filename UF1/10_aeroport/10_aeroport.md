# Enunciat.

## 1.Publicitat

Es desitja confeccionar una base de dades que emmagatzemi informació sobre la publicitat actualment emesa en els principals mitjans de comunicació. Hi ha tres tipus de suports publicitaris: televisió, ràdio i premsa escrita. Dels anuncis es coneix l'eslògan que utilitzen. Si l'anunci és televisiu o és una falca radiofònica, es desitja conèixer quants minuts dura, el nom de les cadenes de televisió o emissores de ràdio respectivament que l'emeten i quantes vegades al dia és emès en cada un dels mitjans. Si l'anunci és imprès s'emmagatzemarà el nom i el tiratge de les publicacions. Dels anuncis impresos podran tenir imatge o no. Un anunci pertany a una campanya publicitària que pot incloure altres anuncis. Cada campanya publicitària té un tema (venda d'un producte, promoció del turisme en una determinada zona, ajuda a determinats països, prevenció de malalties, ...) i un pressupost total per a tots els anuncis que inclou la mateixa. Aquesta campanya publicitària la contracta a un anunciant del que coneixem el seu nom i si és institució o empresa.

Finalment es vol contemplar quins dels mitjans audiovisuals (és a dir cadenes de televisió i emissores de ràdio) considerats abans són al seu torn empreses que s'anuncien.

Nota: establiu les claus primàries que considereu oportunes.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas aeroport](https://drive.google.com/file/d/1PVGS6yTJ3bAea4w5KiOdYRI6PNR-JrHH/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas aeroport](../img/aeroport.png)
  
  
# 3. Model lògic relacional
## 3.1. Esquema lògic
### Original  

  **Companyia**(<ins>idCompanyia</ins>, companyia, seu)  
  **Vol**(<ins>*idCompanyia*,idVol</ins>, horaSortidaProgramada, horaArribadaProgramada, *aeroportOrigen,terminalOrigen*, *aeroportDesti, terminalDesti*)  
  **VolEquivalent**(<ins>*companyia,Vol*,*companyiaEquivalent,VolEquivalent*</ins>, disponibilitat)  
  **Aeroport**(<ins>IATA</ins>, aeroport,ciutat,pais)  
  **Terminal**(<ins>*aeroport*, idTerminal</ins>, terminal)  
  **Viatge**(<ins>*companyia,Vol*,*dataSortida*</ins>, horaSortidaEstimada, horaArribadaEstimada, estat)  
  **Passatger**(<ins>DNI</ins>, nom, ...)  
  **Bitllet**(<ins>*DNI*, *companyia, vol*, data</ins>, classe, seient)  

## 3.2. Diagrama referencial

### Original
Relació referencial|Clau aliena|Relació referida
-|:-:|-
Vol|idCompanyia|Companyia
Vol|aeroporOrigen,terminalOrigen|Terminal
Vol|aeroporDesti,terminalDesti|Terminal
VolEquivalent|companyia,Vol|Vol
VolEquivalent|companyiaEquivalent,VolEquivalent|Vol
Terminal|aeroport|Aeroport
Viatge|companyia,vol|Vol
Viatge|dataSortida|Data
Bitllet|DNI|Passatger
Bitllet|companyia,vol|Vol
Bitllet|data|Data

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)